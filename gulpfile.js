var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
	
	mix.scripts([
		'../bower_components/jquery/dist/jquery.min.js',
		'../bower_components/babel/browser.min.js',
		'../bower_components/material-design-lite/material.min.js',
		'../bower_components/dialog-polyfill/dialog-polyfill.js',
		'jquery-ui.min.js',
	],'public/js/all.js'); 

	mix.scripts([
		'../bower_components/react/react.min.js',
		'../bower_components/react/react-dom.min.js',
	],'public/js/react.js')

	mix.scripts([
		'cash-register.js',
	],'public/js/cash-register.min.js')

	mix.styles([
		'../bower_components/font-awesome/css/font-awesome.min.css',
		'../bower_components/material-design-lite/material.min.css',
		'../bower_components/material-design-icons/iconfont/material-icons.css',
		'../bower_components/dialog-polyfill/dialog-polyfill.css',
		'../css/pagination.css',
		'/jquery-ui.css',
		'/custom.css',
	],'public/css/all.css');   

	mix.copy('resources/assets/bower_components/font-awesome/fonts', 'public/fonts');
    mix.copy('resources/assets/bower_components/material-design-icons/iconfont/MaterialIcons-Regular.eot', 'public/fonts/MaterialIcons-Regular.eot');
    mix.copy('resources/assets/bower_components/material-design-icons/iconfont/MaterialIcons-Regular.woff2', 'public/fonts/MaterialIcons-Regular.woff2');
    mix.copy('resources/assets/bower_components/material-design-icons/iconfont/MaterialIcons-Regular.woff', 'public/fonts/MaterialIcons-Regular.woff');
    mix.copy('resources/assets/bower_components/material-design-icons/iconfont/MaterialIcons-Regular.ttf', 'public/fonts/MaterialIcons-Regular.ttf');
	mix.copy('resources/assets/images', 'public/css/images');
	mix.copy('resources/assets/bower_components/highcharts', 'public/highcharts');


});