@extends('layouts.app')

@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Add Spoilage</h3>
		<br />
		<div class="row">
			<div class="medium-3 columns"></div>
			<div class="medium-9 columns">
				@if (count($errors) > 0)
				<div class="row">
					<div class="medium-8 columns warning callout" data-closable>
						<ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				@endif
				@if(Session::has('message'))
				<div class="row">
					<div class="medium-8 columns success callout" data-closable>
						Ingredient has been added to spoilages!
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				
				</div>
				 @endif
				<form id="add_menu" action="{{ route('add_spoilage') }}" method="POST">
					<div class="row">
						<div class="medium-8 columns">
							<label>Spoilage Date
								<input id="spoilage_date" name="spoilage_date" type="text" placeholder="Spoilage Date" autocomplete="off" value="{{ old('spoilage_date') }}">
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>Item Name
								<select id="item_id" name="item_id">
									<option value=""></option>
									@foreach($items as $item)
									<option value="{{ $item->id }}">{{ $item->name }}</option>
									@endforeach
								</select>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>Quantity
								<input name="quantity" type="number" id="quantity" value="{{ old('quantity') }}">
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<input name="user_id" type="hidden" id="user_id" value="{{ Auth::user()->id }}" />
							<input type="submit" class="button" value="Add">
							<a class="button secondary" href="{{ route('spoilages') }}">Cancel</a>
						</div>
					</div>
					{!! csrf_field() !!}
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
$( function() {
		$( "#spoilage_date" ).datepicker(
			{ dateFormat: 'yy-mm-dd' }
		);
	});
</script>
@stop