@extends('layouts.app')

@section('content')
@parent
<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Spoilages</h3>
		<br />
		<div class="row">
			<div class="large-4 columns">
	  			<a href="{{ route('add_spoilages_page') }}" class="button small">
	  			<i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Add Spoilages</a>
	  		</div>
			<div class="large-4 large-offset-4 columns">
				<form action="" method="get">
					<input type="text" id="search" name="search" placeholder="Search Item" value="{{ $search }}" />
				</form>
			</div>
  		</div>
		<table align="center" id="item_table" width="100%">
			<thead>
				<tr>
					<th width="20%">Spoilage Date</th>
					<th width="52%">Item Name</th>
					<th width="8%">Quantity</th>
					<th width="20%">Unit</th>
				</tr>
			</thead>
			<tbody>
				@forelse ($spoilages as $spoilage)
				<tr>
					<td>{{ $spoilage->spoilage_date }}</td>
					<td>{{ $spoilage->Items->name }}</td>
					<td>-{{ $spoilage->quantity }}</td>
					<td>{{ $spoilage->Items->unit }}</td>
				</tr>
				@empty
					<tr><td colspan="4" style="text-align:center;">No Spoilages Yet</td></tr>
				@endforelse
			</tbody>
		</table>
		<div align="center">
		@if(!$spoilages->isEmpty())
			{{ $spoilages->render() }}
		@endif
		</div>
	</div>
</div>
@stop