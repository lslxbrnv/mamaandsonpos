@extends('layouts.app')

@section('title', 'Order Details')
@section('content')
<script src="https://cdn.pubnub.com/sdk/javascript/pubnub.4.19.0.min.js"></script>
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
<style type="text/css">
	/* Always set the map height explicitly to define the size of the div * element that contains the map. */ 
	#map { height: 100%; } /* Optional: Makes the sample page fill the window. */ html, 
	body { height: 100%; margin: 0; padding: 0; }

</style>
<div class="row">
	<div class="medium-3 columns"></div>
	<div class="medium-9 columns">
		@if(Session::has('message'))
		<div class="notification is-success">
			<button class="delete"></button>
				Menu has been deleted!
		</div>
		 @endif
		 <div class="content">
		 	<p> <strong>Name : </strong> {{ $transaction->online_user->name }} </p>
		 	<p> <strong>Contact Number : </strong> {{ $transaction->online_user->contact_number }} </p>
		 	<p> <strong> Address :  </strong>{{ $transaction->online_user->address }} </p>
		 	<p> <strong> E-mail : </strong>{{ $transaction->online_user->email }} </p>
		 	<h3> Orders :  </h3>
		 	<table class="table">
		 		<thead>
		 			<tr>
		 				<th>Menu</th>
		 				<th>Quantity</th>
		 				<th>Price</th>
		 				<th>Total</th>
		 			</tr>
		 		</thead>
		 		<tbody>
		 			@php $total_bill = 0; @endphp
				 	@foreach ($orders as $order)
				 	<tr>
				 		<td>
				 			{{ $order->menu->menu }}
				 		</td>
				 		<td>
				 			{{ $order->quantity }}
				 		</td>
				 		<td>
				 			P {{ $order->pricing->price }}
				 		</td>
				 		<td>
				 			P {{ number_format($order->quantity * $order->pricing->price,2) }}
				 			@php $total_bill = $total_bill + ($order->quantity * $order->pricing->price) @endphp
				 		</td>
				 	</tr>
				 	@endforeach	
			 	</tbody>
		 	</table>
		 	<hr>
		 	<p><strong>Total Bill : </strong> P {{ number_format($total_bill,2) }} </p>
		 	<p><strong>Change for : </strong> P {{ $transaction->has_change }} </p>
		 </div>
		 <p>
		 	<div id="map" style="width:100%; height: 500px">Loading Map...</div>
		 </p>
	</div>
</div>
  <div id="map-canvas" style="width:600px;height:400px"></div>
<!-- Replace the value of the key parameter with your own API key. --> 
<!-- <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjNjNbjTmb_ri6vOjOWruDkr0SQPwQI1s&callback=initMap"> </script> -->
 <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ getenv('GOOGLE_API') }}&callback=getPoints"></script>
<script type="text/javascript">
  setInterval(function(){ 
      getPoints();
  }, 5000);

  function getPoints()
  {
    $.get("{{ route('order_location', $order->id) }}",showMap);
  }

  function showMap(response) {
    var $map = document.getElementById('map');
    var position = { lat: parseFloat(response.lat), lng: parseFloat(response.long) };
    window.map = new google.maps.Map($map, {
      center: position,
      zoom: 15
    });

    console.log(map);
    window.markers = window.markers || [];
    var marker = new google.maps.Marker({ map: map, position: position });
  }
</script>
@endsection