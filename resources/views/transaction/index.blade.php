@extends('layouts.app')

@section('content')
@parent
	<div class="row">
		<div class="medium-2 columns"></div>
		<div class="medium-10 columns">
			<h3>Transaction</h3><br />
			<div class="row">
				<div class="large-4 columns">
		  			&nbsp;
		  		</div>
		  		
				<div class="large-4 large-offset-4 columns">
					<form action="" method="get">
						<input type="text" id="search" name="search" placeholder="Search O.R. #" value="{{ $search }}" />
					</form>
				</div>
			</div>
			<table id="purchases_table" width="100%">
				<thead>
					<tr>
						<th width="10%" class="text-center">O.R. No.</th>
						<th width="20%">Customer</th>
						<th width="10%">Table</th>
						<th width="10%">Terminal</th>
						<th width="20%">Cashier</th>
						<th width="30%" class="text-center">Date</th>
					</tr>
				</thead>
				<tbody>
					@foreach($transaction as $row)
					<tr>
						<td class="text-center"><a style="text-decoration:underline;" title="View Transaction" href="{{ route('officialReceiptPrint', $row->id) }}" target="_blank">{{ $row->official_receipt_format }}</a></td>
						<td>{{ $row->customer_name }}</td>
						<td>{{ $row->tables->name }}</td>
						<td>{{ $row->terminal->terminal_name }}</td>
						<td>{{ $row->user->name }}</td>
						<td class="text-center">{{ $row->transaction_on }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<div align="center">
				@if(!$transaction->isEmpty())
				{{ $transaction->render() }}
				@endif
		</div>
		</div>
	</div>
@stop