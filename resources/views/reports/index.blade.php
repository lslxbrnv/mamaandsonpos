@extends('layouts.app')

@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Generate Reports</h3>
		<br />
		<div class="row">
			<div class="medium-3 columns"></div>
			<div class="medium-9 columns">
				@if (count($errors) > 0)
				<div class="row">
					<div class="medium-8 columns warning callout" data-closable>
						<ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				@endif
				@if(Session::has('message'))
				<div class="row">
					<div class="medium-8 columns success callout" data-closable>
						Ingredient has been successfully updated!
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				
				</div>
				@endif
				<div class="row">
					<div class="medium-8 columns">
						<label>Report Type
							<select id="select_report">
								<option value="1">Purchases</option>
								<!-- <option value="2">Inventory</option> -->
								<option value="3">Sales</option>
							</select>
						</label>
					</div>
				</div>
				<form target="_blank" method="GET" action="{{ route('generate_purchases') }}"  id="purchases_pane">
					<div class="row">
						<div class="medium-8 columns">
							<label>From
								<input name="purchases_from" type="text" id="purchases_from"  />
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>To
								<input name="purchases_to" type="text" id="purchases_to"  />
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<input type="submit" class="button" value="View">
							<a class="button secondary" href="{{ route('items') }}">Cancel</a>
						</div>
					</div>
				</form>
<!-- 
				<form target="_blank" method="GET" action="{{ route('generate_inventory') }}"  id="inventory_pane">
				<div class="row">
						<div class="medium-8 columns">
							<label>From
								<input name="purchases_from" type="text" id="inventory_from"  />
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>To
								<input name="purchases_to" type="text" id="inventory_to"  />
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<input type="submit" class="button" value="View">
							<a class="button secondary" href="{{ route('items') }}">Cancel</a>
						</div>
					</div>
				</form> -->

				<form target="_blank" method="GET" action="{{ route('generate_sales') }}"  id="sales_pane">
					<div class="row">
						<div class="medium-8 columns">
							<label>Terminal
							<select id="sales_terminal" name="sales_terminal">
								<option></option>
								@foreach($terminals as $terminal)
									<option value="{{ $terminal->id }}">{{ $terminal->terminal_name }}</option>
								@endforeach
							</select>
						</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>From
								<input name="sales_from" type="text" id="sales_from"  />
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>To
								<input name="sales_to" type="text" id="sales_to"  />
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<input type="submit" class="button" value="View">
							<a class="button secondary" href="{{ route('items') }}">Cancel</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$( function() {
		$('#inventory_pane').hide();
		$('#sales_pane').hide();

		$("#select_report").change(function(){
		   if($("#select_report").val() == 1)
		   {
				$('#purchases_pane').show();
				$('#inventory_pane').hide();
				$('#sales_pane').hide();
		   }
		   else if($("#select_report").val() == 2)
		   {
				$('#purchases_pane').hide();
				$('#inventory_pane').show();
				$('#sales_pane').hide();
		   }
		   else{
				$('#purchases_pane').hide();
				$('#inventory_pane').hide();
				$('#sales_pane').show();
		   }
		});
		
		$( "#inventory_to, #inventory_from, #sales_to, #sales_from, #purchases_to, #purchases_from" ).datepicker(
			{ dateFormat: 'yy-mm-dd' }
		);
	});

</script>
@stop