@extends('layouts.empty')

@section('content')
@parent
	<h4 align="center">Sales Report from {{ $sales_from }} to {{ $sales_to }}</h4>
	<div align="center">Total No. of Transactions : {{ count($transactions) }}</div>
	<div align="center">
		<button id="print" onclick="Print();" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary">Print</button>
	</div>
	<table width="100%" class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
		<?php 
			$grand_total_gross_sales = 0;
			$grand_total_vat_sales = 0;
		?>
		@forelse($transactions as $transaction)
		<tr style="background-color:#e3e3e3">
			<th scope="row" width="20%">Transaction Number</th>
			<th width="40%" class="mdl-data-table__cell--non-numeric">Cashier</th>
			<th width="40%" class="mdl-data-table__cell--non-numeric" colspan="2">Transaction Date</th>
		</tr>
		<tr>
			<td width="20%"><strong>{{ $transaction->official_receipt_format }}</strong></td>
			<td width="40%" class="mdl-data-table__cell--non-numeric">{{ $transaction->user->name }}</td>
			<td width="40%" class="mdl-data-table__cell--non-numeric" colspan="2">{{ $transaction->transaction_on }}</td>
		</tr>	
		<tr>
			<th width="20%">&nbsp;</th>
			<th width="40%" class="mdl-data-table__cell--non-numeric" scope="row">Item Name</th>
			<th width="10%" class="mdl-data-table__cell--non-numeric" scope="row">Item Price</th>
			<th width="30%" class="mdl-data-table__cell--non-numeric" scope="row">&nbsp;</th>
		</tr>
		<?php 

		$total_sales = 0;
		$total_vat_sales = 0;
		$menus = App\TransactionMenu::with('menu')->where('transaction_id', '=' ,$transaction->id)->get();

		foreach ($menus as $menu) 
		{
			$total_sales += $menu->price;
			$total_vat_sales += ($menu->price*$menu->vat);
			?>
				<tr>
					<td width="20%"></td>
					<td width="40%" class="mdl-data-table__cell--non-numeric">{{ $menu->menu->menu }}</td>
					<td width="10%" class="mdl-data-table__cell--non-numeric">P {{ $menu->menu->selling_price }}</td>
					<td width="30%">&nbsp;</td>
					
				</tr>
			<?php

		}
		?>
		<tr>
			<?php $grand_total_gross_sales += $total_sales; ?>
			<td width="20%"></td>
			<td width="40%" ><strong>Gross Sales</strong></td>
			<td width="40%" class="mdl-data-table__cell--non-numeric">P {{ number_format($total_sales, 2) }}</td>
			<td width="30%"></td>
		</tr>
		<tr>
			<?php $grand_total_vat_sales += $total_vat_sales; ?>
			<td width="20%"></td>
			<td width="40%"><strong>Vat Sales</strong></td>
			<td width="40%" class="mdl-data-table__cell--non-numeric">P {{ number_format($total_vat_sales, 2) }}</td>
			<td width="30%"></td>
		</tr>
		
		<tr>
			<td width="20%"></td>
			<td width="40%"><strong>Net Sales</strong></td>
			<td width="40%" class="mdl-data-table__cell--non-numeric">
					P {{ number_format( ($total_sales - $total_vat_sales) , 2) }}
			</td>
			<td width="30%"></td>
		</tr>
		<!-- <td>{{ number_format($total_vat_sales, 2) }}</td>
		<td>{{ number_format($total_sales, 2) }}</td>
		<td>{{ number_format( ($total_sales - $total_vat_sales) , 2) }}</td> -->
		
		<tr></tr>
	
		@empty
		<tr colspan="4">No Records Found</tr>
		@endforelse
		<tr>
			<td width="20%"></td>
			<td width="40%" ><h5>Grand total Gross Sales</h5></td>
			<td width="40%" class="mdl-data-table__cell--non-numeric"><h5>P {{ number_format($grand_total_gross_sales, 2) }}</h5></td>
			<td width="30%"></td>
		</tr>
		<tr>
			<td width="20%"></td>
			<td width="40%"><h5>Grand total Vat Sales</h5></td>
			<td width="40%" class="mdl-data-table__cell--non-numeric"><h5>P {{ number_format($grand_total_vat_sales, 2) }}</h5></td>
			<td width="30%"></td>
		</tr>
			<?php $grandtotalPurchases = 0; ?>
			@forelse($purchases as $purchase)
			<?php $subtotalPurchases = 0; ?>
				@foreach($purchase->PurchasedItems as $PurchasedItem)
					<?php $subtotalPurchases +=  ($PurchasedItem->price *  $PurchasedItem->quantity); ?>
				@endforeach
				<?php $grandtotalPurchases+=$subtotalPurchases; ?>
			@empty
			@endforelse

		<tr>
			<td width="20%"></td>
			<td width="40%"><h5>Grand Total Net Sales</h5></td>
			<td width="40%" class="mdl-data-table__cell--non-numeric">
					<h5>P {{ number_format( ($grand_total_gross_sales - $grand_total_vat_sales) , 2) }}</h5>
			</td>
			<td width="30%"></td>
		</tr>

		<tr>
			<td width="20%"></td>
			<td width="40%"><h5>Grand Total Purchases</h5></td>
			<td width="40%" class="mdl-data-table__cell--non-numeric">
					<h5>P {{ number_format($grandtotalPurchases , 2) }}</h5>
			</td>
			<td width="30%"></td>
		</tr>

		<tr>
			<td width="20%"></td>
			<td width="40%"><h5>Grand Total Income</h5></td>
			<td width="40%" class="mdl-data-table__cell--non-numeric">
					<h5>P {{ number_format(($grand_total_gross_sales - $grand_total_vat_sales) - $grandtotalPurchases , 2) }}</h5>
			</td>
			<td width="30%"></td>
		</tr>
	</table>
	<script type="text/javascript">
		function Print()
		{
			$('#print').hide();
			window.print();
			window.close();
		}
	</script>
@stop