@extends('layouts.empty')

@section('content')
@parent
	<h4 align="center">Inventory Report from {{ $inventory_from }} to {{ $inventory_to }}</h4>
	<div align="center">
		<button id="print" onclick="Print();" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary">Print</button>
	</div>
	<table width="100%" class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
		<thead>
			<tr>
				<th class="mdl-data-table__cell--non-numeric" width="50%">Item Name</th>
				<th width="25%">Quantity</th>
				<th class="mdl-data-table__cell--non-numeric" width="25%">Transaction Date</th>
			</tr>
		</thead>
		<tbody>
			<?php $totalQuantity = 0; ?>
			@foreach($inventory as $row)
			<?php $totalQuantity += $row['quantity']; ?>
			@if($row['name'] == '')
			<tr style="color:green">
			@else
			<tr style="color:red">
			@endif
				<td width="50%" class="mdl-data-table__cell--non-numeric">{{ $row['item_name'] }}</td>
				<td width="25%">{{ $row['quantity'] }}</td>
				<td width="25%" class="mdl-data-table__cell--non-numeric">{{ $row['transaction_on'] }}</td>
			</tr>
			@endforeach
			<tr>
				<td width="50%"></td>
				<td width="25%"><h5>Total Quantity : {{ $totalQuantity }}</h5></td>
				<td width="25%"></td>
				
			</tr>
		</tbody>
	</table>
	<script type="text/javascript">
		function Print()
		{
			$('#print').hide();
			window.print();
			window.close();
		}
	</script>
@stop