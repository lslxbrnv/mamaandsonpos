@extends('layouts.empty')

@section('content')
@parent
	<h4 align="center">Purchases Report from {{ $purchase_from }} to {{ $purchase_to }}</h4>
	<div align="center">
		<button id="print" onclick="Print();" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary">Print</button>
	</div>
	<table width="100%" class="mdl-data-table mdl-js-data-table ">
		<tbody>
			<?php $grandtotal = 0; ?>
			@forelse($purchases as $purchase)
				<tr style="background-color:#e3e3e3;">
					<th width="5%" class="mdl-data-table__cell--non-numeric">Receipt Number : {{ $purchase->receipt }}</th>
					<th width="30%"></th>
					<th width="30%"></th>
					<th width="30%"></th>
					<th width="5%">Purchase Date : {{ $purchase->purchase_date }}</th>
				</tr>
				<tr>
					<th colspan="2" width="30%" class="mdl-data-table__cell--non-numeric">Item Name</th>
					<th width="30%">Quantity</th>
					<th width="30%"></th>
					<th width="10%">Total</th>
				</tr>
				<?php $subtotal = 0; ?>
				@foreach($purchase->PurchasedItems as $PurchasedItem)
				<?php $subtotal +=  $PurchasedItem->price; ?>
				<tr>
					<!-- <th width="5%"></th> -->
					<td colspan="2" width="30%" class="mdl-data-table__cell--non-numeric">{{ $PurchasedItem->Items->name }}</td>
					<td width="30%">{{ $PurchasedItem->quantity }} {{ $PurchasedItem->Items->unit }} </td>
					<td width="30%"></td>
					<td width="10%">P {{ $PurchasedItem->price }}</td>
				</tr>
				@endforeach
				<tr>
					<td colspan="3"></td>
					<td><strong>Subtotal : </strong></td>
					<td ><strong>{{ number_format($subtotal, 2) }}</strong></td>
					<?php $grandtotal+=$subtotal; ?>
				</tr>
			@empty
			<tr>No Results Found</tr>
			@endforelse
			<tr>
					<td colspan="3"></td>
					<td><h4>Grand Total Purchases : </h4></td>
					<td ><h4>{{ number_format($grandtotal, 2) }}</h4></td>
				</tr>
		</tbody>
	</table>
	<script type="text/javascript">
		function Print()
		{
			$('#print').hide();
			window.print();
			window.close();
		}
	</script>
@stop