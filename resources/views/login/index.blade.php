<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
         <title>Mama and Sons Pizzeria - Log In</title>

        <!-- Fonts -->
        <link rel="stylesheet" href="{{ asset('components/foundation-sites/dist/css/foundation.min.css') }}" >
        <link rel="stylesheet" href="{{ asset('components/foundation-sites/dist/css/foundation-flex.min.css') }}" >

    </head>
    <style>
        input.button{
            background-color:#333;
        }
        input.button:hover{
            background-color:#ccc;
        }
        body{
            background-color:#fff;
        }
        #logo{
            margin-top:-90px;
            color: #ccc;
            height: 280px;
            background: url({{ asset('images/home-icon.png') }}) center / cover;
            background-size:360px;
            background-repeat: no-repeat;
        }
    </style>
    <body>
        <div class="row" >
            <div class="small-2 large-4 columns"></div>
            <div style="margin-top:100px;" class="small-4 large-4 columns">
                <form action="{{ route('user_login') }}" method="POST">
                    <div class="row">
                        <div id="logo" class="medium-12 columns">
                            
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-12 columns">
                            @if(Session::has('login-message'))
                            <div id="message">
                                <div align="center" class="small-12 large-12 columns alert callout">
                                    <strong>{!! Session::get('login-message') !!}</strong>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="medium-12 columns">
                            <label>Username
                                <input type="text" id="username" name="username" placeholder="Username">
                            </label>
                        </div>
                     </div>
                    <div class="row">
                        <div class="medium-12 columns">
                            <label>Password
                                <input type="password" id="password" name="password" placeholder="Password">
                            </label>
                        </div>
                    </div>
                   
                    {!! csrf_field() !!}
                    <div class="row" >
                        <div class="medium-12 columns">
                            <input type="submit" class="button" value="Login">
                        </div>
                    </div>
                </form>
            </div>
            <div class="small-6 large-2 columns"></div>
        </div>
        
        <script src="{{ asset('components/jquery/dist/jquery.js') }}"></script>
        <script src="{{ asset('components/foundation-sites/dist/js/foundation.min.js') }}"></script>
        <script src="{{ asset('components/what-input/dist/what-input.min.js') }}"></script>

        <script type="text/javascript">
            $(document).foundation();
        </script>
    </body>
</html>