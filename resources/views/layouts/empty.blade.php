<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mama and Sons Official Receipt</title>
    <style>
        body{
            background-color:#f3f3f3;
        }
    </style>
    <!-- Fonts -->
    <link rel="stylesheet" href="{{ asset('css/all.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}" />
    <script src="{{ asset('js/all.js') }}"></script>
    <script src="{{ asset('js/all.js') }}"></script>
</head>
<body id="app-layout">
	<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
		<main class="mdl-layout__content">
	    	<div class="page-content">
                @yield('content')
	    	</div>
		</main>
	</div>
</body>
</html>
