<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Mama and Sons POS</title>
    <style>
        body{
            background-color:#CCC;
        }
        ul#left-panel-menu li{ 
            padding:1px;
        }
        ul#left-panel-menu li a{ 
            color:#333;
        }
         ul#left-panel-menu li a:hover{ 
            color:#c3c3c3;
        }
        #title{
            padding:2px;
            margin-top:5px;
        }
        #canvas-inner{
            background-color:#f3f3f3;
        }
        a.button, input.button, button.button{
           /* background-color:#ca4300;*/
        }
         a.button:hover, input.button:hover{
            background-color:#333;
        }
        table td a{
            color:#333;
        }
         table td a:hover{
            color:black;
        }
    </style>
    <!-- Fonts -->
<!-- 
    <link rel="stylesheet" href="{{ asset('css/all.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}" /> -->
    <link rel="stylesheet" href="{{ asset('components/foundation-sites/dist/css/foundation.min.css') }}" >
    <link rel="stylesheet" href="{{ asset('components/foundation-sites/dist/css/foundation-flex.min.css') }}" >
    <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" >

    <link rel="stylesheet" href="{{ asset('components/jquery/dist/jquery-ui.css') }}" >

    <!-- JavaScripts -->
    <script src="{{ asset('components/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('components/jquery/dist/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('components/foundation-sites/dist/js/foundation.min.js') }}"></script>
    <script src="{{ asset('components/what-input/dist/what-input.min.js') }}"></script>
   <!--  <script src="{{ asset('js/all.js') }}"></script> -->
    <script src="{{ asset('highcharts/highcharts.js') }}"></script>
</head>
<body style="background-color:#ebebeb">
    <div class="off-canvas-wrapper" id="off-canvas">
        <div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
            <div class="off-canvas position-left reveal-for-large" id="canvas-inner" data-off-canvas data-position="left">
                <div class="row column">
                    <br>
                    <h5 id="title">
                        <img src="{{ asset('images/brand.png') }}">
                    </h5>
                    <div id="logo"></div>
                </div>
                <ul id="left-panel-menu" class="vertical menu" data-accordion-menu>
                    <li><a href="{{ route('inventorylist') }}">
                       <!--  <i class="fa fa-cube" aria-hidden="true"></i>&nbsp; -->
                           <h5> Inventory</h5>
                        </a>
                    </li>
                    <li><a href="{{ route('items') }}">
                        <!-- <i class="fa fa-lemon-o" aria-hidden="true"></i>&nbsp; -->
                            <h5>Items</h5>
                        </a>
                    </li>
                    <li><a href="{{ route('purchases') }}">
                        <!-- <i class="fa fa-money" aria-hidden="true"></i>&nbsp; -->
                        <h5>Purchases</h5>
                        </a>
                    </li>
                       <li><a href="{{ route('spoilages') }}">
                       <!--  <i class="fa fa-trash" aria-hidden="true"></i>&nbsp; -->
                        <h5>Perished Items</h5>
                        </a>
                    </li>
                    <li> <a href="">
                        <!-- <i class="fa fa-cutlery" aria-hidden="true"></i> &nbsp; --><h5>Menu</h5> </a>
                        <ul class="menu vertical nested">
                            <li><a href="{{ route('category') }}"> Category</a></li>
                            <li><a href="{{ route('menu') }}"> Menu</a></li>
                        </ul>
                    </li>
                    <li><a href="">
                    <!-- <i class="fa fa-money" aria-hidden="true"></i>&nbsp; --> <h5>Sales</h5></a>
                         <ul class="menu vertical nested">
                            <li><a href="{{ route('transaction') }}">Transactions</a></li>
                            <li><a href="{{ route('sales') }}">Cash Register</a></li>
                        </ul>
                    </li>
                    <li><a href="{{ route('reports') }}">
                       <!--  <i class="fa fa-file-text-o" aria-hidden="true"></i>&nbsp; --><h5>Reports</h5></a>
                    </li>


                    <li><a href=""><h5>Settings</h5></a> 
                        <ul class="menu vertical nested">
                            <li><a href="{{ route('sizes') }}">Sizes</a></li>
                            <li><a href="{{ route('orders') }}">Orders</a></li>
                            <li><a href="{{ route('table-settings') }}">Tables</a></li>
                            <li><a href="{{ route('users') }}">Users</a></li>
                        </ul>
                    </li>

                    <li><a href="{{ route('logout') }}"> 
                           <!--  <i class="fa fa-sign-out" aria-hidden="true"></i> &nbsp; --><h5>Logout</h5>
                        </a>
                    </li>
                </ul>
            </div>
            @yield('content')
        </div>
    </div>
    <script type="text/javascript">
        $(document).foundation();

        $(function(){
           // $('#left-panel-menu').foundation('showAll');    
        });
    </script>
</body>
</html>
