@extends('layouts.app')
@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Purchases</h3>
		<br />
		<div class="row">
	  		<div class="large-4 columns">
	  			<a href="{{ route('add_purchase_page') }}" class="button small">
	  			<i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Add Purchase</a>
	  		</div>
			<div class="large-4 large-offset-4 columns">
				<form action="" method="get">
					<input type="text" id="search" name="search" placeholder="Search Receipt #" value="{{ $search }}" />
				</form>
			</div>
		</div>
		<table align="center" id="menu_table" width="100%">
			<thead>
			<tr>
				<th class="text-center" width="20%">View</th>
				<th width="40%">Receipt No.</th>
				<th width="40%" class="text-center">Date of Purchase</th>
			</tr>
			</thead>
			<tbody>
			@forelse ($purchases as $purchase)
				<tr>
					<td width="20%" align="center">
						<a href="{{ route('view_purchase_details' ,  $purchase->id)}}">
							<i class="fa fa-eye" aria-hidden="true"></i>
						</a>
					</td>
					<td width="40%">{{ $purchase->official_receipt_format }}</td>
					<td width="40%" align="center">{{ $purchase->purchase_date->format('Y-m-d') }}</td>
				</tr>
				@empty
					<tr><td colspan="4" style="text-align:center;">No Purchases Yet</td></tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	
	function DeleteMenu(menuid, selector)
	{
		if(confirm("Are you sure to delete this?") ==  true){
			$('#menurow-'+menuid).remove();
			$.ajax({
				type : 'GET',
				url : '{{ route("delete_menu") }}',
				data : 'menuid='+menuid,
				cache : false,
				dataType : "script"
			}).done(function(response) {
				
			}).fail(function(jqXHR, textStatus, y) {	
				//alert(textStatus);
			});
		}
	}
</script>

@stop