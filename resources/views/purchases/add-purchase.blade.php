@extends('layouts.app')
@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Add Purchase</h3>
		<br />
		<div class="row">
			<div class="medium-1 columns"></div>
			<div class="medium-11 columns">
				@if (count($errors) > 0)
				<div class="row">
					<div class="medium-10 columns warning callout" data-closable>
						<ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				@endif
				@if(Session::has('message'))
				<div class="row">
					<div class="medium-10 columns success callout" data-closable>
						Purchased has been saved!
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				 @endif

				 <form id="add_purchased_order" action="{{ route('add_purchased_order') }}" method="POST">	
				 	<div class="row">
						<div class="medium-4 columns">
							<label>Receipt Number
								<input name="receipt" type="text" id="receipt" value="{{ old('receipt') }}" autocomplete="off" />
							</label>
						</div>
						<div class="medium-4 columns">
							<label>Date of Purchase
								<input name="purchase_date" type="text" id="purchase_date" readonly value="{{ old('purchase_date') }}" /></label>
							</label>
						</div>
						<div class="medium-4 columns">
							<button style="margin-top: 22px;" class="button" type="submit" id="save_purchase">Save</button>
						</div>
					</div>			
					<div class="row">
						<div class="medium-4 columns">
							<label>Search Ingredient
								<input class="add-items mdl-textfield__input" name="search_item" type="text" id="search_item"  />
								<input class="add-items" name="hidden_search_item" type="hidden" id="hidden_search_item" />
							</label>
						</div>
						<div class="medium-2 columns">
							<label>Price
								<input class="add-items" name="price" type="text" id="price" autocomplete="off" type="text" />
							</label>
						</div>
						<div class="medium-2 columns">
							<label>Quantity
								<input class="add-items" pattern="-?[0-9]*(\.[0-9]+)?" name="quantity" type="text" id="quantity" autocomplete="off" />
							</label>
						</div>
						<div class="medium-2 columns">
							<label>Unit
								<input class="add-items" name="unit" type="text" id="unit" autocomplete="off" placeholder="Unit" readonly />
							</label>
						</div>

						<input type="hidden" name="purchased_items_array" id="purchased_items_array" value="" />
						<button type="button" id="add_purchased_items_button" class="add-items button-small" style="margin-top:15px;">
							<i class="fa fa-plus" aria-hidden="true"></i>
						</button>	
					</div>
					<div class="row">
						<div class="medium-10 columns">
							<table id="item_list" width="100%" class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
								<thead>
									<tr>
										<th width="10%" align="center">Action </th>
										<th width="70%">Item </th>
										<th width="10%" class="text-right">Price </th>
										<th width="10%">Quantity </th>
									</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</div>
					{!! csrf_field() !!}
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
		$( function() {
			$( "#purchase_date" ).datepicker(
				{ dateFormat: 'yy-mm-dd' }
			);
		});

		$("#quantity").keyup(function(event){
		    if(event.keyCode == 13){
		        $("#add_purchased_items_button").click();
		    }
		});


	var xhr_item_search = null;
		$("#search_item").autocomplete({
	        source: function (request, response) {
	        	if(xhr_item_search != null)
	        	{
	        		return false;
	        	}
	            xhr_item_search = $.ajax({
	                type : 'get',
	                url : "{{ route('items_json') }}",
	                data : 'search=' + request.term,
	                cache : true,
	                dataType : "json",
	                beforeSend: function(xhr){
	                    if (xhr_item_search != null)
	                    {
	                        xhr_item_search.abort();
	                    }
	                }
	            }).done(function(data){
	            	//console.log(data);
	                response($.map( data, function(value, key){
	                    return { label: value, value: key }
	                   //alert(key);
	                }));
	                xhr_item_search = null;
	            }).fail(function(jqXHR, textStatus){
	                //console.log('Request failed: ' + textStatus);
	            });
	        }, 
	        minLength: 3,
	        autoFocus: true,
	        select: function(event, ui){
	        	console.log(ui);
	            var id = ui.item.value;
	            var name = ui.item.label;
	            //alert($('#search_item').val());
	            id = id.split('~~');
	            $('#search_item').val(name);
	            $('#hidden_search_item').val(id[0]);
	            $('#unit').val(id[1]);
	            return false;
	        }
	    });

	    $('#add_purchased_items_button').click(function(){
	    	var itemid = $('#hidden_search_item').val();
	    	var itemname = $('#search_item').val();
	    	var quantity = $('#quantity').val();
	    	var price = $('#price').val();
	    	var items_array = [];

			if(itemname == ""){
				$('#search_item').focus();
				alert("Please enter an item");
			}else if(itemid == ""){
				alert("Please enter a valid item name.");
			}else if(price == ""){
				$('#price').focus();
				alert("Please enter item price");
			}else if(quantity == ""){
				$('#quantity').focus();
				alert("Please enter quantity");
			}else{
				if(document.getElementById('item-row-'+itemid) == null)
				{
					$('#item_list tbody').append('<tr id="item-row-'+itemid+'"> ' +
						'<td align="center"> '+
							'<a id="delete_item" onclick="DeletePurchasedItem('+itemid+');"> '+
								'<i class="fa fa-trash-o" aria-hidden="true"></i> '+
							'</a>'+ 
						'</td> ' +
						'<td>'+ itemname +'</td> ' +
						'<td class="text-right">'+ price +'</td> ' +
						'<td>'+ quantity +'</td> ' +
					'</tr>');
					$('.add-items').val('');
					$('#search_item').focus();

					if($('#purchased_items_array').val() == "")
					{
						 items_array = {'items':[
								{'item_id':itemid,
								'item_name':itemname,
								'price':price,
								'quantity':quantity}
								]};
						$('#purchased_items_array').val(JSON.stringify(items_array));

					}else{
						console.log($('#purchased_items_array').val());
						items_array = JSON.parse($('#purchased_items_array').val());
						items_array.items.push({'item_id':itemid,'item_name':itemname,'price':price,'quantity':quantity});
						$('#purchased_items_array').val(JSON.stringify(items_array));
					}
				}
				else{
					alert('Ingredient already exists!');
				}
			}			
		});	

		function DeletePurchasedItem(itemid)
		{
			var row = '#item-row-'+itemid;
			$(row).remove();
			$('#search_item').focus();
		}	   	
</script>
@stop