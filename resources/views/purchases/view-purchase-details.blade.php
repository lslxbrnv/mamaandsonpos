@extends('layouts.app')
@section('content')
@parent

	<style></style>
	<div class="row">
		<div class="medium-2 columns"></div>
		<div class="medium-10 columns">
			<a href="{{ route('purchases') }}" class="button small">
	  			<i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;Back</a>
			<h4>Purchase Details</h4>
			<table width="100%">
				<thead>
					<tr>
						<th width="50%">Item Name</th>
						<th class="text-right" width="25%">Quantity</th>
						<th class="text-right" width="25%">Price</th>
					</tr>
				</thead>
				<tbody>
					@forelse($purchases as $purchase)
						<tr>
							<td>{{ $purchase->Items->name }}</td>
							<td align="right">{{ $purchase->quantity }} {{ $purchase->Items->unit }}</td>
							<td align="right">P {{ $purchase->price }}</td>
						</tr>
					@empty
					@endforelse
				</tbody>
			</table>

		</div>
		<div class="mdl-cell mdl-cell--2-col"></div>
	</div>
	
@stop