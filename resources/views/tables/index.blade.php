@extends('layouts.app')
@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Tables</h3>
		<br />
		<div class="row">
	  		<div class="large-4 columns">
	  			<a href="{{ route('add_table_page') }}" class="button small">
	  			<i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Add Table</a>
	  			
	  		</div>
		</div>
		<table align="center" id="menu_table" width="100%">
			<thead>
			<tr>
				<th width="10%"></th>
				<th width="90%">Table Name</th>
			</tr>
			</thead>
			<tbody>
				@forelse ($tables as $table)
				<tr id="table-row-{{ $table->id }}">
					<td align="center">
						<a onclick="DeleteTable({{ $table->id }})" title="Delete Table">
							<i class="fa fa-trash-o" aria-hidden="true"></i>
						</a> &nbsp;	
					</td>
					<td class="mdl-data-table__cell--non-numeric">
						<span class="item-name-current">{{ $table->name }}</span>
					</td>
				</tr> 
				@empty
					<tr id="no-item"><td colspan="3" style="text-align:center;">No Items Yet</td></tr>
				@endforelse
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	function DeleteTable(tableid, selector)
	{
		if(confirm("Are you sure to delete this?") ==  true){
			$('#table-row-'+tableid).remove();
			$.ajax({
				type : 'GET',
				url : '{{ route("delete_table") }}',
				data : 'id='+tableid,
				cache : false,
				dataType : "script"
			}).done(function(response) {
				
			}).fail(function(jqXHR, textStatus, y) {	
				//alert(textStatus);
			});
		}
	}
</script>

@stop