@extends('layouts.app')

@section('content')
@parent
	<div class="mdl-grid">
		<div class="mdl-cell mdl-cell--1-col"></div>
		<div class="mdl-cell mdl-cell--10-col">
			<div class="mdl-grid">
				<div class="mdl-cell mdl-cell--12-col">
					<h3>Terminals</h3>
					<table align="center" width="100%" class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
						<thead>
							<tr>
								<th class="mdl-data-table__cell--non-numeric"width="11%">Edit</th>
								<th class="mdl-data-table__cell--non-numeric"width="30%">Terminal Name</th>
								<th class="mdl-data-table__cell--non-numeric"width="15%">IP Address</th>
								<th class="mdl-data-table__cell--non-numeric"width="15%">HD Serial</th>
								<th class="mdl-data-table__cell--non-numeric"width="15%">Machine ID</th>
								<th class="mdl-data-table__cell--non-numeric"width="7%">Server</th>
								<th class="mdl-data-table__cell--non-numeric"width="7%">Active</th>

							</tr>
						</thead>
						<tbody>
							@forelse ($terminals as $terminal)
							<tr>
								<td class="mdl-data-table__cell--non-numeric">
									<a class="mdl-button mdl-js-button mdl-button--icon" href="{{ route('edit_terminal', $terminal->id) }}">
										<i class="material-icons">mode_edit</i>
									</a>
								</td>
								<td class="mdl-data-table__cell--non-numeric">{{ $terminal->terminal_name }}</td>
								<td class="mdl-data-table__cell--non-numeric">{{ $terminal->ip }}</td>
								<td class="mdl-data-table__cell--non-numeric">{{ $terminal->hd_serial }}</td>
								<td class="mdl-data-table__cell--non-numeric">{{ $terminal->machine_id }}</td>
								@if($terminal->server === 1)
									<td class="mdl-data-table__cell--non-numeric">Yes</td>
								@else
									<td class="mdl-data-table__cell--non-numeric">No</td>
								@endif

								@if($terminal->active === 1)
									<td class="mdl-data-table__cell--non-numeric">Yes</td>
								@else
									<td class="mdl-data-table__cell--non-numeric">No</td>
								@endif
							</tr>	
								@empty
								<tr id="no-item"><td colspan="3" style="text-align:center;">No Terminals Found</td></tr>
							@endforelse
						</tbody>
					</table>
					<div align="center">
					@if(!$terminals->isEmpty())
						{{ $terminals->render() }}
					@endif
					</div>
				</div>
			</div>
		</div>
		<div class="mdl-cell mdl-cell--1-col"></div>
	</div>
	<div aria-live="assertive" aria-atomic="true" aria-relevant="text" id="item-message" class="mdl-js-snackbar mdl-snackbar">
		<div class="mdl-snackbar__text"></div>
		<button class="mdl-snackbar__action" type="button"></button>
	</div>

<script type="text/javascript">

</script>
@stop