@extends('layouts.app')

@section('content')
@parent
	<style>		
		.demo-card-wide.mdl-card {
			
		}
		.demo-card-wide > .mdl-card__supporting-text {
		}
		.alert{
			width:290px;
		}
	</style>
	
	<div class="mdl-grid">
		<div class="mdl-cell mdl-cell--4-col"></div>
		<div class="mdl-cell mdl-cell--4-col"><br /><br /><br />
			@if (count($errors) > 0)
			<div class="alert">
				<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
				<ul>
			        @foreach ($errors->all() as $error)
			            <li>{{ $error }}</li>
			        @endforeach
			    </ul>
			</div>
			@endif
			 @if(Session::has('message'))
			<div class="alert success">
				<span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
				Table has been updated!
			</div>
			 @endif
			<br />
			<div class="demo-card-wide mdl-card mdl-shadow--2dp">
				<div class="mdl-card__title">
					<h2 class="mdl-card__title-text">Edit Table</h2>
				</div>					
				<form id="edit_table" action="{{ route('update_table') }}" method="POST">
					<div class="mdl-card__supporting-text">		
						<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
							<input class="mdl-textfield__input" name="name" type="text" id="name" value="{{ $table->name }}" />
							<label class="mdl-textfield__label" for="name">Table Name</label>
						</div>	
					</div>
					<div class="mdl-card__actions mdl-card--border" align="right">
						<input name="name_hidden" type="hidden" id="name_hidden" value="{{ $table->name }}" />
						<input name="id" type="hidden" id="id" value="{{ $table->id }}"/>
						<a href="{{ route('table-settings') }}" class="mdl-button close">Back</a>
						<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--primary">Update</button>
					</div>
					{!! csrf_field() !!}
				</form>
			</div>
		</div>
		<div class="mdl-cell mdl-cell--4-col"></div>
	</div>
@stop