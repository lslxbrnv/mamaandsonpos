@extends('layouts.app')

@section('content')
@parent
<style>
	.mdl-js-textfield
	{
		margin-right:8px;
	}
</style>
<div class="mdl-grid">
	<div class="mdl-cell mdl-cell--1-col"></div>
	<div class="mdl-cell mdl-cell--10-col">
		<div class="mdl-grid">
			<div class="mdl-cell mdl-cell--10-col">
				<h3>Company</h3>
			</div>
			<div class="mdl-cell mdl-cell--1-col"><br />
			<br />
				<button id="edit" class="mdl-button mdl-js-button mdl-button--raised">
					Edit
				</button>
			</div>
		</div>
		<div class="mdl-grid">
			<div class="mdl-cell mdl-cell--12-col">
				<form action="{{ route('update_company') }}" method="POST">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:610px;" >
						<input value="{{ $company->name or '' }}"  class="mdl-textfield__input" name="name" type="text" id="name" disabled>
						<label class="mdl-textfield__label" for="name">Company Name</label>
					</div> 
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input value="{{ $company->telephone_number or '' }}"  class="mdl-textfield__input" name="telephone_number" type="text" id="telephone_number" disabled>
						<label class="mdl-textfield__label" for="telephone_number">Telephone Number</label>
					</div>

					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input value="{{ $company->tin or '' }}" class="mdl-textfield__input" name="serial_number" type="text" id="serial_number" disabled>
						<label class="mdl-textfield__label" for="serial_number">TIN</label>
					</div>
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input value="{{ $company->serial_number or '' }}"  class="mdl-textfield__input" name="tin" type="text" id="tin" disabled>
						<label class="mdl-textfield__label" for="tin">Serial Number</label>
					</div>
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input value="{{ $company->permit_number or '' }}" class="mdl-textfield__input" name="permit_number" type="text" id="permit_number" disabled>
						<label class="mdl-textfield__label" for="permit_number">Permit Number</label>
					</div>
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label" style="width:610px;">
						<input value="{{ $company->owner or '' }}"  class="mdl-textfield__input" name="owner" type="text" id="owner" disabled>
						<label class="mdl-textfield__label" for="owner">Owner</label>
					</div>
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
						<input value="{{ $company->vat or '' }}"  class="mdl-textfield__input" name="vat" type="text" id="vat" disabled>
						<label class="mdl-textfield__label" for="vat">VAT</label>
					</div>
					<br />
					<div id="form_buttons" align="right" style="margin-right:80px; display:none;">
						<a href="{{ route('settings') }}" type="button" id="cancel_edit" class="mdl-button mdl-js-button mdl-button--raised">
							Cancel
						</a>
						<button  type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-button--colored">
							Save
						</button>
						{!! csrf_field() !!}
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="mdl-cell mdl-cell--1-col"></div>
</div>
	
<script type="text/javascript">
	$('#edit').click(function(){
		$('#edit').hide();
		$('#form_buttons').show();
		$('.mdl-textfield__input').prop("disabled", false);
		$('#name').focus();
	});
</script>
@stop