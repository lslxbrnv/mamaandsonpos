@extends('layouts.app')

@section('content')
@parent
<!-- Starts -->
 <style>
	.demo-card-wide	{
		height:280px;
	}
 </style>
 <div class="mdl-grid">
	<div class="mdl-cell mdl-cell--1-col"></div>
	<div class="mdl-cell mdl-cell--10-col" >
		<h3>Dashboard</h3>
		<div class="mdl-grid">
			<div class="mdl-cell mdl-cell--1-col"></div>
			<div class="mdl-cell mdl-cell-10-col" >
				<div>
					Select Menu to generate chart : 
					<select class="mdl-select__input" id="menu_id" namae="menuid" style="width:300px;">
						@foreach($menus as $menu)
						<option value="{{ $menu->id }}">{{ $menu->menu }}</option>
						@endforeach
					</select>
				</div>
				<div style="width:850px;" id="monthly_sales_per_menu_container"></div><br />
				<div style="width:850px;" id="annual_sales_container"></div>
		</div>
			</div>
			<div class="mdl-cell mdl-cell--1-col"></div>

		</div>
 		
		
	</div>
	<div class="mdl-cell mdl-cell--1-col"></div>
</div>
<script type="text/javascript">
   $(function () {
   		LoadMontlySalesPerMenu($('#menu_id').val());
   		LoadAnnualSalesChart();

   		//Item Change Dropdown
   		$('#menu_id').change(function(){
   			LoadMontlySalesPerMenu($('#menu_id').val());
		});
   		// Load Monthly Sales per Menu Chart
   		function LoadMontlySalesPerMenu(menuid)
   		{
   			if(menuid === null){
   				menuid = $('#menu_id').val();
   			}
			var monthly_sales_per_menu_chart;

			var route_monthly_sales_per_menu = '{{ route("monthly_sales_per_menu_json", ":id" ) }}';
			route_monthly_sales_per_menu = route_monthly_sales_per_menu.replace(':id', menuid);
	        $.getJSON(route_monthly_sales_per_menu, function(jsonData) {
	            monthly_sales_per_menu_chart = new Highcharts.Chart({
	                chart: {
	                    renderTo: 'monthly_sales_per_menu_container',
	                    type: 'column',
	                    marginRight: 130,
	                    marginBottom: 25
	                },
	                title: {
	                    text: 'Orders Per Menu',
	                    x: -20 //center
	                },
	                subtitle: {
	                    text: '',
	                    x: -20
	                },
	                xAxis: {
	                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	                },
	                yAxis: {
	                    title: {
	                        text: 'No. of Menu Ordered'
	                    },
	                    plotLines: [{
	                        value: 0,
	                        width: 1,
	                        color: '#808080'
	                    }]
	                },
	                tooltip: {
	                    formatter: function() {
	                            return '<b>'+ this.series.name +'</b><br/>'+
	                            this.x +': '+ this.y.toFixed(2);
	                    }
	                },
	                legend: {
	                    layout: 'vertical',
	                    align: 'right',
	                    verticalAlign: 'top',
	                    x: -10,
	                    y: 100,
	                    borderWidth: 0
	                },
	                series: jsonData
	            });
	        });

		}
		function LoadAnnualSalesChart()
		{
   		// Load Annual Sales Chart
			var annual_sales_chart;
	        $.getJSON('{{ route("annual_sales_json") }}', function(json) {
	            annual_sales_chart = new Highcharts.Chart({
	                chart: {
	                    renderTo: 'annual_sales_container',
	                    type: 'column',
	                    marginRight: 130,
	                    marginBottom: 25
	                },
	                title: {
	                    text: 'Annual Sales',
	                    x: -20 //center
	                },
	                subtitle: {
	                    text: '',
	                    x: -20
	                },
	                xAxis: {
	                    categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
	                },
	                yAxis: {
	                    title: {
	                        text: 'Total Sales'
	                    },
	                    plotLines: [{
	                        value: 0,
	                        width: 1,
	                        color: '#808080'
	                    }]
	                },
	                tooltip: {
	                    formatter: function() {
	                            return '<b>'+ this.series.name +'</b><br/>'+
	                            this.x +': '+ this.y.toFixed(2);
	                    }
	                },
	                legend: {
	                    layout: 'vertical',
	                    align: 'right',
	                    verticalAlign: 'top',
	                    x: -10,
	                    y: 100,
	                    borderWidth: 0
	                },
	                series: json
	            });
	        });
		}
		/**/
   
    });
</script>
@stop