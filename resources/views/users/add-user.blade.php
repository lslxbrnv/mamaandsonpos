@extends('layouts.app')

@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Add User</h3>
		<br />
		<div class="row">
			<div class="medium-3 columns"></div>
			<div class="medium-9 columns">
				@if (count($errors) > 0)
				<div class="row">
					<div class="medium-8 columns warning callout" data-closable>
						<ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				@endif
				@if(Session::has('message'))
				<div class="row">
					<div class="medium-8 columns success callout" data-closable>
						User has been successfully added!
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				
				</div>
				 @endif
				<form id="add_item" action="{{ route('add_user_submit') }}" method="POST">
					<div class="row">
						<div class="medium-8 columns">
							<label> Name
								<input type="text" name="name" value="{{ old('name') }}" placeholder="Name" autocomplete="off">
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>Username
								<input name="username" type="text" placeholder="Username" autocomplete="off" value="{{ old('username') }}">
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>Password
								<input name="password" type="password" id="password" placeholder="Password" autocomplete="off">
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>Access
								<select id="access" name="access">
									<option value="2" selected>Cashier</option>
									<option value="1">Admin</option>
								</select>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<input type="submit" class="button" value="Add">
							<a class="button secondary" href="{{ route('users') }}">Cancel</a>
						</div>
					</div>
					{!! csrf_field() !!}
				</form>
			</div>
		</div>
	</div>
</div>

@stop