@extends('layouts.app')
@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Edit User</h3>
		<br />
		<div class="row">
			<div class="medium-3 columns"></div>
			<div class="medium-9 columns">
				@if (count($errors) > 0)
				<div class="row">
					<div class="medium-8 columns warning callout" data-closable>
						<ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				@endif
				@if(Session::has('message'))
				<div class="row">
					<div class="medium-8 columns success callout" data-closable>
						User has been successfully updated!
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				
				</div>
				 @endif
				<form id="edit_users" action="{{ route('update_user') }}" method="POST">					
					<div class="row">
						<div class="medium-8 columns">
							<label>Name
								<input name="name" type="text" id="name" value="{{ $user[0]->name }}" autocomplete="off" />
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>Username
								<input name="username" type="text" id="username" value="{{ $user[0]->username }}" autocomplete="off" />

							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>Access
								<select id="access" name="access">
									@if($user[0]->access === 1)
										<option value="1" selected>Admin</option>
										<option value="2">Cashier</option>
									@else
										<option value="1" >Admin</option>
										<option value="2" selected>Cashier</option>
									@endif
								</select>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>Status
								<select id="status" name="status">
								@if($user[0]->status === 1)
									<option value="1" selected>Enabled</option>
									<option value="0">Disabled</option>
								@else
									<option value="1">Enabled</option>
									<option value="0" selected>Disabled</option>
								@endif
							</select>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<input type="hidden" id="id" name="id" value="{{ $user[0]->id }}" />
							<input type="hidden" id="hidden_username" name="hidden_username" value="{{ $user[0]->username }}" />
							<input type="submit" class="button" value="Update">
							<a class="button secondary" href="{{ route('users') }}">Cancel</a>
						</div>
					</div>
					{!! csrf_field() !!}
				</form>
			</div>
		</div>
	</div>
</div>

@stop