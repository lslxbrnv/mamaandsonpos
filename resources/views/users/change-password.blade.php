@extends('layouts.app')
@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Edit Menu</h3>
		<br />
		<div class="row">
			<div class="medium-3 columns"></div>
			<div class="medium-9 columns">
				@if (count($errors) > 0)
				<div class="row">
					<div class="medium-8 columns warning callout" data-closable>
						<ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				@endif
				@if(Session::has('message'))
				<div class="row">
					<div class="medium-8 columns success callout" data-closable>
						User has been successfully updated!
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				
				</div>
				 @endif
				<form id="edit_users" action="{{ route('submit_change_password') }}" method="POST">					
					<div class="row">
						<div class="medium-8 columns">
							<label>New Password
								<input name="password" type="password" id="password" value="{{ old('password') }}" autocomplete="off" />
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>Confirm New Password
								<input  name="password_confirmation" type="password" id="password_confirmation" value="{{ old('password_confirmation') }}" autocomplete="off" />

							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<input type="hidden" name="id" value="{{ $userid }}" />
							<input type="submit" class="button" value="Change Password">
							<a class="button secondary" href="{{ route('users') }}">Cancel</a>
						</div>
					</div>

					{!! csrf_field() !!}
				</form>
			</div>
		</div>
	</div>
</div>

@stop