@extends('layouts.app')

@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Users</h3>
		<br />
		<div class="row">
	  		<div class="large-4 columns">
	  			<a href="{{ route('add_user_page') }}" class="button small">
	  			<i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Add New User</a>
	  		</div>
		</div>
		<table align="center" id="item_table" width="100%">
			<thead>
			<tr>
				<th width="10%"></th>
				<th width="40%">Name</th>
				<th width="30%">Username</th>
				<th width="10%">Access</th>
				<th width="10%">Status</th>
			</tr>
			</thead>
			<tbody>
			@forelse($users as $user)
			<tr id="item-row-{{ $user->id }}">
				<td class="text-center">
					@if($user->username != 'admin')
					<a href="{{ route('edit_user', $user->id ) }}" title="Edit User" id="edit_user{{ $user->id }}">
						<i class="fa fa-pencil" aria-hidden="true"></i>
					</a> &nbsp;	
					@endif
					<a href="{{ route('change_password', $user->id ) }}" id="change_password{{ $user->id }}" title="Change Password">
						<i class="fa fa-key" aria-hidden="true"></i>
					</a>
				</td>
				<td>
					<span>{{ $user->name }}</td></span>
				</td>
				<td>
					<span>{{ $user->username }}</td></span>
				</td>
				@if( $user->access == 1)
				<td >Admin</td>
				@else

				<td>Cashier</td>
				@endif
				@if( $user->status == 1)
				<td>Active</td>
				@else

				<td>Disabled</td>
				@endif

			</tr> 
			@empty
				<tr id="no-ingredient"><td colspan="4" style="text-align:center;">No Ingredients Yet</td></tr>

			@endforelse
			</tbody>
		</table>
	</div>
</div>
<script type="text/javascript"></script>

@stop