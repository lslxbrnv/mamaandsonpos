@extends('layouts.app')

@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3><i class="fa fa-cube" aria-hidden="true"></i> &nbsp; Items</h3>
		<br />
		<div class="row">
	  		<div class="large-4 columns">
	  			<a href="{{ route('add_item_page') }}" class="button small">
	  			<i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Add Ingredient</a>
	  		</div>
			<div class="large-4 large-offset-4 columns">
				<form action="" method="get">
					<input type="text" id="search" name="search" placeholder="Search Ingredient" value="{{ $search }}" />
				</form>
			</div>
		</div>
		<table align="center" id="item_table" width="100%">
			<thead>
			<tr>
				<th width="10%"></th>
				<th width="50%">Item Name</th>
				<th width="30%">Unit of Measure</th>
				<th width="10%">Quantity</th>
			</tr>
			</thead>
			<tbody>
			@forelse ($items as $item)
			<tr id="item-row-{{ $item->id }}">
				<td class="text-center">
					<a onclick="DeleteItem({{ $item->id }})" id="delete_item{{ $item->id }}" title="Delete Ingredient">
						<i class="fa fa-trash-o" aria-hidden="true"></i>
					</a> &nbsp;	
					<a href="{{ route('edit_item', $item->id ) }}" id="edit_item{{ $item->id }}" title="Edit Ingredient">
						<i class="fa fa-pencil" aria-hidden="true"></i>
					</a>&nbsp;	
					<a href="{{ route('inventory', $item->id) }}" title="Ingredient Inventory History">
						<i class="fa fa-history" aria-hidden="true"></i>
					</a>
				</td>
				<td>
					<span class="item-name-current">{{ $item->name }}</span>
				</td>
				<td>
					<span class="item-unit-current">{{ $item->unit }}</span>
				</td>
				<td>{{ $item->current_quantity }}</td>
			</tr> 
			@empty
				<tr id="no-ingredient"><td colspan="4" style="text-align:center;">No Ingredients Yet</td></tr>

			@endforelse
			</tbody>
		</table>
		<div align="center">
			@if(!$items->isEmpty())
			{{ $items->render() }}
			@endif
		</div>
	</div>
</div>
<script type="text/javascript">
	function DeleteItem(itemid, selector)
	{
		if(confirm("Are you sure to delete this?") ==  true){
			$.ajax({
				type : 'GET',
				url : '{{ route("delete_item") }}',
				data : 'item_id='+itemid,
				cache : false,
				dataType : "script"
			}).done(function(response) {
				$(selector).closest("tr").remove();
				$('#item-row-'+itemid).remove();
			}).fail(function(jqXHR, textStatus, y) {	
				//alert(textStatus);
			});
		}
	}
</script>

@stop