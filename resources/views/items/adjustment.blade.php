@extends('layouts.app')

@section('content')
@parent
	<style></style>
	<div class="mdl-grid">
		<div class="mdl-cell mdl-cell--1-col"></div>
		<div class="mdl-cell mdl-cell--10-col">
			<h3>Adjustments</h3>
			<div class="mdl-grid">
				<div class="mdl-select mdl-js-select mdl-select--floating-label">
					<select class="mdl-select__input" id="method" name="method">
						<option value=""></option>
						<option value="0">Add</option>
						<option value="1">Deduct</option>
					</select>
					<label id="select_label" class="mdl-select__label" for="method">Method</label>
				</div>	
				<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
					<input class="mdl-textfield__input" name="name" type="text" id="name" value="" />
					<label class="mdl-textfield__label" for="name">Quantity</label>
				</div>	
				
			</div>
		</div>
		<div class="mdl-cell mdl-cell--1-col"></div>
	</div>
	<script type="text/javascript">
		$("#method").change(function(){
			if($('#method').val() != "")
			{
				$('#select_label').hide();
			}else{
				$('#select_label').show();
			}
		});
	</script>
@stop