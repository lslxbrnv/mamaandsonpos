@extends('layouts.app')

@section('content')
@parent
<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Edit Ingredient</h3>
		<br />
		<div class="row">
			<div class="medium-3 columns"></div>
			<div class="medium-9 columns">
				@if (count($errors) > 0)
				<div class="row">
					<div class="medium-8 columns success callout" data-closable>
						<ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				@endif
				@if(Session::has('message'))
				<div class="row">
					<div class="medium-8 columns success callout" data-closable>
						Item has been updated!
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				
				</div>
				 @endif
				<form id="edit_item" action="{{ route('update_item') }}" method="POST">
					<div class="row">
						<div class="medium-8 columns">
							<label>Item Name
								<input name="name" type="text" id="name" value="{{ $items->name }}" placeholder="Item Name" autocomplete="off">
								<input name="name_hidden" type="hidden" id="name_hidden" value="{{ $items->name }}" />
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>Unit of Measure
								<input name="unit" type="text" id="unit" value="{{ $items->unit }}" placeholder="Unit of Measure" autocomplete="off">
								<input name="id" type="hidden" id="id" value="{{ $items->id }}"/>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<input type="submit" class="button" value="Update">
							<a class="button secondary" href="{{ route('items') }}">Cancel</a>
						</div>
					</div>
					{!! csrf_field() !!}
				</form>
			</div>
		</div>
	</div>
</div>
@stop