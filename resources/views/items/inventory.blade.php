@extends('layouts.app')

@section('content')
@parent
	<div class="row">
		<div class="medium-2 columns"></div>
		<div class="medium-10 columns"><br />
			<div class="row">
				<div class="medium-1 columns">
					<a class="button secondary" href="{{ route('items') }}" title="Back">
						<i class="fa fa-arrow-left" aria-hidden="true"></i>
					</a>
				</div>
				<div class="medium-10 columns">
					<h4>Ingredient <U>{{ strtoupper($item->name) }}</U> History</h4>
					<table width="100%">
						<thead>
							<tr>
								<th width="20%">Quantity</th>
								<th width="20%">Method</th>
								<th width="30%">Date Tracked</th>
								<th width="30%">User</th>
							</tr>
						</thead>
						<tbody>
							@forelse($stocks as $row)
							@if($row['name'] == '')
							<tr style="color:green">
							@else
							<tr style="color:red">
							@endif
								<td>{{ $row['quantity'] }}</td>
								<td>{{ $row['method'] }}</td>
								<td>{{ $row['transaction_on'] }}</td>
								<td>{{ $row['name'] }}</td>
							</tr>
							@empty
								<tr><td colspan="4" style="text-align:center;">No History Yet</td></tr>
							@endforelse
						</tbody>
					</table>
					<div align="center">
						@if(!$stocks->isEmpty())
							{!! str_replace('/?', '?', $stocks->render()) !!}
						@endif
					</div>	
				</div>
		<!-- <div class="mdl-cell mdl-cell--1-col">
			<button class="mdl-button mdl-button--fab mdl-button--primary" title="Add Inventory" style="right:0;bottom:0; position:absolute; margin:25px; "> 
				<i class="material-icons">add</i>
			</button>
		</div> -->
			</div>
		</div>
	</div>
			

@stop