@extends('layouts.app')

@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Add Ingredient</h3>
		<br />
		<div class="row">
			<div class="medium-3 columns"></div>
			<div class="medium-9 columns">
				@if (count($errors) > 0)
				<div class="row">
					<div class="medium-8 columns warning callout" data-closable>
						<ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				@endif
				@if(Session::has('message'))
				<div class="row">
					<div class="medium-8 columns success callout" data-closable>
						Ingredient has been successfully updated!
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				
				</div>
				 @endif
				<form id="add_item" action="{{ route('add_item') }}" method="POST">
					<div class="row">
						<div class="medium-8 columns">
							<label>Item Name
								<input name="name" type="text" placeholder="Item Name" autocomplete="off" value="{{ old('name') }}">
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>Unit of Measure
								<input name="unit" type="text" placeholder="Unit of Measure" autocomplete="off" value="{{ old('unit') }}">
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<input type="submit" class="button" value="Add">
							<a class="button secondary" href="{{ route('items') }}">Cancel</a>
						</div>
					</div>
					{!! csrf_field() !!}
				</form>
			</div>
		</div>
	</div>
</div>

@stop