@extends('layouts.empty')

@section('content')
@parent
<style type="text/css">
body{
	font-size: 12px!important
}
.mdl-grid{
	margin: 0;
	padding: 0;
}
.mdl-grid .mdl-cell{
	padding-top: 0;
	padding-bottom: 0;
	margin-top: 0;
	margin-bottom: 0;
}
table th,
table td,
table tr{
	padding: 0!important;
	margin: 0!important;
	height: 0px!important;
}
table td{
	border: none!important;
}
table th{
	border-top: 1px solid rgba(0, 0, 0, 0.12);
	border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
</style>
	<br />
	<table class="mdl-data-table mdl-js-data-table" width="100%">
		<thead>
			<tr>
				<th class="text-left">Table No : <strong>{{ $cash_register->table_reverse->name }} </strong></th>
				<th class="text-right">Customer Name :  <strong>{{ $cash_register->customer_name }} </strong></th>
			</tr>
			<tr>
				<th class="text-left">&nbsp;</th>
				<th class="text-right">&nbsp;</th>
			</tr>
			<tr>
				<th class="text-left">Menu </th>
				<th class="text-right">Selling Price </th>
			</tr>
		</thead>
		<tbody>
			<?php 
				$total = 0;
			?>
			@foreach($cash_register_menu as $menu)
			<?php 

				$total  += $menu->pricing->price;
			?>
			<tr>
				<td class="text-left">{{$menu->pricing->menu->menu}}</td>
				<td class="text-right">{{$menu->pricing->price}}</td>

			</tr>
			@endforeach
			<tr>
				<td>&nbsp;	</td>
				<td>&nbsp;</td>
			</tr>
			<tr>
				<td class="text-left"><strong>Total : </strong></td>
				<td class="text-right"><strong>{{ number_format($total,2) }}</strong></td>
			</tr>
		</tbody>
	</table>
	<br />
	
	<script type="text/javascript">
	$(document).ready(function(){
		setTimeout(function(){ window.print(); 	}, 1000);
		setTimeout(function(){ close(); 		}, 2000);
	});
	</script>
@endsection