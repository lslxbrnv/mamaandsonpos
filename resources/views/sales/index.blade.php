<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Mama and Sons POS</title>
    <style>
        body{
            background-color:#f3f3f3;
        }
    </style>
    <!-- Fonts -->
   <!--  <link rel="stylesheet" href="{{ asset('components/foundation-sites/dist/css/foundation.min.css') }}" >
    <link rel="stylesheet" href="{{ asset('components/foundation-sites/dist/css/foundation-flex.min.css') }}" >
    <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" >

    <link rel="stylesheet" href="{{ asset('components/jquery/dist/jquery-ui.css') }}" -->

    <!-- JavaScripts 
    <script src="{{ asset('components/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('components/jquery/dist/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('components/foundation-sites/dist/js/foundation.min.js') }}"></script> 

    <script src="{{ asset('components/what-input/dist/what-input.min.js') }}"></script>-->
    
    <link rel="stylesheet" href="{{ asset('css/all.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/styles.css') }}" />

    <script src="{{ asset('js/all.js') }}"></script>
    <script src="{{ asset('js/custom.js') }}"></script>

	<style>
	#app-layout{
		background-color: #ccc;
	}
	.page-content{
		height: 97%!important;
	}
	.mdl-grid{
		height: 98%!important;
	}
	.demo-card-wide.mdl-card{
		width: 99%;
	}
    .demo-card-wide > .mdl-card__title{
        color: #fff;
        background: url({{ asset('images/brand.png') }}) ;
		background-size:300px;
		background-position:center;
        background-repeat: no-repeat;
    }
	#search_menu{
		width: 100%;
	}
	#leftCard{
		height: 100%;
	}
	#leftCard_supportingText{
		height: 100%;
		width: 96.6%!important;
		overflow-y: scroll;
	}
	.paddingTopNone{
		padding-top: 0;
	}
	#amount_due_row{
		padding-top: 0!important;
	}
	#amount_tendered_row{
		padding-top: 0!important;
	}
	.mdl-expand {
		width: 100%;
	}
	.mdl-default{
		color: #fff;
		background-color: #008cba;
    	border-color: #007095;
	}
	.mdl-default:hover{
		background-color: #007095;
		border-color: #007095;
	}
	.mdl-secondary{
		color: #333;
		background-color: #e7e7e7;
    	border-color: #b9b9b9;
	}
	.mdl-secondary:hover{
		background-color: #b9b9b9;
		border-color: #b9b9b9;
	}
	.mdl-success{
		color: #fff;
		background-color: #43ac6a;
		border-color: #368a55;
	}
	.mdl-success:hover{
		background-color: #368a55;
		border-color: #368a55;
	}
	.mdl-alert{
		color: #fff;
		background-color: #f04124;
    	border-color: #cf2a0e;
	}
	.mdl-alert:hover{
		background-color: #cf2a0e;
		border-color: #cf2a0e;
	}
	#headerGuide{
		font-size: 12px!important;
		background-color: #fff;
		margin: 0!important;
	}
	#headerGuide .mdl-grid{
		margin: 0!important;
		padding: 0!important;
	}
	#headerGuide .mdl-cell{
		margin: 0!important;
	}
	#headerGuide .mdl-cell span{
		padding-left: 20px;
		padding-right: 20px;
		border-right: 1px solid #ccc;
	}
	#f1{
		padding-left: none!important;
	}
	#f12{
		border-right: none!important;
	}
	#userName{
		margin-right: -10px;
	}
	#dateTime{
		margin-right: -10px;
	}
	#menuListTable td.borderNone{
		border: none!important;
		background-color: rgba(0, 0, 0, 0.10);
		padding: 0!important;
	}
	#menuListTable .demo-list-item{
		margin: 0!important;
	}
	#menuListTable .mdl-list__item{
		padding-top: 0!important;
		padding-bottom: 0!important;
		padding-right: 0!important;
		min-height: 0!important
	}
	#rightLogoAndTables{
		height: 100%;
	}
	#posBanner{
		height: 26%;
	}
	#order_type{
		height: 4%;
	}
	#tables{
		height: 70%;
		overflow-y: scroll;
		border-top: 1px solid rgba(0, 0, 0, 0.12);
	}
	.pointer{
		cursor: pointer;
	}
	/*#menuListTable td{
		border: none!important;
	}*/
	</style>
</head>
<body id="app-layout">
	<div class="mdl-layout mdl-js-layout mdl-layout--fixed-header">
		<main class="mdl-layout__content">
	    	<div class="page-content">
				<div id="headerGuide" class="mdl-shadow--6dp">
		    		<div class="mdl-grid">
						<div class="mdl-cell mdl-cell--8-col">
							<span id="f1">F1</span> <div class="mdl-tooltip" for="f1">Focus Search Menu</div>
							<span id="f2">F2</span> <div class="mdl-tooltip" for="f2">Focus Customer Name</div>
							<span id="f3">F3</span> <div class="mdl-tooltip" for="f3">Focus Amount Tendered</div>
							<span id="f4">F4</span> <div class="mdl-tooltip" for="f4">Checkout/Complete Sale</div>
							<span id="f6">F6</span> <div class="mdl-tooltip" for="f6">Cancel Transaction</div>
							<span id="f12">F12</span> <div class="mdl-tooltip" for="f12">Logout</div>
						</div>
						<div class="mdl-cell mdl-cell--4-col text-right">
							{{ Auth::user()->name }}
						</div>
						<!-- <div class="mdl-cell mdl-cell--2-col text-right">
							<a href="#" id="dateTime"></a>
						</div> -->
					</div>
				</div>
	    		<div class="mdl-grid">
	    			<!-- Left -->
	    			<div class="mdl-cell mdl-cell--9-col">
		                <div id="leftCard" class="demo-card-wide mdl-card mdl-shadow--6dp">
		                    <div id="leftCard_supportingText" class="mdl-card__supporting-text">
								<form id="billout" action="{{ route('billout') }}" method="get" class="">
	    							<div class="mdl-grid">
	    								<!-- Row 1 -->
										<div class="mdl-textfield mdl-js-textfield mdl-cell mdl-cell--2-col paddingTopNone">
									    	<input class="mdl-textfield__input text-center" type="text" id="show_table_number" name="show_table_number" autocomplete="off" placeholder="Table #" title="Table #" readonly="readonly">
									    	<label class="mdl-textfield__label" for="show_table_number"></label>
										</div>
										<div class="mdl-textfield mdl-js-textfield mdl-cell mdl-cell--2-col paddingTopNone">
									    	<input class="mdl-textfield__input" type="text" id="show_transaction_type" name="show_transaction_type" autocomplete="off" placeholder="Type" title="Type" readonly="readonly">
									    	<label class="mdl-textfield__label" for="show_transaction_type"></label>
										</div>
										<div class="mdl-textfield mdl-js-textfield mdl-cell mdl-cell--8-col paddingTopNone">
									    	<input class="mdl-textfield__input" type="text" id="show_customer_name" name="show_customer_name" autocomplete="off" placeholder="Customer Name" title="Customer Name" readonly="readonly">
									    	<label class="mdl-textfield__label" for="show_customer_name"></label>
										</div>
	    								<!-- Row 2 -->
										<div class="mdl-textfield mdl-js-textfield mdl-cell mdl-cell--3-col paddingTopNone">
									    	<input class="mdl-textfield__input text-right" type="text" id="show_amount_due" name="show_amount_due" autocomplete="off" placeholder="Amount Due" title="Amount Due" readonly="readonly">
									    	<label class="mdl-textfield__label" for="show_amount_due"></label>
										</div>
										<div class="mdl-textfield mdl-js-textfield mdl-cell mdl-cell--3-col paddingTopNone">
									    	<input class="mdl-textfield__input text-right" type="text" id="discount_amount" name="discount_amount" autocomplete="off" placeholder="Discount" title="Discount" readonly="readonly">
									    	<label class="mdl-textfield__label" for="discount_amount"></label>
										</div>
										<div class="mdl-textfield mdl-js-textfield mdl-cell mdl-cell--3-col paddingTopNone">
									    	<input class="mdl-textfield__input text-right" type="text" id="show_total_amount_due" name="show_total_amount_due" autocomplete="off" placeholder="Total" title="Total" readonly="readonly">
									    	<label class="mdl-textfield__label" for="show_total_amount_due"></label>
										</div>
										<div class="mdl-textfield mdl-js-textfield mdl-cell mdl-cell--3-col paddingTopNone">
									    	<input class="mdl-textfield__input text-right" type="text" id="amount_tendered" name="amount_tendered" autocomplete="off" placeholder="Amount Tendered" title="Amount Tendered">
									    	<label class="mdl-textfield__label" for="amount_tendered"></label>
										</div>
	    								<!-- Row 3 -->
										<div class="mdl-textfield mdl-js-textfield mdl-cell mdl-cell--6-col paddingTopNone">
											&nbsp;{!! csrf_field() !!}
	                            			<select id="payment_type" name="payment_type" class="hide"><option value="CASH">CASH</option></select>
											<input type="hidden" name="cash_register_id">
											<input type="hidden" name="total_amount_due">
										</div>
										<div class="mdl-textfield mdl-js-textfield mdl-cell mdl-cell--2-col paddingTopNone">
											<button id="menu_verify" type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect" >
												<i class="material-icons">assignment</i> Verify Menu
											</button>
										</div>
										<div class="mdl-textfield mdl-js-textfield mdl-cell mdl-cell--2-col paddingTopNone">
											<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-success">
												<i class="material-icons">checked</i> Billout
											</button>
										</div>
										<div class="mdl-textfield mdl-js-textfield mdl-cell mdl-cell--2-col paddingTopNone">
											<button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-alert" onclick="menu_auth_cancel()">
												<i class="material-icons">close</i> Cancel
											</button>
										</div>
									</div>
								</form>
								<form id="orderedMenu" action="{{ route('menuSaleAdd') }}" method="get" class="">
	    							<div class="mdl-grid">
										<div class="mdl-textfield mdl-js-textfield mdl-cell mdl-cell--9-col paddingTopNone">
									    	<input class="mdl-textfield__input" type="text" id="search_menu" name="search_menu" autocomplete="off" placeholder="Search Menu...">
									    	<label class="mdl-textfield__label" for="search_menu"></label>
										</div>
										<div class="mdl-textfield mdl-js-textfield mdl-cell mdl-cell--2-col paddingTopNone">
									    	<input class="mdl-textfield__input" type="number" id="search_quantity" name="search_quantity" autocomplete="off" value="1" min="1">
									    	<label class="mdl-textfield__label" for="search_quantity"></label>
									    	<span class="mdl-textfield__error">Input is not a number!</span>
										</div>
										<div class="mdl-textfield mdl-js-textfield mdl-cell mdl-cell--1-col paddingTopNone">
									    	<input type="hidden" name="menu_id" id="menu_id" value="">
											<input type="hidden" name="cash_register_id" id="cash_register_id" value="">
											<button class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored" type="submit">
												<i class="material-icons">add_shopping_cart</i>
											</button>
										</div>
									</div>
									
									{!! csrf_field() !!}
								</form>
								<table id="menuListTable" class="mdl-data-table mdl-js-data-table" width="100%">
									<thead>
									    <tr>
									    	<th width="10%"></th>
									    	<th width="60%" class="mdl-data-table__cell--non-numeric">Menu</th>
									    	<th width="15%">Price</th>
									    	<th width="15%">Total</th>
									    </tr>
									</thead>
									<tbody></tbody>
								</table>
		                    </div>
		                </div>
	    			</div>
	    			<!-- Right -->
					<div class="mdl-cell mdl-cell--3-col">
		                <div id="rightLogoAndTables" class="demo-card-wide mdl-card mdl-shadow--6dp">
		                    <div id="posBanner" class="mdl-card__title">
		                        <h2 class="mdl-card__title-text"></h2>
		                    </div>
		                    <div id="order_type" class="mdl-card__supporting-text text-center">
								<button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-default">
									Dine In
								</button>
								<button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-secondary" onclick="show_customer_transaction(1, 'TAKE OUT', 'TAKE OUT')">
									Take Out
								</button>
		                    </div>
		                    <div id="tables" class="mdl-card__supporting-text"></div>
		                </div>
					</div>
	    		</div>
	    	</div>
		</main>
		<!-- Modal Authenticate -->
		<dialog id="auth_admin_dialog" class="mdl-dialog">
			<form id="auth_admin_form" action="#" method="post" >
				<div class="mdl-dialog__title">
					<h4>Authentication</h4>
				</div>
				<div class="mdl-dialog__content">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
						<input class="mdl-textfield__input" name="username" type="password" id="username" autocomplete="off" placeholder="Username"/>
	    				<label class="mdl-textfield__label" for="username">&nbsp;</label>
	  				</div>
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
						<input class="mdl-textfield__input" name="password" type="password" id="password" autocomplete="off" placeholder="Password"/>
	    				<label class="mdl-textfield__label" for="password">&nbsp;</label>
	  				</div>			
				</div>
				<div class="mdl-dialog__actions ">
					{!! csrf_field() !!}
					<input class="mdl-textfield__input" name="menu_id" type="hidden"/>
					<input class="mdl-textfield__input" name="type" type="hidden"/>
					<button type="button" class="mdl-button close">Cancel</button>
					<button type="submit" class="mdl-button mdl-button-primary">Submit</button>
				</div>
			</form>
		</dialog>
		<!-- Modal Fillup form -->
		<dialog id="customer_transaction_dialog" class="mdl-dialog">
			<form id="customer_transaction_form" action="#" method="psot" >
				<div class="mdl-dialog__title">
					<h4>New Transaction</h4>
				</div>
				<div class="mdl-dialog__content">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
						<input class="mdl-textfield__input" id="table_id" name="table_id" type="hidden"/>
						<input class="mdl-textfield__input" name="table_number" type="text" id="table_number" autocomplete="off" placeholder="Table Number" readonly="readonly" />
	    				<label class="mdl-textfield__label" for="table_number">&nbsp;</label>
	  				</div>
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
						<input class="mdl-textfield__input" name="transaction_type" type="text" id="transaction_type" autocomplete="off" placeholder="Transction Type" readonly="readonly" />
	    				<label class="mdl-textfield__label" for="transaction_type">&nbsp;</label>
	  				</div>	
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
						<input class="mdl-textfield__input" name="customer_name" type="text" id="customer_name" autocomplete="off" placeholder="Customer Name"/>
	    				<label class="mdl-textfield__label" for="customer_name">&nbsp;</label>
	  				</div>			
				</div>
				<div class="mdl-dialog__actions ">
					{!! csrf_field() !!}
					<button type="button" class="mdl-button close">Cancel</button>
					<button type="submit" class="mdl-button mdl-button-primary">Submit</button>
				</div>
			</form>
		</dialog>
		<!-- Modal Discount form -->
		<dialog id="discount_dialog" class="mdl-dialog">
			<form id="discount_form" action="#" method="psot" >
				<div class="mdl-dialog__title">
					<h4>Discount</h4>
				</div>
				<div class="mdl-dialog__content">
					<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label ">
						<input class="mdl-textfield__input" name="cash_register_discount" type="number" id="cash_register_discount" autocomplete="off" placeholder="Discount" maxlength="3" />
	    				<label class="mdl-textfield__label" for="cash_register_discount">&nbsp;</label>
	  				</div>			
				</div>
				<div class="mdl-dialog__actions ">
					{!! csrf_field() !!}
					<input type="hidden" name="cash_register_discount_id" id="cash_register_discount_id" />
					<button type="button" class="mdl-button close">Cancel</button>
					<button type="submit" class="mdl-button mdl-button-primary">Submit</button>
				</div>
			</form>
		</dialog>

	</div>
	<script type="text/javascript" src="{{ asset('js/cash-register.min.js') }}"></script>
	<script type="text/javascript">
		
		var xhr_ordered_menu = null;
		var xhr_menu_search = null;
		var xhr_menu_delete = null;
		var xhr_menu_list = null;
		var xhr_auth_admin = null;
		var xhr_billout = null;
		var xhr_menu_cancel = null;
		var xhr_datetime = null;
		var xhr_remove_item = null;
		var xhr_tables = null;
		var xhr_add_customer = null;
		var xhr_show_customer_transaction = null;
		var xhr_discount_form = null;

		var tables_url = "{{ route('tables') }}";
		var add_customer_url = "{{ route('addCustomer') }}";
		var customer_transaction_url = "{{ route('customerTransaction') }}";
		var discount_url = "{{ route('addDiscount') }}";

		var auth_admin_url = "{{ route('authAdmin') }}";
		var menu_json_url = "{{ route('menuDataJson') }}";
		var menu_order_url = "{{ route('menuSaleList') }}";
		var menu_delete_url = "{{ route('menuSaleDelete') }}";
		var menu_cancel_url = "{{ route('menuSaleCancel') }}";
		var logout_url = "{{ route('logout') }}";
		var datetime_url = "{{ route('datetime') }}";
		var or_url = "{{ route('officialReceiptPrint') }}";
		var verify_url = "{{ route('verify') }}";
		var remove_item_url = "{{ route('removeItem') }}";

		tables(0);

		$('#menu_verify').click(function(e){
			if($("input[name='cash_register_id']").val() != ''){
				cash_reg_id = $("input[name='cash_register_id']").val();
				window.open( verify_url + "/" + cash_reg_id, '_blank' );
			}else{
				alert('No table selected!');
			}
		});
	</script>
</body>
</html>
