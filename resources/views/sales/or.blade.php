@extends('layouts.empty')

@section('content')
@parent
<style type="text/css">
body{
	font-size: 12px!important
}
.mdl-grid{
	margin: 0;
	padding: 0;
}
.mdl-grid .mdl-cell{
	padding-top: 0;
	padding-bottom: 0;
	margin-top: 0;
	margin-bottom: 0;
}
table th,
table td,
table tr{
	padding: 0!important;
	margin: 0!important;
	height: 0px!important;
}
table td{
	border: none!important;
}
table th{
	border-top: 1px solid rgba(0, 0, 0, 0.12);
	border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}
</style>
	
	<div class="mdl-grid">
		<div class="mdl-cell mdl-cell--12-col text-center">{{ $company->name }}</div>
		<div class="mdl-cell mdl-cell--12-col text-center">{{ $company->address }}</div>
		<div class="mdl-cell mdl-cell--12-col text-center">Tel. No.: {{ $company->telephone_number }}</div>
		<div class="mdl-cell mdl-cell--12-col text-center">TIN: {{ $company->tin }}</div>
		<div class="mdl-cell mdl-cell--12-col text-center">S/N: {{ $terminal->hd_serial or "serial" }}</div>
		<div class="mdl-cell mdl-cell--12-col text-center">Owner: {{ $company->owner }}</div>
		<div class="mdl-cell mdl-cell--12-col text-center">Permit No.: {{ $company->permit_number }}</div>
		<div class="mdl-cell mdl-cell--12-col text-center">MIN: {{ $terminal->machine_id or "test" }}</div>
	</div>
	<br />
	<table class="mdl-data-table mdl-js-data-table" width="100%">
		<thead>
			<tr>
				<td class="text-left" colspan="1">O.R. No.</td>
				<td class="text-left" colspan="3">{{ $transaction->official_receipt_format }}</td>
			</tr>
			<tr>
				<td class="text-left" colspan="1">Table #</td>
				<td class="text-left" colspan="3">{{ $transaction->tables->name }}</td>
			</tr>
			<tr>
				<td class="text-left" colspan="1">Customer</td>
				<td class="text-left" colspan="3">{{ $transaction->customer_name }}</td>
			</tr>
			<tr>
				<td class="text-left" colspan="1">Staff</td>
				<td class="text-left" colspan="3">{{ $transaction->user->name }}</td>
			</tr>
			<tr>
				<td class="text-left" colspan="1">Date</td>
				<td class="text-left" colspan="3">{{ Carbon\Carbon::parse($transaction->transaction_datetime)->toDayDateTimeString() }}</td>
			</tr>
		    <tr>
		    	<th width="35%" class="mdl-data-table__cell--non-numeric">Description</th>
		    	<th width="25%" class="text-right">Price</th>
		    	<th width="15%" class="text-center">Qty</th>
		    	<th width="25%" class="text-right">Total</th>
		    </tr>
		</thead>
		<tbody>
			<!-- Start Loop -->
			@foreach($menus as $menu)
			<?php
			$total  += $menu->total_price;
			$vat  	+= $menu->vat_amount;
			#echo $menu->vat_amount;
			?>
			<tr>
				<td colspan="4" class="text-left">{{ $menu->pricing->menu->menu }} - {{ $menu->pricing->sizes->size }}</td>
			</tr>
			<tr>
				<td></td>
				<td class="text-right">{{ number_format($menu->price, 2) }}</td>
				<td class="text-center">{{ $menu->quantity }}</td>
				<td class="text-right">{{ number_format($menu->total_price, 2) }}</td>
			</tr>
			@endforeach
			<?php
			$discount = $total * $discount;
			$total -= $discount;
			?>
			<!-- End Loop -->
			<tr>
				<td colspan="4">&nbsp;</td>
			</tr>
			<tr class="afterItemList">
				<td class="text-right" colspan="2">Discount</td>
				<td class="text-right" colspan="2">{{ number_format($discount, 2) }}</td>
			</tr>
			<tr>
				<td class="text-right" colspan="2">Total</td>
				<td class="text-right" colspan="2">{{ number_format($total, 2) }}</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td class="text-right" colspan="2">Tendered</td>
				<td class="text-right" colspan="2">{{ number_format($tendered, 2) }}</td>
			</tr>
			<tr>
				<td class="text-right" colspan="2">Change</td>
				<td class="text-right" colspan="2">{{ number_format( ( $tendered - $total ), 2) }}</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<tr>
				<td class="text-right" colspan="2">VATable</td>
				<td class="text-right" colspan="2">{{ number_format($vat, 2) }}</td>
			</tr>
			<tr>
				<td class="text-right" colspan="2">VAT</td>
				<td class="text-right" colspan="2">{{ number_format( ($total - $vat), 2) }}</td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
		</tbody>
	</table>
	<br />
	<div class="mdl-grid">
		<div class="mdl-cell mdl-cell--12-col text-center">THANK YOU, COME AGAIN!</div>
		<div class="mdl-cell mdl-cell--12-col text-center">THIS SERVE AS AN OFFICIAL RECEIPT</div>
	</div>
	<script type="text/javascript">
	$(document).ready(function(){
		setTimeout(function(){ window.print(); 	}, 1000);
		setTimeout(function(){ close(); 		}, 2000);
	});
	</script>
@endsection