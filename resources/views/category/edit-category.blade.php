@extends('layouts.app')
@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Edit Category</h3>
		<br />
		<div class="row">
			<div class="medium-3 columns"></div>
			<div class="medium-9 columns">
				@if (count($errors) > 0)
				<div class="row">
					<div class="medium-8 columns warning callout" data-closable>
						<ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				@endif
				@if(Session::has('message'))
				<div class="row">
					<div class="medium-8 columns success callout" data-closable>
						Category has been successfully updated!
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				
				</div>
				 @endif
				<form id="edit_category" action="{{ route('update_category') }}" method="POST">
					<div class="row">
						<div class="medium-8 columns">
							<label>Category Name
								<input name="name" type="text" placeholder="Category Name" autocomplete="off" value="{{ $category->name }}">
								<input name="name_hidden" type="hidden" id="name_hidden" value="{{ $category->name }}" />
								<input name="id" type="hidden" id="id" value="{{ $category->id }}"/>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<input type="submit" class="button" value="Update">
							<a class="button secondary" href="{{ route('category') }}">Cancel</a>
						</div>
					</div>
					{!! csrf_field() !!}
				</form>
			</div>
		</div>
	</div>
</div>

@stop