@extends('layouts.app')
@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Category</h3>
		<br />
		<div class="row">
	  		<div class="large-4 columns">
	  			<a href="{{ route('add_category_page') }}" class="button small">
	  			<i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Add Category</a>
	  		</div>
		</div>
		<table align="center" width="100%">
			<thead>
			<tr>
				<th width="10%"></th>
				<th width="90%">Category Name</th>
			</tr>
			</thead>
			<tbody>
			@forelse ($categories as $category)
				<tr id="category-row-{{ $category->id }}">
					<td align="center">
						<a onclick="DeleteCategory({{ $category->id }})" id="delete_item{{ $category->id }}" title="Delete Category">
							<i class="fa fa-trash-o" aria-hidden="true"></i>
						</a> &nbsp;	
						<a href="{{ route('edit_category', $category->id ) }}" id="edit_category{{ $category->id }}" title="Edit Category">
							<i class="fa fa-pencil" aria-hidden="true"></i>
						</a>	
					</td>
					<td>{{ $category->name }}</td>
				</tr> 
				@empty
					<tr id="no-cat"><td colspan="2" style="text-align:center;">No Category Yet</td></tr>
				@endforelse
			</tbody>
		</table>
		<div align="center">
			@if(!$categories->isEmpty())
			{{ $categories->render() }}
			@endif
		</div>
	</div>
</div>
<script type="text/javascript">
	function DeleteCategory(categoryid, selector)
	{
		if(confirm("Are you sure to delete this?") ==  true){
			$.ajax({
				type : 'GET',
				url : '{{ route("delete_category") }}',
				data : 'category_id='+categoryid,
				cache : false,
				dataType : "script"
			}).done(function(response) {
				$(selector).closest("tr").remove();
				$('#category-row-'+categoryid).remove();
			}).fail(function(jqXHR, textStatus, y) {	
				//alert(textStatus);
			});
		}
	}
</script>
@stop