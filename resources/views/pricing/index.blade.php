@extends('layouts.app')
@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Pricing</h3>
		<br />
		@if (count($errors) > 0)
		<div class="row">
			<div class="medium-12 columns warning callout" data-closable>
				<ul>
			        @foreach ($errors->all() as $error)
			            <li>{{ $error }}</li>
			        @endforeach
			    </ul>
				<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</div>
		@endif
		@if(Session::has('message'))
		<div class="row">
			<div class="medium-12 columns success callout" data-closable>
				Price has been successfully added!
				<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		
		</div>
		 @endif
		 @if(Session::has('message2'))
		<div class="row">
			<div class="medium-12 columns success callout" data-closable>
				Price has been removed!
				<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		
		</div>
		 @endif
		<form action="{{ route('add_price') }}" method="POST">
			<div class="row">
		  		<div class="medium-5 columns">
					<label>Size
						<select id="size" name="size">
						@forelse ($sizes as $size)
							<option value="{{ $size->id }}"> {{ $size->size }} </option>
						@empty
							<option> </option>
						@endforelse
					</select>
					</label>
				</div>
				<div class="medium-5 columns">
					<label>Price
						<input name="price" type="text" id="price" value="{{ old('price') }}" autocomplete="off" />
						<input name="menu_id" type="hidden" id="price" value="{{ $menuid }}" autocomplete="off" />
					</label>
				</div>
				<div class="medium-2 columns">
					<label>&nbsp; <br>
						<input type="submit" class="button" value="Add">
					</label>
				</div>
			</div>
			{!! csrf_field() !!}
		</form>
		<table align="center" width="100%">
			<thead>
			<tr>
				<th width="10%"></th>
				<th width="50%">Size</th>
				<th width="40%">Price</th>
			</tr>
			</thead>
			<tbody>
				@forelse($prices as $price)
				<tr>
					<td align="center">
						<a onclick="return confirm('Are you sure you want to remove price?')" title="Remove Price" href="{{ route('remove_price', $price->id) }}">
							<span class="icon has-text-info"><i class="fa fa-minus-square"></i></span>
						</a>&nbsp;
						<a title="Manage Recipes" id="manage_recipes{{ $price->id }}" href="{{ route('manage_recipes', [$price->id]) }}">
							<i class="fa fa-bars" aria-hidden="true"></i>
						</a>
					</td>
					<td>{{ $price->sizes->size }}</td>
					<td>{{ $price->price }}</td>
				</tr>
				@empty
				<tr>
					<td align="center" colspan="3"> No prices.</td>
				</tr>
				@endforelse
			</tbody>
		</table>
		<div align="center">
		</div>
	</div>
</div>
<script type="text/javascript">
	function DeleteCategory(categoryid, selector)
	{
		if(confirm("Are you sure to delete this?") ==  true){
			$.ajax({
				type : 'GET',
				url : '{{ route("delete_category") }}',
				data : 'category_id='+categoryid,
				cache : false,
				dataType : "script"
			}).done(function(response) {
				$(selector).closest("tr").remove();
				$('#category-row-'+categoryid).remove();
			}).fail(function(jqXHR, textStatus, y) {	
				//alert(textStatus);
			});
		}
	}
</script>
@stop