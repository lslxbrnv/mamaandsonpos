@extends('layouts.app')

@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>
			<i class="fa fa-cube" aria-hidden="true"></i>&nbsp;
			Inventory
			</h3>
		<br />
		<div class="row">
	  		<div class="large-4 columns">
	  			&nbsp;
	  		</div>
			<div class="large-4 large-offset-4 columns">
				<form action="" method="get">
					<input type="text" id="search" name="search" placeholder="Search Ingredient" value="{{ $search }}" />
				</form>
			</div>
		</div>
		<table align="center" id="item_table" width="100%">
			<thead>
			<tr>
				<th width="25%">Item</th>
				<th width="10%" class="text-right">Quantity</th>
				<th width="10%">Unit of Measure</th>
				<th width="15%" class="text-center">Sold</th>
				<th width="15%" class="text-center">Perished Items</th>
			</tr>
			</thead>
			<tbody>
				@forelse ($items as $item)
				<tr id="item-row-{{ $item->id }}">
					<td>{{ $item->name }}</td>
					<td class="text-right">{{ $item->current_quantity }}</td>
					<td>{{ $item->unit }}</td>
					<td class="text-center">{{ $item->total_transaction_items }} {{  $item->unit }}</td>
					<td class="text-center">{{ $item->total_spoilages }} {{  $item->unit }}</td>
				</tr> 
			@empty
				<tr id="no-ingredient"><td colspan="6" style="text-align:center;">No Items Added.</td></tr>

			@endforelse
			</tbody>
		</table>
		<div align="center">
			@if(!$items->isEmpty())
			{{ $items->render() }}
			@endif
		</div>
	</div>
</div>
<script type="text/javascript">
	function DeleteItem(itemid, selector)
	{
		if(confirm("Are you sure to delete this?") ==  true){
			$.ajax({
				type : 'GET',
				url : '{{ route("delete_item") }}',
				data : 'item_id='+itemid,
				cache : false,
				dataType : "script"
			}).done(function(response) {
				$(selector).closest("tr").remove();
				$('#item-row-'+itemid).remove();
			}).fail(function(jqXHR, textStatus, y) {	
				//alert(textStatus);
			});
		}
	}
</script>

@stop