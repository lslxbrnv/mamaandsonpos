@extends('layouts.app')
@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Sizes</h3>
		<br />
		@if (count($errors) > 0)
		<div class="row">
			<div class="medium-12 columns warning callout" data-closable>
				<ul>
			        @foreach ($errors->all() as $error)
			            <li>{{ $error }}</li>
			        @endforeach
			    </ul>
				<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		</div>
		@endif
		@if(Session::has('message'))
		<div class="row">
			<div class="medium-12 columns success callout" data-closable>
				Size has been successfully added!
				<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		
		</div>
		 @endif
		 @if(Session::has('message2'))
		<div class="row">
			<div class="medium-12 columns success callout" data-closable>
				Size has been removed!
				<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		
		</div>
		 @endif
		<form action="{{ route('add_size') }}" method="POST">
			<div class="row">
		  		<div class="medium-10 columns">
					<label>Size
						<input name="size" type="text" id="size" value="{{ old('size') }}" autocomplete="off" />
					</label>
				</div>
				<div class="medium-2 columns">
					<label>&nbsp; <br>
						<input type="submit" class="button" value="Add">
					</label>
				</div>
			</div>
			{!! csrf_field() !!}
		</form>
		<table align="center" width="100%">
			<thead>
			<tr>
				<th width="10%"></th>
				<th width="90%">Size</th>
			</tr>
			</thead>
			<tbody>
				@forelse($sizes as $size)
				<tr>
					<td align="center">
						<a onclick="return confirm('Are you sure you want to remove size?')" title="Remove Size" href="{{ route('remove_size', $size->id) }}">
							<span class="icon has-text-info"><i class="fa fa-minus-square"></i></span>
						</a>
					</td>
					<td>{{ $size->size }}</td>
				</tr>
				@empty
				<tr>
					<td align="center" colspan="2"> No Sizes.</td>
				</tr>
				@endforelse
			</tbody>
		</table>
		<div align="center">
		</div>
	</div>
</div>
<script type="text/javascript">
	function DeleteCategory(categoryid, selector)
	{
		if(confirm("Are you sure to delete this?") ==  true){
			$.ajax({
				type : 'GET',
				url : '{{ route("delete_category") }}',
				data : 'category_id='+categoryid,
				cache : false,
				dataType : "script"
			}).done(function(response) {
				$(selector).closest("tr").remove();
				$('#category-row-'+categoryid).remove();
			}).fail(function(jqXHR, textStatus, y) {	
				//alert(textStatus);
			});
		}
	}
</script>
@stop