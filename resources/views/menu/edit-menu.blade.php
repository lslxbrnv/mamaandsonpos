@extends('layouts.app')
@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Edit Menu</h3>
		<br />
		<div class="row">
			<div class="medium-3 columns"></div>
			<div class="medium-9 columns">
				@if (count($errors) > 0)
				<div class="row">
					<div class="medium-8 columns warning callout" data-closable>
						<ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				@endif
				@if(Session::has('message'))
				<div class="row">
					<div class="medium-8 columns success callout" data-closable>
						Menu has been successfully updated!
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				
				</div>
				 @endif
				{!! Form::open(array('route' => 'update_menu','files'=>true)) !!}
					<div class="row">
						<div class="medium-8 columns">
							<label>Category Name
								<select class="mdl-select__input" id="category_id" name="category_id">
								<option value="0"></option>
								@forelse ($categories as $category)
									@if ($category->id == $menus->category_id)
										<option value="{{ $category->id }}" selected="selected"> {{ $category->name }} </option>
									@else
										<option value="{{ $category->id }}"> {{ $category->name }} </option>
									@endif
								@empty
								@endforelse
							</select>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>Menu
								<input name="menu" type="text" id="menu" value="{{ $menus->menu }}" autocomplete="off" />
							</label>
						</div>
					</div>
				<!-- 	<div class="row">
						<div class="medium-8 columns">
							<label>Selling Price
								<input name="selling_price" type="text" id="selling_price" pattern="-?[0-9]*(\.[0-9]+)?" value="{{ $menus->selling_price }}" autocomplete="off" />
							</label>
						</div>
					</div> -->
					<div class="row">
						<div class="medium-8 columns">
							<label>Details
								<textarea name="details" class="textarea" type="text" placeholder="Details">{{ $menus->details }}</textarea>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<div class="file">
								<label class="file-label">
									 {!! Form::file('image', array('class' => 'file-input')) !!}
								</label>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<input name="id" type="hidden" id="id" value="{{ $menus->id }}" />
							<input type="submit" class="button" value="Update">
							<a class="button secondary" href="{{ route('menu') }}">Cancel</a>
						</div>
					</div>
					{!! csrf_field() !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@stop