@extends('layouts.app')
@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Menu</h3>
		<br />
		<div class="row">
	  		<div class="large-4 columns">
	  			<a href="{{ route('add_menu_page') }}" class="button small">
	  			<i class="fa fa-plus" aria-hidden="true"></i> &nbsp;Add Menu</a>
	  			
	  		</div>
	  		<div class="large-4 large-offset-4 columns">
				<form action="" method="get">
					<input type="text" id="search" name="search" placeholder="Search Menu" value="{{ $search }}" />
				</form>
			</div>
		</div>
		<table align="center" id="menu_table" width="100%">
			<thead>
			<tr>
				<th width="15%"></th>
				<th width="25%">Category</th>
				<th width="50%">Menu</th>
			</tr>
			</thead>
			<tbody>
			@forelse ($menus as $menu)
				<tr id="menurow-{{ $menu->id }}">
					<td align="center">
						<a  title="Delete Menu" id="delete_menu{{ $menu->id }}" onclick="DeleteMenu({{ $menu->id }})">
							<i class="fa fa-trash-o" aria-hidden="true"></i>
						</a>&nbsp;
						<a  title="Edit Menu" id="edit_menu{{ $menu->id }}" href="{{ route('edit_menu', $menu->id ) }}">
							<i class="fa fa-pencil" aria-hidden="true"></i>
						</a>&nbsp;<!-- 
						<a title="Manage Recipes" id="manage_recipes{{ $menu->id }}" href="{{ route('manage_recipes', [$menu->id]) }}">
							<i class="fa fa-bars" aria-hidden="true"></i>
						</a>&nbsp;

 -->						<a title="Add Prices" id="pricing{{ $menu->id }}" href="{{ route('pricing', [$menu->id]) }}">
							<i class="fa fa-dollar"></i>
						</a>		
					<!-- 	<a title="Manage Prices" href="{{ route('manage_prices', [$menu->id]) }}">
							<i class="fa fa-dollar" aria-hidden="true"></i>
						</a>	 -->			
					</td>
					<td>{{ $menu->Category['name'] }}</td>
					<td>{{ $menu->menu }}</td>
				</tr> 
				@empty
					<tr><td colspan="4" style="text-align:center;">No Menu Yet</td></tr>
			@endforelse
			</tbody>
		</table>
		<div align="center">
			@if(!$menus->isEmpty())
				{{ $menus->render() }}
			@endif
		</div>
	</div>
</div>
<script type="text/javascript">
	function DeleteMenu(menuid, selector)
	{
		if(confirm("Are you sure to delete this?") ==  true){
			$('#menurow-'+menuid).remove();
			$.ajax({
				type : 'GET',
				url : '{{ route("delete_menu") }}',
				data : 'menuid='+menuid,
				cache : false,
				dataType : "script"
			}).done(function(response) {
				
			}).fail(function(jqXHR, textStatus, y) {	
				//alert(textStatus);
			});
		}
	}
</script>

@stop