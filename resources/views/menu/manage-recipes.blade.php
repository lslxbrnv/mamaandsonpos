@extends('layouts.app')

@section('content')
@parent
<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<a href="{{ route('menu') }}" class="button small">
	  			<i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;Back</a>
		<h4> Recipe for {{ $price->menu->menu }} - {{ $price->sizes->size }}</h4>
		<br />
		<div class="row">
			<div class="medium-3 columns"></div>
			<div class="medium-9 columns">

				@if (count($errors) > 0)
				<div class="row">
					<div class="medium-8 columns warning callout" data-closable>
						<ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				@endif
				@if(Session::has('message'))
				<div class="row">
					<div class="medium-8 columns success callout" data-closable>
						Recipe has been successfully updated!
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				
				</div>
				 @endif
				<form id="add_recipe" action="{{ route('add_recipe') }}" method="POST">	
				 	<div class="row">
						<div class="medium-5 columns">
							<label>Ingredient
								<input name="search_item" type="text" id="search_item" value="{{ old('search_item') }}" autocomplete="off" />
								<input type="hidden" name="item" id="item"/>
							</label>
						</div>
						<div class="medium-4 columns">
							<label>Servings
								<input style="width:150px;" name="serving" type="text" id="serving" value="{{ old('serving') }}" />
								<input name="pricing_id" type="hidden" id="pricing_id" value="{{ $priceid }}" />
							</label>
						</div>
						<div class="medium-3 columns">
							<button style="margin-top: 27px; margin-left:-60px;" class="button small" type="submit" >Add Recipe </button>	
						</div>
					</div>			
					<div class="row">
						<div class="medium-10 columns">
							<table id="item_list" width="100%">
								<thead>
									<tr>
										<th width="15%"></th>
										<th width="65%">Item Name</th>
										<th width="30%">Serving</th>
									</tr>
								</thead>
								<tbody>
									@forelse($recipes as $recipe)
									<tr>
										<td class="text-center">
											<a title="Delete Recipe" id="delete_recipe{{ $recipe->id }}" href="{{ route('delete_recipe', ['recipeid'=>$recipe->id, 'pricing_id'=>$priceid]) }}">
												<i class="fa fa-trash-o" aria-hidden="true"></i>
											</a>	
										</td>
										<td>{{ $recipe->Item->name }} </td>
										<td>{{ $recipe->serving }} {{ $recipe->Item->unit }}</td>
									</tr>
									@empty
										<tr><td colspan="3" style="text-align:center;">No Recipes Yet</td></tr>
									@endforelse
								</tbody>
							</table>
						</div>
					</div>
					{!! csrf_field() !!}
				</form>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var xhr_item_search = null;
	$("#search_item").autocomplete({
        source: function (request, response) {
        	if(xhr_item_search != null)
        	{
        		return false;
        	}
            xhr_item_search = $.ajax({
                type : 'get',
                url : "{{ route('items_json') }}",
                data : 'search=' + request.term,
                cache : true,
                dataType : "json",
                beforeSend: function(xhr){
                    if (xhr_item_search != null)
                    {
                        xhr_item_search.abort();
                    }
                }
            }).done(function(data){
            			//console.log(data);
                response($.map( data, function(value, key){
                    return { label: value, value: key }
                }));
                xhr_item_search = null;
            }).fail(function(jqXHR, textStatus){
                //console.log('Request failed: ' + textStatus);
            });
        }, 
        minLength: 3,
        autoFocus: true,
        select: function(event, ui){
            var id = ui.item.value;
            var name = ui.item.label;
            $('#search_item').val(name);
            $('#item').val(id);
            return false;
        }
    });	

</script>
@stop