@extends('layouts.app')
@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Manage Sizes</h3>
		<br />
		<div class="row">
			<div class="medium-12 columns">
				@if (count($errors) > 0)
				<div class="row">
					<div class="medium-12 columns warning callout" data-closable>
						<ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				@endif
				@if(Session::has('message'))
				<div class="row">
					<div class="medium-12 columns success callout" data-closable>
						Size has been successfully added!
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				 @endif
				 @if(Session::has('message2'))
				<div class="row">
					<div class="medium-12 columns warning callout" data-closable>
						Size has been removed!
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				 @endif
				{!! Form::open(array('route' => 'add_size')) !!}
				<div class="row">
					<div class="medium-11 columns">
						<label>Size
							<input name="size" type="text" value="{{ old('size') }}" autocomplete="off" />
						</label>
					</div>
					<div class="medium-1 columns">
						<label>
							<input type="submit" class="button" value="Add" style="margin-top:24px;" />
						</label>
					</div>
				</div>
				{!! csrf_field() !!}
				{!! Form::close() !!}
			</div>
			<div class="medium-12 columns">
				<table>
					<thead>
						<tr>
							<th width="10%"></th>
							<th>Size</th>
					</thead>
					<tbody>
						@forelse($sizes as $size)
						<tr>
							<td align="center">
								<a onclick="return confirm('Are you sure you want to remove size price?')" href="{{ route('remove_size',  $size->id) }}" title="Remove Size">
									<span class="icon has-text-info"><i class="fa fa-minus-square"></i></span>
								</a>
							</td>
							<td>{{ $size->size }}</td>
						</tr>
						@empty
						<tr>
							<td rowspan="2">No Data.</td>
						</tr>
						@endforelse
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop