@extends('layouts.app')
@section('content')
@parent

<div class="row">
	<div class="medium-2 columns"></div>
	<div class="medium-10 columns">
		<h3>Add Menu</h3>
		<br />
		<div class="row">
			<div class="medium-3 columns"></div>
			<div class="medium-9 columns">
				@if (count($errors) > 0)
				<div class="row">
					<div class="medium-8 columns warning callout" data-closable>
						<ul>
					        @foreach ($errors->all() as $error)
					            <li>{{ $error }}</li>
					        @endforeach
					    </ul>
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				</div>
				@endif
				@if(Session::has('message'))
				<div class="row">
					<div class="medium-8 columns success callout" data-closable>
						Menu has been successfully added!
						<button class="close-button" aria-label="Dismiss alert" type="button" data-close>
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				
				</div>
				 @endif
				{!! Form::open(array('route' => 'add_menu','files'=>true)) !!}
					<div class="row">
						<div class="medium-8 columns">
							<label>Category Name
								<select id="category_id" name="category_id">
								@forelse ($categories as $category)
									<option value="{{ $category->id }}"> {{ $category->name }} </option>
								@empty
								@endforelse

							</select>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>Menu
								<input name="menu" type="text" id="menu" value="{{ old('menu') }}" autocomplete="off" />
							</label>
						</div>
					</div>
					<div class="row">
						<div class="medium-8 columns">
							<label>Details
								<textarea name="details" class="textarea" type="text" placeholder="Details">{{ old('details') }}</textarea>
							</label>
						</div>
					</div>
					<div class="row">
						<div class="file">
							<label class="file-label">
								 {!! Form::file('image', array('class' => 'file-input')) !!}
							</label>
						</div>
					</div>

					<div class="row">
						<div class="medium-8 columns">
							<input type="submit" class="button" value="Add">
							<a class="button secondary" href="{{ route('menu') }}">Cancel</a>
						</div>
					</div>
					{!! csrf_field() !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>

@stop