


		var authAdminDialog = document.querySelector('#auth_admin_dialog');
	    //var showAuthAdminDialog = document.querySelector('#show_auth_admin_dialog');
		var customerTransactionDialog = document.querySelector('#customer_transaction_dialog');
		var discountDialog = document.querySelector('#discount_dialog');
		
	    if (!authAdminDialog.showModal)
	    {
	      dialogPolyfill.registerDialog(authAdminDialog);
	      dialogPolyfill.registerDialog(customerTransactionDialog);
	      dialogPolyfill.registerDialog(discountDialog);
	    }

		$(function(){
			/*setInterval(function(){
				datetime();
			}, 60000);*/
			/*
				Hot Keys
			*/
			$(document).on('keydown', function(e){

				var ctrl = e.ctrlKey;
				var key = e.keyCode;
				var key_array = [112, 113, 114, 115, 117, 118, 119, 120, 121, 122, 123];

				//console.log(key + " + " + e.ctrlKey);
				if ($.inArray(key, key_array) > -1)
				{
					if (key == 112 && ctrl == false)//	F1 = Focus Search Menu
					{
						$("#search_menu").focus();
					}
					else if (key == 113 && ctrl == false)//	F2 = Focus Customer
					{
						$("#customer_name").focus();
					}
					else if (key == 114 && ctrl == false)//	F3 = Focus Amount Tendered
					{
						$("#amount_tendered").focus();
					}
					else if (key == 115 && ctrl == false)//	F4 = Checkout/Complete Sale
					{
						$("#billout").submit();
					}
					else if (key == 117 && ctrl == false)//	F6 = Cancel Transaction
					{
						$("#billout button[type='button']").click();
					}
					else if (key == 118 && ctrl == false)//	F7 = Discount
					{
						discountDialog.showModal();
					}
					else if (key == 123 && ctrl == false)//	F12 = Logout
					{
						window.location.replace( logout_url );
					}

					e.preventDefault();
					return false;
				}
			});
			/*
				Close MOdal
			*/
			$('#auth_admin_dialog .close').click(function(){
				$('#auth_admin_form input').val('');
				authAdminDialog.close();
			});
			$('#customer_transaction_dialog .close').click(function(){
				customerTransactionDialog.close();
			});
			$('#discount_dialog .close').click(function(){
				discountDialog.close();
			});
			/*
				Search
			*/
		    $("#search_menu").autocomplete({
		        source: function (request, response) {
		        	if(xhr_menu_search != null)
		        	{
		        		return false;
		        	}

		            xhr_menu_search = $.ajax({
		                type : 'get',
		                url : menu_json_url,
		                data : 'search=' + request.term,
		                cache : true,
		                dataType : "json",
		                beforeSend: function(xhr){
		                    if (xhr_menu_search != null)
		                    {
		                        xhr_menu_search.abort();
		                    }
		                }
		            }).done(function(data){

		                response($.map( data, function(value, key){
		                    return { label: value, value: key }
		                }));

		                xhr_menu_search = null;

		            }).fail(function(jqXHR, textStatus){
		                //console.log('Request failed: ' + textStatus);
		            });
		        }, 
		        minLength: 3,
		        autoFocus: true,
		        select: function(a, b){
		            var id = b.item.value;
		            var name = b.item.label;

		            $("input[name='menu_id']").val(id);
		            $("#search_menu").val(id).focus();
		            //$("#search_menu").val('').focus();
		            //$("#orderedMenu").trigger( "submit" );

            		return false;
		        }
		    });
			/*
				Selected Menu
			*/
			$('#orderedMenu').submit(function(e){

				if(xhr_ordered_menu != null)
				{
					return false;
				}

			    xhr_ordered_menu = $.ajax({
			        type : $(this).attr('method'),
			        url : $(this).attr('action'),
			        data : $(this).serialize(),
			        cache : false,
			        dataType : "json",
			        beforeSend: function(xhr) {
			            if (xhr_ordered_menu != null){
			                xhr_ordered_menu.abort();
			            }
			        }
			    }).done(function(data){

			    	if( data['status'] == 'error' )
			    	{
			    		alert(data['message']);
			    	}
			    	else
			    	{
			    		$('#orderedMenu input').val('');
				    	$('#menuListTable tbody').html('');
		            	$("input[name='search_quantity']").val(1);
				    	menu_list(data['cash_register_id']);
			    	}

			    	xhr_ordered_menu = null;
			    
			    }).fail(function(jqXHR, textStatus){

			    });

				e.preventDefault();
				return false;
			});
			/*
				Add Customer
			*/
			$('#customer_transaction_form').submit(function(e){

				if(xhr_add_customer != null)
				{
					return false;
				}

			    xhr_add_customer = $.ajax({
			        type : 'get',
			        url : add_customer_url,
			        data : $(this).serialize(),
			        cache : false,
			        dataType : "json",
			        beforeSend: function(xhr) {
			            if (xhr_add_customer != null){
			                xhr_add_customer.abort();
			            }
			        }
			    }).done(function(data){

			    	if( data['status'] == 'error' )
			    	{
			    		alert(data['message']);
			    	}
			    	else
			    	{
			    		setTimeout(function(){
							customerTransactionDialog.close();
							tables(data['transaction_type']);
			    			show_customer_transaction(data['table_id'], data['table_number'], data['transaction_type']);
			    		}, 1000);
			    	}

			    	xhr_add_customer = null;
			    
			    }).fail(function(jqXHR, textStatus){

			    });

				e.preventDefault();
				return false;
			});
			/*
				Billout
			*/
			$('#discount_form').submit(function(e){

				if(xhr_discount_form != null)
				{
					return false;
				}

			    xhr_discount_form = $.ajax({
			        type : 'get',
			        url : discount_url,
			        data : $(this).serialize(),
			        cache : false,
			        dataType : "json",
			        beforeSend: function(xhr) {
			            if (xhr_discount_form != null){
			                xhr_discount_form.abort();
			            }
			        }
			    }).done(function(data){

			    	if( data['status'] == 'error' )
			    	{
			    		alert(data['message']);
			    	}
			    	else
			    	{

						discountDialog.close();
						$('#discount_form input').val('');
			    		show_customer_transaction(data['table_id'], data['table_number'], data['transaction_type']);
			    	}

			    	xhr_discount_form = null;
			    
			    }).fail(function(jqXHR, textStatus){

			    });

				e.preventDefault();
				return false;
			});
			/*
				Billout
			*/
			$('#billout').submit(function(e){

				if(xhr_billout != null)
				{
					return false;
				}

			    xhr_billout = $.ajax({
			        type : $(this).attr('method'),
			        url : $(this).attr('action'),
			        data : $(this).serialize(),
			        cache : false,
			        dataType : "json",
			        beforeSend: function(xhr) {
			            if (xhr_billout != null){
			                xhr_billout.abort();
			            }
			        }
			    }).done(function(data){

			    	if( data['status'] == 'error' )
			    	{
			    		alert(data['message']);
			    	}
			    	else
			    	{
			    		$('#cash_register_id').val(data['cash_register_id']);
			    		menu_cancel();
						window.open( or_url + "/" + data['id'], '_blank' );
			    	}

			    	xhr_billout = null;
			    
			    }).fail(function(jqXHR, textStatus){

			    });

				e.preventDefault();
				return false;
			});

			/*
				Authenticate
			*/
			$('#auth_admin_form').submit(function(e){

				var status = '';
				var type = $("input[name='type']").val();

	            if (xhr_auth_admin != null)
	            {
	                return false;
	            }

			    xhr_auth_admin = $.ajax({
			        type : 'get',
			        url : auth_admin_url,
			        data: $(this).serialize(),
			        cache : false,
			        dataType : "json",
			        beforeSend: function(xhr){
			            if (xhr_auth_admin != null)
			            {
			                xhr_auth_admin.abort();
			            }
			        }
			    }).done(function(data){

			    	status = data['status'];

			    	if( status == 'ok' )
			    	{
			    		if(type == 'cancel')
			    		{
							menu_cancel();
			    		}
			    		else if(type == 'delete')
			    		{
							var menu_id = $("#auth_admin_form input[name='menu_id'").val();
				    		menu_delete(menu_id);
			    		}
			    		authAdminDialog.close();
			    		$("#auth_admin_form input").val('');
			    	}
			    	else
			    	{
			    		alert('Invalid username and password!');
			    	}

			    	xhr_auth_admin = null;
			    
			    }).fail(function(jqXHR, textStatus){
			        //console.log('Request failed: ' + textStatus);
			    });

				e.preventDefault();
				return false;
			});
		});
		/*
			Ordered List
		*/
		function tables(takeout)
		{
            if (xhr_tables != null)
            {
                return false;
            }

		    xhr_tables = $.ajax({
		        type : 'get',
		        url : tables_url,
		        cache : false,
		        data  : { takeout : takeout },
		        dataType : "json",
		        beforeSend: function(xhr){
		        	//
		        }
		    }).done(function(data){

		    	var rows = '';
		    	var type = 'TAKE OUT';

		    	$.each(data['tables'], function(key, value){

		    		var status = 'mdl-success';

		    		if(value['cash_register'] == true)
		    		{
		    			status = 'mdl-alert';
		    		}

		    		if(value['id'] != 1)
		    		{
		    			type = 'DINE IN';
		    		}

		    		rows += '<button onclick="show_customer_transaction(' + value['id'] + ', \'' + value['name'].replace('\'', '`') + '\', \'' + type + '\')" type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect ' + status + ' mdl-expand" ' + onclick + ' ><strong>' + value['name'] + '</strong></button><br /><br />';
		    	});

		    	$('#tables').html(rows);

		    	xhr_tables = null;
		    
		    }).fail(function(jqXHR, textStatus){
		        //console.log('Request failed: ' + textStatus);
		    });
		}
		/*
			Open customer transsaction form
		*/
		function show_customer_transaction(id, name, type)
		{
            if (xhr_show_customer_transaction != null)
            {
                return false;
            }

		    xhr_show_customer_transaction = $.ajax({
		        type : 'get',
		        url : customer_transaction_url,
		        data : { table_id : id },
		        cache : false,
		        dataType : "json",
		        beforeSend: function(xhr){
		        	//
		        }
		    }).done(function(data){

		    	if(data['status'] == 'ok')
		    	{
		    		menu_list( data['id'] );
		    		$('#search_menu').focus();
					$('#show_table_number').val(name);
					$('#discount_amount').val(data['discount']);
					$('#show_transaction_type').val(type);
					$('#show_customer_name').val(data['customer_name']);
					$('#cash_register_id, input[name=\'cash_register_id\']').val(data['id']);

					$('#table_id, #table_number, #transaction_type, #customer_name').val('');

					$('#cash_register_discount_id').val(data['id']);
		    	}
		    	else
		    	{
					customerTransactionDialog.showModal();
					$('#table_id').val(id);
					$('#table_number').val(name);
					$('#transaction_type').val(type);
					$('#customer_name').focus();

					$('#cash_register_discount_id').val(id);

					$('#billout input[type=\'text\'], #cash_register_id, input[name=\'cash_register_id\']').val('');
		    		$('#menuListTable tbody').html('');
		    	}

		    	xhr_show_customer_transaction = null;
		    
		    }).fail(function(jqXHR, textStatus){
		        //console.log('Request failed: ' + textStatus);
		    });
		}
		/*
			Ordered List
		*/
		function menu_list(id)
		{
            if (xhr_menu_list != null)
            {
                return false;
            }

		    xhr_menu_list = $.ajax({
		        type : 'get',
		        url : menu_order_url,
		        data : { id : id },
		        cache : false,
		        dataType : "json",
		        beforeSend: function(xhr) 
		        {
		            if (xhr_menu_list != null)
		            {
		                xhr_menu_list.abort();
		            }
		        }
		    }).done(function(data){

		    	var row = '';

		    	if(data['cart'].length >= 1)
		    	{
			    	$.each(data['cart'], function(key, value){

			    		var qty = value['qty'];
			    		var price = value['price'];
			    		var total = price * qty;

			    		row += 	'<tr id="menu-' + value['id'] + '"><td class="text-center">' + 
			    						'<i class="material-icons mdl-list__item-icon pointer" onclick="menu_auth_delete(' + value['id'] + ')">close</i>' + 
			    						'<i class="material-icons mdl-list__item-icon pointer showMenuItems showMenuItems-' + value['id'] + '" onclick="showMenuItems(' + value['id'] + ')">keyboard_arrow_up</i>' + 
			    						'<i class="material-icons mdl-list__item-icon pointer hideMenuItems hideMenuItems-' + value['id'] + ' hide" onclick="hideMenuItems(' + value['id'] + ')">keyboard_arrow_down</i>' + 
			    					'</td><td class="text-left" >' + value['name'] + 
			    					'</td><td>' + value['price_formatted'] + 
			    					'</td><td>' + value['total_formatted'] + 
			    					'</td></tr>' + 
			    					'<tr id="item-' + value['id'] + '" class="menuItems"><td colspan="4" class="borderNone">' + 
				    					'<ul class="demo-list-item mdl-list">';

				    	$.each(value['items'], function(k, v){
				    		row += 			'<li class="mdl-list__item" id="menu-item-' + v['id'] + '">' + 
				    							'<span class="mdl-list__item-primary-content">' + 
				    								'<button onclick="remove_item(' + v['id'] + ')" class="mdl-button mdl-js-button mdl-button--icon mdl-button--colored"><i class="material-icons mdl-list__item-icon">close</i></button>' + v['name'] +
				    							'</span>' + 
				    						'</li>';
			    		});

				    	row += 			'</ul>' + 
			    					'</td></tr>';
			    	});

		    		$('#show_amount_due').val(data['amount_due_formatted']);
		    		$('#show_total_amount_due').val(data['total_amount_due_formatted']);
		    		$('input[name=\'total_amount_due\']').val(data['total_amount_due']);
		    		//$('#amount_due').val(data['amount_due']);
		    	}
		    	else
		    	{
		    		row = '<tr><td colspan="4" class="text-center"><strong>No Item/s</strong></td></tr>';
		    		$('#amountDue').html('');
		    		$('#customer_name, #amount_tendered, #amount_due, #amount_due_formatted').val('');
		    	}

			    $('#menuListTable tbody').html(row);
		    	$('.menuItems').hide();

		    	xhr_menu_list = null;
		    
		    }).fail(function(jqXHR, textStatus){
		        //console.log('Request failed: ' + textStatus);
		    });
		}
		/*
			show Menu Items
		*/
		function showMenuItems(id)
		{
			$('.showMenuItems-' + id).hide();
			$('.hideMenuItems-' + id).show();
			$('#item-' + id).toggle();
		}
		/*
			hide Menu Items
		*/
		function hideMenuItems(id)
		{
			$('.hideMenuItems-' + id).hide();
			$('.showMenuItems-' + id).show();
			$('#item-' + id).toggle();
		}
		/*
			Order Delete
		*/
		function menu_auth_delete(menu_id)
		{
			$("#auth_admin_form input[name='type'").val('delete');
			$("#auth_admin_form input[name='menu_id'").val(menu_id);
			authAdminDialog.showModal();
		}

		function menu_delete(menu_id)
		{
		    $('#menu-' + menu_id + ', #item-' + menu_id).remove();

		    xhr_menu_delete = $.ajax({
		        type : 'get',
		        url : menu_delete_url,
		        data: { menu_id : menu_id },
		        cache : false,
		        dataType : "json",
		        beforeSend: function(xhr){
		        	//
		        }
		    }).done(function(data){

		    	xhr_menu_delete = null;
		    
		    }).fail(function(jqXHR, textStatus){
		        //console.log('Request failed: ' + textStatus);
		    });

		}
		/*
			Order Cancel
		*/
		function menu_auth_cancel()
		{
			$("#auth_admin_form input[name='type'").val('cancel');
			authAdminDialog.showModal();
		}

		function menu_cancel()
		{
		    //$('#show_table_number, #amount_tendered, #show_transaction_type, #show_customer_name, #show_amount_due, #discount_amount, #show_total_amount_due').val('');
		    $('#billout input').val('');

		    var mc_cash_register_id = $('#cash_register_id').val();

		    xhr_menu_cancel = $.ajax({
		        type : 'get',
		        url : menu_cancel_url,
		        data : { cash_register_id : mc_cash_register_id },
		        cache : false,
		        dataType : "json",
		        beforeSend: function(xhr){
		        	//
		        }
		    }).done(function(data){

		    	tables(0);

				menu_list(mc_cash_register_id);

		    	xhr_menu_cancel = null;
		    
		    }).fail(function(jqXHR, textStatus){
		        //console.log('Request failed: ' + textStatus);
		    });
		}
		/*
			Order Cancel
		*/
		function datetime()
		{
		    xhr_datetime = $.ajax({
		        type : 'get',
		        url : datetime_url,
		        data : {},
		        cache : false,
		        dataType : "json",
		        beforeSend: function(xhr){
		        	//
		        }
		    }).done(function(data){

		    	$('#dateTime').text(data['datetime']);
		    	xhr_datetime = null;
		    
		    }).fail(function(jqXHR, textStatus){
		        //console.log('Request failed: ' + textStatus);
		    });
		}
		/*
			Remove Items
		*/
		function remove_item(id)
		{
		    $('#menu-item-' + id).remove();
		    xhr_remove_item = $.ajax({
		        type : 'get',
		        url : remove_item_url,
		        data : { id : id },
		        cache : false,
		        dataType : "json",
		        beforeSend: function(xhr){
		        	//
		        }
		    }).done(function(data){

		    	xhr_remove_item = null;
		    
		    }).fail(function(jqXHR, textStatus){
		        //console.log('Request failed: ' + textStatus);
		    });
		}
