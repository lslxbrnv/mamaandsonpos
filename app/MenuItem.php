<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    public $table = 'menu_item';
    public $fillable = ['menu_id', 'item_id'];
    public $timestamps = false;

    public function Menu()
    {
    	return $this->hasMany('App\Menu');
    }

    public function Item()
    {
    	return $this->belongsTo('App\Items');
    }
}
