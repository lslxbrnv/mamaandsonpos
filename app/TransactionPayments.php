<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionPayments extends Model
{
    public $table = 'transaction_payments';
    public $fillable = [ 'transaction_id', 'type', 'amount', 'deleted_at' ];
    public $timestamps = false;

    public function transaction()
    {
    	return $this->hasMany('App\Transaction', 'id');
    }
}
