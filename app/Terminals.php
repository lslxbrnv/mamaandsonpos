<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Terminals extends Model
{
    public $table = 'terminals';
    public $fillable = ['terminal_name', 'ip', 'hd_serial', 'machine_id', 'server', 'active', 'created_at', 'updated_at'];
    public $timestamps = true;

    public function transaction()
    {
    	return $this->hasMany('App\Transaction');
    }
}
