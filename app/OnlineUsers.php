<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlineUsers extends Model
{
    protected $connection = 'mysql2';

    public $table = 'users';
    public $timestamps = true;

    protected $fillable = [
        'email', 'name', 'contact_number', 'address', 'username', 'password', 'access', 'remember_token', 'created_at', 'updated_at', 'deleted_at', 'provider', 'provider_id'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    

}
