<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

# Login
Route::get('/', ['as' => 'login_page', 'uses' => 'HomeController@login_page']);
Route::post('/login',['as' => 'user_login', 'uses' => 'HomeController@user_login']);

Route::group(['prefix' => 'portal', 'middleware' => 'auth'], function(){
	# Logout
	Route::get('/logout', ['as' => 'logout', 'uses' => 'HomeController@logout']);
#dashboard_home
	# Dashboard
	Route::get('/', ['as' => 'dashboard', 'uses' => 'HomeController@dashboard_home']);
	Route::get('/annual-sales-json', ['as' => 'annual_sales_json', 'uses' => 'HomeController@annual_sales_json']);
	Route::get('/montly-sales-per-menu-json/{menuid}', ['as' => 'monthly_sales_per_menu_json', 'uses' => 'HomeController@monthly_sales_per_menu_json']);

	# Sales
	Route::group(['prefix' => 'sales'], function(){
		# Cash Register
		Route::get('/', ['as' => 'sales', 'uses' => 'CashRegisterController@sales']);
		# Official Receipt
		Route::get('/or/{id?}', ['as' => 'officialReceiptPrint', 'uses' => 'CashRegisterController@officialReceiptPrint']);
		# Session
		Route::group(['prefix' => 'cart'], function(){
			Route::get('/tables', ['as' => 'tables', 'uses' => 'CashRegisterController@tables']);
			Route::get('/add-customer', ['as' => 'addCustomer', 'uses' => 'CashRegisterController@addCustomer']);
			Route::get('/discount', ['as' => 'addDiscount', 'uses' => 'CashRegisterController@discount']);
			Route::get('/customer-transaction', ['as' => 'customerTransaction', 'uses' => 'CashRegisterController@customerTransaction']);

			Route::get('/datetime', ['as' => 'datetime', 'uses' => 'CashRegisterController@datetime']);

			Route::get('/billout', ['as' => 'billout', 'uses' => 'CashRegisterController@billout']);
			Route::get('/verify/{id?}', ['as' => 'verify', 'uses' => 'CashRegisterController@verify']);
			Route::get('/add', ['as' => 'menuSaleAdd', 'uses' => 'CashRegisterController@menuSaleAdd']);

			Route::get('/list', ['as' => 'menuSaleList', 'uses' => 'CashRegisterController@menuSaleList']);

			Route::get('/cancel', ['as' => 'menuSaleCancel', 'uses' => 'CashRegisterController@cancel']);
			Route::get('/remove-item', ['as' => 'removeItem', 'uses' => 'CashRegisterController@removeItem']);
			Route::get('/delete', ['as' => 'menuSaleDelete', 'uses' => 'CashRegisterController@menuSaleDelete']);

			Route::get('/auth-admin', ['as' => 'authAdmin', 'uses' => 'CashRegisterController@authenticateAdmin']);
		});
	});

	#inventory
	Route::group(['prefix' => 'inventorylist'], function(){
		Route::get('/', ['as' => 'inventorylist', 'uses' => 'InventoryController@index']);
	});

	#settings
	Route::group(['prefix' => 'settings'], function(){
		Route::get('/', ['as' => 'settings', 'uses' => 'SettingsController@index']);
		Route::post('/update', ['as' => 'update_company', 'uses' => 'SettingsController@update_company']);

		Route::get('/terminals', ['as' => 'terminals', 'uses' => 'SettingsController@terminals']);
		Route::get('/edit-terminal/{terminalid}', ['as' => 'edit_terminal', 'uses' => 'SettingsController@edit_terminal']);

		Route::get('/sizes/', ['as' => 'sizes', 'uses' => 'SettingsController@sizes']);
		Route::post('/add-size/', ['as' => 'add_size', 'uses' => 'SettingsController@add_size']);
		Route::get('/remove-size/{id}', ['as' => 'remove_size', 'uses' => 'SettingsController@remove_size']);

		Route::get('/pricing/{id}', ['as' => 'pricing', 'uses' => 'SettingsController@pricing']);
		Route::post('/add-price/', ['as' => 'add_price', 'uses' => 'SettingsController@add_price']);
		Route::get('/remove-price/{id}', ['as' => 'remove_price', 'uses' => 'SettingsController@remove_price']);

		Route::get('/orders/', ['as' => 'orders', 'uses' => 'SettingsController@orders']);
		Route::get('/order-details/{id}', 'SettingsController@check_order_details')->name('admin_check_order_details');
		Route::get('/receive-order/{id}', 'SettingsController@receive_order')->name('admin_receive_order');
		
		Route::get('order-location/{id}', 'SettingsController@user_order_location')->name('order_location');
		#orders


	});
	#tables
	Route::group(['prefix' => 'table-settings'], function(){
		Route::get('/', ['as' => 'table-settings', 'uses' => 'TableController@index']);
		Route::get('/add-table', ['as' => 'add_table_page', 'uses' => 'TableController@add_table_page']);
		Route::post('/add-table-submit', ['as' => 'add_table', 'uses' => 'TableController@add_table']);
		Route::get('/edit-table/{tableid}', ['as' => 'edit_table', 'uses' => 'TableController@edit_table']);
		Route::post('/update-table/', ['as' => 'update_table', 'uses' => 'TableController@update_table']);
		Route::get('/delete-table', ['as' => 'delete_table', 'uses' => 'TableController@delete_table']);
	});

	#sizes
	Route::group(['prefix' => 'sizes'], function(){
		Route::get('/', ['as' => 'sizes', 'uses' => 'MenuController@sizes']);
		Route::post('/add-size', ['as' => 'add_size', 'uses' => 'MenuController@add_size']);
		Route::get('/remove-size/{id}', ['as' => 'remove_size', 'uses' => 'MenuController@remove_size']);
		
	});


	#Inventory
	Route::group(['prefix' => 'inventory'], function(){
		Route::get('/', ['as' => 'inventory', 'uses' => 'InventoryController@index']);
		Route::get('/history/{itemid}', ['as' => 'inventory_history', 'uses' => 'InventoryController@history']);
	});

	#Transaction
	Route::group(['prefix' => 'transaction'], function(){
		Route::get('/', ['as' => 'transaction', 'uses' => 'TransactionController@index']);
	});

	#Items
	Route::group(['prefix' => 'items'], function(){
		Route::get('/', ['as' => 'items', 'uses' => 'ItemController@index']);
		Route::get('/add', ['as' => 'add_item_page', 'uses' => 'ItemController@add_item_page']);
		Route::post('/add_item', ['as' => 'add_item', 'uses' => 'ItemController@add_item']);
		Route::get('/edit_item/{itemid}', ['as' => 'edit_item', 'uses' => 'ItemController@edit_item']);

		Route::get('/inventory/{itemid}', ['as' => 'inventory', 'uses' => 'ItemController@inventory']);

		Route::post('/update_item', ['as' => 'update_item', 'uses' => 'ItemController@update_item']);
		
		Route::get('/delete_item', ['as' => 'delete_item', 'uses' => 'ItemController@delete_item']);
		Route::get('/items_json', ['as' => 'items_json', 'uses' => 'ItemController@items_json']);

		Route::get('/adjustment/{itemid}', ['as' => 'adjustment', 'uses' => 'ItemController@adjustment']);
	});

	#Spoilages
	Route::group(['prefix' => 'spoilages'], function(){
		Route::get('/', ['as' => 'spoilages', 'uses' => 'SpoilagesController@index']);
		Route::get('/add-spoilages', ['as' => 'add_spoilages_page', 'uses' => 'SpoilagesController@add_spoilages_page']);
		Route::post('/add-spoilage', ['as' => 'add_spoilage', 'uses' => 'SpoilagesController@add_spoilage']);
	});

	#purchase
	Route::group(['prefix' => 'purchases'], function(){
		Route::get('/', ['as' => 'purchases', 'uses' => 'PurchasesController@index']);	
		Route::get('/add-purchase', ['as' => 'add_purchase_page', 'uses' => 'PurchasesController@add_purchase_page']);	
		Route::post('/add-item-purchased', ['as' => 'add_item_purchased', 'uses' => 'PurchasesController@add_item_purchased']);	
		Route::post('/add-purchased-order', ['as' => 'add_purchased_order', 'uses' => 'PurchasesController@add_purchased_order']);

		Route::get('/view-purchase-details/{purchaseid}', ['as' => 'view_purchase_details', 'uses' => 'PurchasesController@view_purchase_details']);	

		Route::post('/submit-purchase', ['as' => 'add_purchase', 'uses' => 'PurchasesController@add_purchase']);	
		
	});

	#Category
	Route::group(['prefix' => 'category'], function(){
		Route::get('/', ['as' => 'category', 'uses' => 'CategoryController@index']);
		Route::get('/add_category_page', ['as' => 'add_category_page', 'uses' => 'CategoryController@add_category_page']);
		Route::get('/edit-category/{categoryid}', ['as' => 'edit_category', 'uses' => 'CategoryController@edit_category']);

		Route::post('/add_category', ['as' => 'add_category', 'uses' => 'CategoryController@add_category']);
		Route::get('/delete_category', ['as' => 'delete_category', 'uses' => 'CategoryController@delete_category']);
		Route::post('/update_category', ['as' => 'update_category', 'uses' => 'CategoryController@update_category']);
	});

	#Menu
	Route::group(['prefix' => 'menu'], function(){
		Route::get('/', ['as' => 'menu', 'uses' => 'MenuController@index']);		
		Route::get('/add-menu-page', ['as' => 'add_menu_page', 'uses' => 'MenuController@add_menu_page']);
		Route::post('/add_menu', ['as' => 'add_menu', 'uses' => 'MenuController@add_menu']);
		Route::get('/edit_menu/{menuid}', ['as' => 'edit_menu', 'uses' => 'MenuController@edit_menu']);
		Route::post('/update_menu/', ['as' => 'update_menu', 'uses' => 'MenuController@update_menu']);
		Route::get('/delete_menu/', ['as' => 'delete_menu', 'uses' => 'MenuController@delete_menu']);
		Route::get('/data-json', ['as' => 'menuDataJson', 'uses' => 'MenuController@menuDataJson']);

	#Recipes
		Route::get('/manage_recipes/{menuid}/', ['as' => 'manage_recipes', 'uses' => 'MenuController@manage_recipes']);
		Route::post('/add_recipe', ['as' => 'add_recipe', 'uses' => 'MenuController@add_recipe']);
		Route::get('/delete_recipe/{recipeid}/{menuid}', ['as' => 'delete_recipe', 'uses' => 'MenuController@delete_recipe']);
		# Menu json
	#Prices
		Route::get('/manage-prices/{menuid}/', ['as' => 'manage_prices', 'uses' => 'MenuController@manage_prices']);
		Route::post('/add-price', ['as' => 'add_price', 'uses' => 'MenuController@add_price']);

	});

	

	#Users
	Route::group(['prefix' => 'users', 'middleware' => 'auth'], function(){
		Route::get('/', ['as' => 'users', 'uses' => 'HomeController@users_list']);	
		Route::get('/add-user', ['as' => 'add_user_page', 'uses' => 'HomeController@add_user_page']);
		Route::post('/add-user-submit', ['as' => 'add_user_submit', 'uses' => 'HomeController@add_user_submit']);
		Route::post('/update-user', ['as' => 'update_user', 'uses' => 'HomeController@update_user']);
		Route::get('/edit-user/{userid}', ['as' => 'edit_user', 'uses' => 'HomeController@edit_user']);
		Route::get('/change-password/{userid}', ['as' => 'change_password', 'uses' => 'HomeController@change_password']);
		Route::post('/submit-change-password/', ['as' => 'submit_change_password', 'uses' => 'HomeController@submit_change_password']);

	});

	#Reports
	Route::group(['prefix' => 'reports', 'middleware' => 'auth'], function(){
		Route::get('/', ['as' => 'reports', 'uses' => 'ReportsController@index']);	

		Route::get('/generate-inventory', ['as' => 'generate_inventory', 'uses' => 'ReportsController@generate_inventory']);	
		
		Route::get('/generate-sales', ['as' => 'generate_sales', 'uses' => 'ReportsController@generate_sales']);

		Route::get('/generate-purchases', ['as' => 'generate_purchases', 'uses' => 'ReportsController@generate_purchases']);	
	});
});

