<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Items;
use App\Transaction;
use App\TransactionMenu;
use App\TransactionItem;
use DB;

class TransactionController extends Controller
{
    public function index(Request $request)
    {
        $search = '';
        $query = Transaction::with('tables');

        if($request->has('search'))
        {
            $search = trim(ltrim($request->search, '0'));
            $query->where('official_receipt', 'LIKE', "%$search%")->paginate(25);
        }

    	$transaction = $query->orderBy('transaction_on', 'DESC')->paginate(25);
        
    	$data = compact('transaction', 'search');

        return view('transaction.index', $data);
    }
}
