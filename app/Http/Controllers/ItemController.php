<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\Items;
use App\PurchasedItems;
use App\TransactionItem;
use App\Spoilages;

use Response;

class ItemController extends Controller
{
    public function index(Request $request)
    {
        
        $search = '';
        if($request->has('search'))
        {
            $search = $request->search;
            $items = Items::where('name', 'LIKE', "%$search%")->paginate(10);
        }
        else
        {
            $items = Items::paginate(10);
        }
        $data = compact('items', 'search');

        return view('items.index', $data);
    }

    public function add_item(Request $request) 
    {
        //dd($request->unit);
    	 $this->validate($request, [
            'name' => 'required|max:255|unique:items,name,NULL,id,deleted_at,NULL',
            'unit' => 'required|max:255|'
        ]);

        $x = Items::create($request->only('name', 'unit'));

        $data = [
            'message' => 'Item has been added.',
            'id' => $x->id,
            'name' => $request->name,
            'unit' => $request->unit
        ];

        return redirect()->route('items')->with('message', 'Success');
    }

    public function add_item_page() 
    {
        return view('items.add-item');
    }

    

    public function delete_item(Request $request)
    {
        $item = Items::find($request->item_id);
        $item->delete();
    }

    public function edit_item(Request $request)
    {
        $items = Items::where('id','=', $request->itemid)->first();
        $data = compact('items');

        return view('items.edit-item', $data);   
    }

    public function update_item(Request $request)
    {
        if($request->name_hidden != $request->name)        {
            $name = 'required|max:255|unique:items,name,NULL,id,deleted_at,NULL';
        }else{
            $name = 'required|max:255';
        }
         $this->validate($request, [
            'name' => $name,
            'unit' => 'required|max:255'
        ]);
        $x = Items::whereId($request->id)->update($request->only('name', 'unit'));
       
        $data = [
            'message' => 'Item has been updated.',
            'id' => $request->id,
            'name' => $request->name,
            'unit' => $request->unit
        ];
        return redirect()->route('edit_item', $request->id)->with('message', 'Success');
    }

    public function items_json(Request $request)
    {
        $search = ($request->has('search')) ? trim($request->search) : '';
        $menu = Items::select('name', DB::raw('CONCAT(id,"~~",unit) AS idunit'))->where('name', 'LIKE', '%' . $search . '%')->lists('name', 'idunit');

        return response()->json($menu);
    }

    public function inventory(Request $request, $id)
    {
        $paginate = 12;
        $page = ($request->has('page'))? trim($request->page) : 1;
        
        $item = Items::whereId($id)->first();
        $purchase = PurchasedItems::select([ 
                                            DB::raw("'Purchase ' AS method"),
                                            DB::raw('purchase_date AS transaction_on'), 
                                            DB::raw("'' AS name"), 
                                            'quantity' 
                                        ])
                    ->leftJoin('purchases', 'purchases.id', '=', 'purchased_items.purchase_id')
                    ->where('purchased_items.item_id', $id);

        $spoilages = Spoilages::select([ 
                                        DB::raw("'Spoilage' AS method"),
                                        DB::raw('spoilage_date AS transaction_on'), 
                                        'users.name', 
                                        DB::raw('(-1 * spoilages.quantity) AS quantity') 
                                       ])
                    ->leftJoin('users', 'users.id', '=', 'spoilages.user_id')
                    ->where('spoilages.item_id', $id);

        $used = TransactionItem::select([ DB::raw("'Transaction' AS method"),'transaction.transaction_on', 'users.name',  DB::raw('(-1 * servings) AS quantity') ])
                ->leftJoin('transaction_menu', 'transaction_menu.id', '=', 'transaction_item.transaction_menu_id')
                ->leftJoin('transaction', 'transaction.id', '=', 'transaction_menu.transaction_id')
                ->leftJoin('users', 'users.id', '=', 'transaction.user_id')
                ->where('transaction_item.item_id', $id)
                ->unionall($purchase)
                ->unionall($spoilages)
                ->orderBy('transaction_on', 'DESC')
                ->get();
        #dd($used);     
        $slice = array_slice($used->toArray(), $paginate * ($page - 1), $paginate, true);
        $stocks = new \Illuminate\Pagination\LengthAwarePaginator($slice, count($used), $paginate, $page);
        #dd($id);
        $path = $id.'/';
        $stocks->setPath($path);
        #dd($stocks);
        $data = compact('item', 'stocks');

        return view('items.inventory', $data);
    }

    public function adjustment(Request $request)
    {

        return view('items.adjustment');
    }
}
