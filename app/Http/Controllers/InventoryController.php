<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;
use App\Items;
use App\PurchasedItems;
use App\TransactionItem;
use App\Spoilages;

use Response;

class InventoryController extends Controller
{
    public function index(Request $request)
    {

    	$search = '';
        if($request->has('search'))
        {
            $search = $request->search;
            $items = Items::where('name', 'LIKE', "%$search%")->paginate(25);
        }
        else
        {
            $items = Items::paginate(25);
        }
       # dd($items);
        
       # return $items;

        $data = compact('items', 'search');

        return view('inventory.index', $data);
    }
}
