<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Purchases;
use App\Menu;
use Response;
use App\PurchasedItems;
use App\Transaction;
use App\TransactionMenu;
use DB;

class HomeController extends Controller
{
    public function __construct()
    {
        #$this->middleware('auth');
    }

    public function login_page()
    {
        return view('login.index');
    }

    public function user_login(Request $request)
    {
        #dd($request->password);

        $username = trim($request->username);
        $password = trim($request->password);

        if (Auth::attempt(['username' => $username, 'password' => $password]))
        {
            $status = Auth::user()->status;

            if($status == 0)
            {
                // disable login
                Auth::logout();
                return redirect()->route('login_page')->with('login-message', 'Login inactive!');
            }
            else if($status == 2)
            {
                // reset
            }

            return redirect()->route('inventorylist');
        }

        return redirect()->route('login_page')->with('login-message', 'Invalid Username/Password!');
    }

    public function add_user_page()
    {

        return view('users.add-user');
    }

    public function add_user_submit(Request $request)
    {
         $this->validate($request, [
            'name' => 'required|max:255',
            'username' => 'required|max:255|unique:users,username,NULL,id,status,1',
            'password' => 'required|max:255|'
        ]);

        $input = $request->only('name','username', 'access');
        $input['password'] = bcrypt($request->password);
        $input['status'] = 1;

        $x = User::create($input);

        $data = [
            'message' => 'User has been added.',
        ];

        return redirect()->route('add_user_page')->with('message', 'Success');
    }

    public function edit_user(Request $request)
    {
        $user = User::where('id','=' ,$request->userid)->get();
        $data = compact('user');
       # dd($data);
        return view('users.edit-user', $data);
    }

    public function update_user(Request $request)
    {
        if($request->hidden_username != $request->username)        {
            $username = 'required|max:255|unique:users,username';
        }else{
            $username = 'required|max:255';
        }
        $this->validate($request, [
            'username' => $username,
            'name' => 'required|max:255'
        ]);
        $update = User::whereId($request->id)->update($request->only('username', 'name', 'access', 'status'));

         $data = [
            'message' => 'User has been updated.'
        ];

        return redirect()->route('edit_user', $request->id)->with('message', 'Success');
    }

    public function change_password(Request $request)
    {
        $userid = $request->userid;
        $data = compact('userid');     

        return view('users.change-password', $data);
    }  

    public function submit_change_password(Request $request)
    {
         $this->validate($request, [
            'password'     => 'required|max:255|confirmed',
            'password_confirmation' => 'required|max:255'
        ]);
        
        $request->password = bcrypt($request->password);

        if($request->password == $request->new_password)
        {
             $user = User::find($request->id);
             $user->password = bcrypt($request->password);
             $user->save();

             $data = [
             'message' => 'User has been updated.'
            ];
        }

        return redirect()->route('change_password', $request->id)->with('message', 'Success');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('login_page');
    }

    public function dashboard_home()
    {
        $menus = Menu::get();

        $data = compact('menus');
        return view('dashboard.index', $data);
    }

    public function annual_sales_json()
    {

        $years = Transaction::select(DB::raw('DISTINCT YEAR(transaction_on) as year'))->get();
        $transactions = Transaction::with('transaction_menu')->get();

       # dd($transactions);

        $dataArr = [];

        foreach($years as $year)
        {
            for($i=1; $i<=12; $i++)
            {
               # $dataArray[] = collect(["name", "data"]);

                $filter = $transactions->filter(function($value, $key) use($i, $year){
                    return date("Ym", strtotime($value->transaction_on)) == $year->year.str_pad($i, 2, '0',STR_PAD_LEFT);
                })->sum(function($m){
                    return $m->transaction_menu->sum('price');
                });

                $dataArr[$year->year][] = $filter;
            }
        }
        
        $testArr = [];

        foreach ($dataArr as $key => $value) 
        {
            $testArr[] = ['name' => $key, 'data' => $value];
        }

        $data = collect($testArr)->toJson();

        return response()->json(collect($testArr)); 
    }

    public function monthly_sales_per_menu_json(Request $request)
    {
        #$request->menuid = 1;
        $years = Transaction::select(DB::raw('DISTINCT YEAR(transaction_on) as year'))->get();

        $menu_transactions = Transaction::with(['transaction_menu' => function ($query) use($request) {
            $query->where('transaction_menu.menu_id', $request->menuid);
        }], 'transaction_menu.menu')->get();

        #$menu_transactions = Transaction::has('transaction_menu')->get();
        #dd($menu_transactions);
       # $transaction_menu = TransactionMenu::with('transaction_reverse')->where('menu_id', $request->menuid)->get();
        foreach($years as $year)
        {
            for($i=1; $i<=12; $i++)
            {
                $filter = $menu_transactions->filter(function($value, $key) use($i, $year){
                    return date("Ym", strtotime($value->transaction_on)) == $year->year.str_pad($i, 2, '0',STR_PAD_LEFT);
                })->sum(function($m){
                    return $m->transaction_menu->count('id');
                });

                $dataArr[$year->year][] = $filter;
            }
        }

       # dd($dataArr);
        $testArr = [];

        foreach ($dataArr as $key => $value) 
        {
            $testArr[] = ['name' => $key, 'data' => $value];
        }

        $data = collect($testArr)->toJson();
        return response()->json(collect($testArr)); 

    }

    public function users_list()
    {
        $users = User::paginate(10);
        $data = compact('users');
        
        return view('users.index', $data);
    }

    public function menu()
    {
        return view('menu.index');
    }

    public function inventory()
    {
        return view('inventory.index');
    }
}
