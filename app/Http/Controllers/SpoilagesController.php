<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use Response;

use App\Items;
use App\Spoilages;

class SpoilagesController extends Controller
{
    public function index(Request $request)
    {   
        $search = '';

        if($request->has('search'))
        {
            $search = $request->search;
            $spoilages = Spoilages::with('Items')->whereHas('Items', function ($query) use ($search) {
                                                        $query->where('Items.name', 'LIKE', "%$search%");
                                                    })
            ->paginate(10);
        }
        else
        {
            $spoilages = Spoilages::with('Items')->has('Items')->paginate(10);
        }


        $data = compact('spoilages', 'search');
        
    	return view('spoilages.index', $data);
    }

    public function add_spoilages_page()
    {
    	$items = Items::get();
    	$data = compact('items');

    	return view('spoilages.add-spoilages', $data);
    }

    public function add_spoilage(Request $request)
    {
        #dd($request->only('item_id', 'quantity', 'user_id', 'spoilage_date'));
        
        $this->validate($request, [
                'item_id' => 'required|max:255',
                'quantity' => 'required|max:255',
                'spoilage_date' => 'required'
            ],
            $messages = [
                'spoilage_date.required' => 'The Spoilage Date field is required.',
                'item_id.required'  => 'The Item name field is required.',
                'quantity.required' => 'The Quantity field is required.'
            ]
        );
        

        $x = Spoilages::create($request->only('item_id', 'quantity', 'user_id', 'spoilage_date'));

        $data = [
            'message' => 'Item has been added.',
            'id' => $x->id,
            'name' => $request->name,
            'unit' => $request->unit
        ]; 


        return redirect()->route('add_spoilages_page')->with('message', 'Success');
    }
}
