<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Storage;
use App\Menu;
use App\MenuCategory;
use App\Recipes;
use App\Pricing;
use App\User;
use App\Sizes;
use Response;


use \Carbon\Carbon;

class MenuController extends Controller
{
    public function index(Request $request)
    {

        $search = '';
        if($request->has('search'))
        {
            $search = $request->search;
            $menus = Menu::where('menu', 'LIKE', "%$search%")->paginate(10);
        }
        else
        {
            $menus = Menu::with('Category')->paginate(10);
        }
        
        $data = compact('menus', 'search');

        return view('menu.index', $data);
    }

    public function add_menu_page(Request $request) 
    {
        $categories = MenuCategory::get();
        $data = compact('categories');

        return view('menu.add-menu', $data);
    }

    public function add_menu(Request $request)
    {
        #dd($request);
        $this->validate($request, [
            'menu' => 'required|unique:menu,menu,NULL,id,category_id,'.$request->category_id.',deleted_at,NULL|max:255',
            'category_id' => 'required',
            'details' => 'required',
            'image' => 'required|image|mimes:jpeg|max:2048'
        ]);

        if ($request->hasFile('image')) {
            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);
            Storage::disk('pos')->put( $imageName, 'Contents');
        }

        $input = $request->only('category_id','menu','details');
        $input['user_id'] = Auth::user()->id;
        $input['image'] = $imageName;
        $input['created_at'] = Carbon::now();

        $x = Menu::create($input);

        $data = [
            'message' => 'Menu has been added.',
            'id' => $x->id,
            'name' => $request->menu
        ];

        return redirect()->route('add_menu_page')->with('message', 'Success');
    }

    public function edit_menu(Request $request)
    {
        //dd($request->menuid);
        $menus = Menu::where('id','=', $request->menuid)->first();
        $categories = MenuCategory::get();

        $data = compact('menus', 'categories');

        return view('menu.edit-menu', $data);
    }

    public function update_menu(Request $request)
    {
        //dd($request);
         $this->validate($request, [
            'menu' => 'required|unique:menu,menu,'.$request->id.',id,category_id,'.$request->category_id.',deleted_at,NULL|max:255',
            'category_id' => 'required'
        ]);

        if ($request->hasFile('image')) {
             $this->validate($request, [
                'image' => 'required|image|mimes:jpeg|max:2048'
            ]);

            $imageName = time().'.'.request()->image->getClientOriginalExtension();
            request()->image->move(public_path('images'), $imageName);

            $menu = Menu::find($request->id);
            $menu->image = $imageName;
            $menu->save();

        }
            $menu = Menu::find($request->id);
            $menu->category_id   = $request->category_id;
            $menu->menu          = $request->menu;
            $menu->selling_price = $request->selling_price;
            $menu->details = $request->details;
            $menu->save();
        
        $data = [
            'message' => 'Menu has been added.',
            //'id' => $x->id,
            'name' => $request->menu
        ];

        return redirect()->route('edit_menu', $request->id)->with('message', 'Success');
    }

    public function delete_menu(Request $request)
    {

        $menu = Menu::find($request->menuid);
        $menu->delete();
        #return redirect()->route('index');
    }

    public function manage_recipes(Request $request, $priceid)
    {
       # dd($request);
        #echo $priceid; exit;
        $recipes = Recipes::where('pricing_id','=', $priceid)->with('Item')->has('Item')->paginate(8);
        
        $price = Pricing::with('menu')->where('id','=', $priceid)->first();

        $data = compact('priceid', 'recipes', 'price');
        
        return view('menu.manage-recipes', $data);
    }

    public function add_recipe(Request $request)
    {
        $this->validate($request, [
            'item' => 'required|max:255|unique:recipes,item_id,NULL,id,pricing_id,'.$request->pricing_id,
            'serving' => 'required|max:255'
        ]);

        $recipes = new Recipes;
        $recipes->item_id = $request->item;
        $recipes->pricing_id = $request->pricing_id;
        $recipes->serving = $request->serving;
        $recipes->save();

         $data = [
            'message' => 'Recipe has been added.',
            'id' => $request->menu_id,
            'name' => $request->name
        ];

        return redirect()->route('manage_recipes', $request->pricing_id)->with('message', 'Success');
    }

    public function delete_recipe($recipeid, $priceid)
    {
       #dd($menuid);
        $menu = Recipes::find($recipeid);
        $menu->delete();
        $data = [
            'message' => 'Recipe has been deleted.'
        ];
        return redirect()->route('manage_recipes', $priceid)->with('message', 'Recipe has been deleted.');
    }

    public function manage_prices($menuid)
    {
        $sizes = Sizes::get();

        $prices = Pricing::where('menu_id', $menuid)->get();

        $data = compact('menuid', 'sizes', 'prices');

        return view('pricing.index', $data);
    }

    public function add_price(Request $request)
    {
        $this->validate($request, [
            'size' => 'unique:pricing,size_id,'.$request->size.',id,menu_id,'.$request->menu_id.',deleted_at,NULL',
            'price' => 'required|numeric'
        ]);

        $pricing = new Pricing;
        $pricing->menu_id = $request->menu_id;
        $pricing->size_id = $request->size;
        $pricing->price = $request->price;
        $pricing->created_at = Carbon::now()
 ;       $pricing->save();

         $data = [
            'message' => 'Menu has been updated.',
            'id' => $pricing->id,
            'name' => $request->menu_id
        ];

        return redirect()->route('manage_prices', $request->menu_id)->with('message', 'Success');  
    }

    public function menuDataJson(Request $request)
    {
        $search = ($request->has('search')) ? trim($request->search) : '';
       # $menu = Menu::with('pricing')->where('menu', 'LIKE', '%' . $search . '%')->lists('menu', 'id');
       # return $menu;
        $menu = Pricing::with('menu')->with('sizes')
                    ->whereHas('menu', function($query) use($search){ 
                        $query->where('menu', 'LIKE', '%' . $search . '%');
                    })->get()->lists('custom_name', 'id');
        #return $menu;
       /* $menu->transform(function($item){
            return [$item->id => $item->menu->menu.'-'.$item->sizes->size];
        });
       # return $menu->toArray(); exit;
        $flatten = $menu->flatten(2);
        return $flatten;exit;*/
        /*$menu = DB::select("SELECT pricing.id, menu.menu FROM pricing
                            LEFT JOIN menu ON menu.id = pricing.menu_id
                            WHERE menu.menu = 
                            ")*/
        return response()->json($menu);
    }

    public function sizes()
    {
        $sizes = Sizes::get();

        $data = compact('sizes');

        return view('sizes.index', $data);
    }

    public function add_size(Request $request)
    {
        $this->validate($request, [
            'size' => 'required|unique:sizes,size,NULL,id,deleted_at,NULL|max:255'
        ]);

        $size = new Sizes;
        $size->size = $request->size;
        $size->created_at = Carbon::now();
        $size->save();

        $data = [
            'message' => 'Size has been added.',
            'id' => $size->id,
            'size' => $request->size
        ];

        return redirect()->route('sizes')->with('message', 'Success');
    }

    public function remove_size($id)
    {
        $size = Sizes::find($id);
        $size->delete();

        return redirect()->route('sizes')->with('message2', 'Success');
    }

   
}
