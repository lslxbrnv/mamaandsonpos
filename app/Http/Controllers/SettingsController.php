<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Response;
use App\Settings;
use App\Terminals;
use App\Sizes;
use App\Pricing;
use App\Orders;
use App\OrderMenu;
use App\OnlineTransaction;
use App\OnlineTransactionMenu;
use \Carbon\Carbon;

class SettingsController extends Controller
{
    public function index()
    {
        $company = Settings::first();
        $data = compact('company');
        return view('settings.index', $data);
    }

    public function update_company(Request $request)
    {
        $check_settings = Settings::first();
        if(is_null($check_settings))
        {   
            $x = Settings::create($request->only('name', 'address', 'telephone_number', 'tin', 'serial_number', 'owner', 'permit_number', 'vat'));
        }else{
             $x = Settings::whereId(1)->update($request->only('name', 'address', 'telephone_number', 'tin', 'serial_number', 'owner', 'permit_number', 'vat'));
        }


         $data = [
            'message' => 'Company details has been updated.'
        ];

        return redirect()->route('settings', 1)->with('message', 'Success');
    }

    /* Terminals */

    public function terminals()
    {
        $terminals = Terminals::paginate(10);
        $data = compact('terminals');

        return view('terminals.index', $data);
    }

    public function sizes()
    {
        $sizes = Sizes::get();
        $data = compact('sizes');
        return view('sizes.index', $data);
    }

    public function add_size(Request $request)
    {
        $this->validate($request, [
            'size' => 'required|unique:sizes,size,NULL,id,deleted_at,NULL|max:255'
        ]);

        $size = new Sizes;
        $size->size = $request->size;
        $size->created_at = Carbon::now();
        $size->save();

        $data = [
            'message' => 'Size has been added.',
            'id' => $size->id,
            'size' => $request->size
        ];

        return redirect()->route('sizes')->with('message', 'Success');
    }

    public function remove_size($id)
    {
        $size = Sizes::find($id);
        $size->delete();

        return redirect()->route('sizes')->with('message2', 'Success');
    }

    public function pricing($menuid)
    {
        $prices = Pricing::with('sizes')->where('menu_id', $menuid)->get();
        $sizes = Sizes::get();
        $data = compact('menuid', 'sizes', 'prices');

        return view('pricing.index', $data);
    }

    public function add_price(Request $request)
    {
         $this->validate($request, [
            'size' => 'required',
            'price' => 'required|numeric',
            'menu_id' => 'unique:pricing,menu_id,NULL,id,size_id,'.$request->size.',deleted_at,NULL'
        ]);

        $pricing = new Pricing;
        $pricing->menu_id = $request->menu_id;
        $pricing->size_id = $request->size;
        $pricing->price = $request->price;
        $pricing->created_at = Carbon::now();
        $pricing->save();

        $data = [
            'message' => 'Menu has been u[dated.',
            'id' => $pricing->id,
            'name' => $request->menu_id
        ];

        return redirect()->route('pricing', $request->menu_id)->with('message', 'Success');  
    }

    public function remove_price($id)
    {
        $pricing = Pricing::find($id);
        $pricing->delete();

        return redirect()->route('pricing')->with('message2', 'Success');
    }

    public function edit_terminal(Request $request)
    {

        return view('terminals.edit-terminal');

    }

    public function orders()
    {
        $transactions = OnlineTransaction::where('status', '!=', 0)->get();

        $data = compact('transactions');

        return view('orders.index', $data);
    }

    public function check_order_details($id)
    {
        $transaction = OnlineTransaction::with('online_user')->whereId($id)->first();
        #return $transaction; exit;

        $orders = OnlineTransactionMenu::with('menu')->with('pricing')->where('transaction_id', $id)->get();

        $data = compact('transaction', 'orders');

        return view('orders.orders-details', $data);
    }

    public function receive_order($id)
    {
       # dd($id);
        $update_transaction_status = OnlineTransaction::find($id);
        if($update_transaction_status->status == 1){
            $update_transaction_status->status = 2;
        }else if($update_transaction_status->status == 2)
        {
            $update_transaction_status->status = 3;

        }
        $update_transaction_status->updated_at = Carbon::now();
        $update_transaction_status->save();
        
        return redirect()->route('admin_check_orders')->with('message', 'Status set to received.');

    }

     public function user_order_location($id)
    {
        return \App\DeliveryLocation::select(['lat','long'])->where('transaction_id', $id)->latest()->first();
    }
}
