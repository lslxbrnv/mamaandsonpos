<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Tables;
use Response;

class TableController extends Controller
{
    public function index()
    {
        $tables = Tables::where('id','!=','1')->get();
        $data = compact('tables');

        return view('tables.index', $data);
    }

    public function add_table_page(Request $request)
    {
        return view('tables.add-table');
    }

    public function add_table(Request $request)
    {
    	 $this->validate($request, [
            'name' => 'required|max:255|unique:tables,name,NULL,id,deleted_at,NULL'
        ]);

        $x = Tables::create($request->only('name'));

        $data = [
            'message' => 'Table has been added.',
            'id' => $x->id,
            'name' => $request->name,
            'unit' => $request->unit
        ];
        return redirect()->route('add_table_page', $request->id)->with('message', 'Success');
    }

    public function edit_table(Request $request)
    {
    	$table = Tables::where('id','=', $request->tableid)->first();
        $data = compact('table');

        return view('tables.edit-table', $data);   
    }

    public function update_table(Request $request)
    {
    	if($request->name_hidden != $request->name)        {
            $name = 'required|max:255|unique:tables,name';
        }else{
            $name = 'required|max:255';
        }
         $this->validate($request, [
            'name' => $name
        ]);

        $x = Tables::whereId($request->id)->update($request->only('name'));
       
        $data = [
            'message' => 'Table has been updated.',
            'id' => $request->id,
            'name' => $request->name
        ];
        return redirect()->route('edit_table', $request->id)->with('message', 'Success');  
    }

    public function delete_table(Request $request)
    {
    	$table = Tables::find($request->id);
        $table->delete();
    }
}