<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;

use App\Purchases;
use App\PurchasedItems;

class PurchasesController extends Controller
{
    public function index(Request $request)
    {
        $search = '';

        if($request->has('search'))
        {
            $search = $request->search;
            $purchases = Purchases::where('receipt', 'LIKE', "%$search%")->orderBy('id', 'desc')->paginate(25);
        }
        else
        {
            $purchases = Purchases::orderBy('id', 'desc')->paginate(25);
        }
        #$purchases = Purchases::paginate(10);
        $data = compact('purchases', 'search');
       # dd($data);
        return view('purchases.index', $data);
    }

    public function add_purchase_page()
    {
        return view('purchases.add-purchase');    	
    }

    public function add_purchase(Request $request)
    {
    	//dd($request);
    }

    public function add_item_purchased(Request $request)
    {
        //dd($request);
    }

    public function add_purchased_order(Request $request)
    {
        if($request->purchased_items_array != null)
        {
            $array = json_decode($request->purchased_items_array, true);
        }
        $this->validate($request, [
            'receipt' => 'required|max:255|unique:purchases,receipt',
            'purchase_date' => 'required',
            'purchased_items_array' => 'required'
        ]);

     /*   $array = json_decode($request->purchased_items_array, true);
        $input = $request->only('receipt','purchase_date');*/

        $input = $request->only('receipt','purchase_date');
        $x = Purchases::create($input);
        $purchase_id = $x->id;

        foreach($array as $key => $items)
        {
            foreach($items as $id => $item)
            {   
                //dd($items);
                $purchased_items = new PurchasedItems;
                $purchased_items->purchase_id = $purchase_id;
                $purchased_items->item_id = $item['item_id'];
                $purchased_items->quantity = $item['quantity'];
                $purchased_items->price = $item['price'];
                $purchased_items->save();
            }
        }
          $data = [
            'message' => 'Purchases has been saved.',
            'id' => $x->id,
            'name' => $request->name
        ];
       return redirect()->route('add_purchase_page')->with('message', 'Success');
    }

    public function view_purchase_details($purchaseid)
    {
        $purchases = PurchasedItems::where('purchased_items.purchase_id','=', $purchaseid)->with('Items')->paginate(10);
        #dd($purchases);
        $data = compact('purchases');
        return view('purchases.view-purchase-details', $data);

    }
}
