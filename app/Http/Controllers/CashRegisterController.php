<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Menu;
use App\Http\Requests;
use App\Terminals;
use App\Transaction;
use App\TransactionMenu;
use App\TransactionItem;
use App\TransactionPayments;
use App\CashRegister;
use App\CashRegisterMenu;
use App\CashRegisterItems;
use App\Company;
use App\Recipes;
use App\Tables;
use App\Items;
use Validator;
use Session;
use Auth;
use DB;
use \Carbon\Carbon;

class CashRegisterController extends Controller
{
    public function sales(Request $request)
    {
        return view('sales.index');
    }

    public function datetime()
    {
        $data = [ 'datetime' => Carbon::now()->toDayDateTimeString() ];
        return response()->json( $data );
    }

    public function removeItem(Request $request)
    {
        $id = $request->id;
        CashRegisterItems::whereId( $id )->delete();

        return response()->json( [ 'status' => 'ok' ] ); 
    }

    public function cancel(Request $request)
    {
        $user_id = Auth::user()->id;
        // Delete all items in cart
        $row = CashRegister::whereId( $request->cash_register_id )->first();

        CashRegisterItems::whereIn('cash_register_menu_id', CashRegisterMenu::whereCashRegisterId( $request->cash_register_id )->lists('id') )->delete();
        CashRegisterMenu::whereCashRegisterId( $request->cash_register_id )->delete();
        CashRegister::whereId( $request->cash_register_id )->delete();

        return response()->json( [ 'status' => 'ok', 'transaction_type' => $row->type ] ); 
    }

    public function tables(Request $request)
    {
        $data = [];

        if($request->takeout == 1)
        {
            $tables = Tables::with('cash_register_reverse')->whereId(1)->get();
        }
        else
        {
            $tables = Tables::with('cash_register_reverse')->where('id', '<>', 1)->get();
        }
        $n = 0;
        foreach ($tables as $row)
        {
            $status = true;

            if(is_null($row->cash_register_reverse->first()))
            {
                $status = false;
            }

            $data['tables'][$n] = [
                'id' => $row->id,
                'name' => $row->name,
                'cash_register' => $status
            ];
            $n++;
        }

        return response()->json( $data );
    }

    public function discount(Request $request)
    {
        CashRegister::whereId($request->cash_register_discount_id)
        ->update([
                'discount' => ($request->cash_register_discount/100)
            ]);
        $row = CashRegister::whereId($request->cash_register_discount_id)->first();
        $table = Tables::whereId($row->table_id)->first();

        $data = [
            'status' => 'ok', 
            'table_id' => $table->id, 
            'table_number' => $table->name,
            'transaction_type' => 'DINE IN'
        ];
        return response()->json( $data );
    }

    public function addCustomer(Request $request)
    {
        #dd($request->all());
        $input = $request->only(['table_id', 'table_id']);
        $rules = [
            'table_id' => 'required',
            'customer_name' => 'required',
        ];
        $msg = [
            'table_id.required' => 'Table is required!',
            'customer_name.required' => 'Customer Name is required!'
        ];

        $validator = Validator::make( $request->all(), $rules, $msg );
        // Validate
        if( $validator->fails() )
        {
            foreach ($validator->errors()->all() as $message)
            {
                return response()->json( [ 'status' => 'error', 'message' => $message ] ); 
            }
        }

        $cash_register = CashRegister::create([
                'customer_name' => $request->customer_name,
                'user_id' => Auth::user()->id,
                'added_on' => Carbon::now(),
                'table_id' => $request->table_id,
                'discount' => 0,
                'type' => (($request->transaction_type == 'DINE IN')? 0 : 1)
            ]);

        $data = [ 
            'status' => 'ok', 
            'cash_register_id' => $cash_register->id, 
            'table_id' => $request->table_id, 
            'table_number' => $request->table_number, 
            'transaction_type' => $request->transaction_type 
        ];

        return response()->json( $data ); 
    }

    public function customerTransaction(Request $request)
    {
        $data = [];
        $data['status'] = '';

        $record = CashRegister::whereTableId( $request->table_id )->first();

        if( $record )
        {
            $data['status'] = 'ok';
            $data['id'] = $record->id;
            $data['customer_name'] = $record->customer_name;
            $data['type'] = $record->type;
            $data['discount'] = $record->discount * 100;
        }

        return response()->json( $data ); 
    }

    public function menuSaleAdd(Request $request)
    {
        #dd($request);
        $input = $request->only('cash_register_id', 'menu_id', 'search_quantity');
        #dd($input);
        $rules = [
            'menu_id' => 'required|exists:pricing,id',
            'cash_register_id' => 'required'
        ];
        $msg = [
            'menu_id.required' => 'Menu is required!',
            'menu_id.exists' => 'Menu not exists!',
            'cash_register_id.required' => 'No Customer Selected!'
        ];

        $validator = Validator::make( $input, $rules, $msg );

        if( $validator->fails() )
        {
            $messages = $validator->errors();

            foreach ($messages->all() as $message)
            {
                return response()->json( [ 'status' => 'error', 'message' => $message ] ); 
            }
        }
        if(empty($request->search_quantity) || $request->search_quantity < 1){
            $split_search_menu = 1;
        }else{
            $split_search_menu = $request->search_quantity;
        }


        /*$split_search_menu = explode('*', $request->search_menu);

        if(is_numeric($request->search_menu))
        {
            $split_search_menu = [$request->search_menu, 1];
        }
        elseif(count($split_search_menu) >= 3)
        {
            return response()->json( [ 'status' => 'error', 'message' => 'Invalid search input!' ] ); 
        }*/

      #  dd($split_search_menu);

        for($i = 0; $i <= trim($split_search_menu)-1; $i++)
        {
            $cash_register_menu = CashRegisterMenu::create(  [
                        'cash_register_id' => $request->cash_register_id,
                        'pricing_id' => $request->menu_id,
                        'search_quantity' => $request->search_quantity
                    ] );

            $menu_items = Recipes::with('Item')->has('Item')->wherePricingId( $request->menu_id )->get();
            foreach ($menu_items as $row)
            {
                CashRegisterItems::create( [
                        'cash_register_menu_id' => $cash_register_menu->id,
                        'servings' => $row->serving,
                        'pricing_id' => $request->menu_id,
                        'item_id' => $row->item_id
                    ] );
            }
        }

        $data = [
            'status' => 'success',
            'cash_register_id' => $request->cash_register_id
        ];

        return response()->json( $data );
    }

    public function menuSaleDelete(Request $request)
    {
        $id = $request->menu_id;
        #$crm = CashRegisterMenu::whereId( $id )->first();
        CashRegisterItems::whereCashRegisterMenuId( $id )->delete();
        CashRegisterMenu::whereId( $id )->delete();

        $data = [
            'status' => 'ok'
        ];

        return response()->json($data);
    }

    public function menuSaleList(Request $request)
    {
        $n = 0;
        $data = [ 'cart' => [] ];
        $user_id = Auth::user()->id;
        $id = $request->id;
        $exists = [];
        $total_amount_due = 0;
        $amount_due = 0;
        $discount = 0;

        $record = CashRegister::whereId( $id )->first();
        $menus = CashRegisterMenu::whereCashRegisterId( $id )->get();


        foreach ($menus as $item)
        {
            $i = 0;
            $items = [];
            $total = $item->pricing->price;

            $amount_due += $total;
            $menu_items = CashRegisterItems::whereCashRegisterMenuId( $item->id )->get();

            foreach ($menu_items as $row)
            {
                $items[$i] = [
                    'id' => $row->id,
                    'name' => $row->Items->name,
                    'qty' => $row->qty
                ];
                $i++;
            }

            $data['cart'][$n] = [
                'id' => $item->id,
                'menu_id' => $item->menu_id,
                'name' => $item->pricing->menu->menu,
                'price' => $item->pricing->price,
                'price_formatted' => number_format($item->pricing->price, 2),
                'total' => $total,
                'total_formatted' => number_format($total, 2),
                'items' => $items
            ];

            $n++;
        }
        #echo $amount_due. ' ' .$record->discount; exit;
        if(is_null($record)){
            $discount = $amount_due * 0;
        }else{
            $discount = $amount_due * $record->discount;
        }
        $total_amount_due = $amount_due - $discount;

        $data['amount_due'] = $amount_due;
        $data['discount_amount'] = $discount;
        $data['total_amount_due'] = $total_amount_due;

        $data['amount_due_formatted'] = number_format($amount_due, 2);
        $data['discount_amount_formatted'] = number_format($discount, 2);
        $data['total_amount_due_formatted'] = number_format($total_amount_due, 2);

        return response()->json($data);
    }

    public function billout(Request $request)
    {
    	$input = $request->only(['show_customer_name', 'show_amount_due', 'amount_tendered']);
        $rules = [
            'cash_register_id' => 'required',
            'show_customer_name' => 'required',
            'show_amount_due' => 'required',
            'discount_amount' => 'numeric|min:0|max:100',
            'amount_tendered' => 'required|numeric|min:' . $request->show_amount_due
        ];
        $msg = [
            'cash_register_id.required' => 'No Transaction selected!',
            'show_customer_name.required' => 'Customer Name is required!',
            'show_amount_due.required' => 'No order has been place!',
            'discount_amount.numeric' => 'Discount required numeric!',
            'amount_tendered.required' => 'Amount Tendered is required!'
        ];

        $validator = Validator::make( $request->all(), $rules, $msg );
        // Validate
        if( $validator->fails() )
        {
			foreach ($validator->errors()->all() as $message)
			{
			    return response()->json( [ 'status' => 'error', 'message' => $message ] ); 
			}
        }
        // Success
        $discount = 0;
        $user_id = Auth::user()->id;
        $company = Company::first();
        $terminal = Terminals::whereIp( $request->ip() )->first();
        #dd($request->ip());
        // Count Terminal Records and add 1 to increment records
        $or = Transaction::whereTerminalId( $terminal->id )->count('id') + 1;
        $cash_register = CashRegister::whereId( $request->cash_register_id )->first();
        $cash_register_menu = CashRegisterMenu::whereCashRegisterId( $cash_register->id )->get();
        // Save Transaction
       # return $cash_register_menu; exit;
        $transaction = Transaction::create( [
                'official_receipt' => $or,
                'customer_name' => $cash_register->customer_name,
                'table_id' => $cash_register->table_id,
                'terminal_id' => $terminal->id,
                'user_id' => $cash_register->user_id,
                'transaction_on' => Carbon::now(),
                'type' => $cash_register->type,
                'discount' => $cash_register->discount
            ] );
        // Save Payments
        TransactionPayments::create( [
                'transaction_id' => $transaction->id,
                'type' => $request->payment_type,
                'amount' => $request->amount_tendered
            ] );
        // Save Transaction Items
        foreach ($cash_register_menu as $menu)
        {
            #return $menu;exit;
            $transaction_menu = TransactionMenu::create( [
                    'transaction_id' => $transaction->id,
                    'pricing_id' => $menu->pricing_id,
                    'price' => $menu->pricing->price,
                    'discount' => $menu->discount,
                    'vat' => $company->vat
                ] );

            $cash_register_items = CashRegisterItems::whereCashRegisterMenuId( $menu->id )->get();

            foreach ($cash_register_items as $item)
            {
                TransactionItem::create( [
                        'transaction_menu_id' => $transaction_menu->id,
                        'pricing_id' => $item->pricing_id,
                        'item_id' => $item->item_id,
                        'servings' => $item->servings
                    ] );
            }
            // Delete all items in cart
            //CashRegisterItems::whereCashRegisterMenuId( $cash_register_menu->id )->delete();
        }
        // Delete Item, Menu and Cash Register Records
        CashRegisterItems::whereIn('cash_register_menu_id', CashRegisterMenu::whereCashRegisterId( $cash_register->id )->lists('id') )->delete();

		return response()->json( [ 'status' => 'ok', 'id' => $transaction->id, 'cash_register_id' => $cash_register->id ] ); 
    }

    public function verify(Request $request, $id)
    {
        $cash_register = CashRegister::whereId( $id )->first();
        $cash_register_menu = CashRegisterMenu::whereCashRegisterId( $id )->get();
       
        $data = compact('cash_register', 'cash_register_menu');

        return view('sales.verify', $data);
    }

    public function authenticateAdmin(Request $request)
    {
        $username = trim($request->username);
        $password = trim($request->password);
        $data = [];

        if (Auth::once(['username' => $username, 'password' => $password, 'access' => 1, 'status' => 1]))
        {
        	$data['status'] = 'ok';
        	return response()->json($data);
        }

        $data['status'] = 'error';

        return response()->json($data);
    }

    public function officialReceiptPrint(Request $request, $id)
    {
        $total = 0;
        $vat = 0;
        $discount = 0;
        $change = 0;
        #$tendered = 0;
        $ip = $request->ip();
        
        $company = Company::first();
        $terminal = Terminals::whereIp( $ip )->first();
        $transaction = Transaction::with('user', 'tables')->whereId( $id )->first();
        $menus = TransactionMenu::with('pricing')->whereTransactionId( $id )->groupBy('pricing_id')->get(['id', 'transaction_id', 'pricing_id', 'vat', 'price', DB::raw("COUNT(pricing_id) AS quantity")]);
        #$items = TransactionItem::with('menu')->whereTransactionId( $id )->get();
        $tendered = TransactionPayments::whereTransactionId( $id )->sum('amount');
        $discount = $transaction->discount;
        #dd($transaction);
        $data = compact('company', 'terminal', 'transaction', 'menus', 'payment', 'total', 'vat', 'discount', 'change', 'tendered');

        return view('sales.or', $data);
    }
}
