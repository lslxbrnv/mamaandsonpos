<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

use App\Terminals;
use App\Items;
use App\Purchases;
use App\PurchasedItems;

use App\Transaction;
use App\TransactionItem;
use App\TransactionMenu;
use App\TransactionPayments;
use DB;

class ReportsController extends Controller
{
    public function index()
    {
        $terminals = Terminals::where('active', 1)->get();
        $items = Items::get();
        $data = compact('terminals', 'items');

        return view('reports.index', $data);
    }

    public function generate_inventory(Request $request)
    {
        $inventory_from = $request->inventory_from;
        $inventory_to = $request->inventory_to;
        $inventory_item = $request->inventory_item;

        $purchase = PurchasedItems::select([
            DB::raw('purchase_date AS transaction_on'), 
            DB::raw("'' AS name"), 
            'quantity',
            'items.name AS item_name'
            ])
                ->leftJoin('items', 'items.id', '=', 'purchased_items.item_id')
                ->leftJoin('purchases', 'purchases.id', '=', 'purchased_items.purchase_id')
                ->where(function($query) use ($inventory_from, $inventory_to){
                    $query->where(DB::raw('date(purchase_date)') ,'>=', date('Y-m-d', strtotime($inventory_from)))
                        ->where(DB::raw('date(purchase_date)') ,'<=' , date('Y-m-d', strtotime($inventory_to)));
                });

        if($inventory_item != '')
        {
            $purchase->where('purchased_items.item_id', $inventory_item)->get();
        }

        $query = TransactionItem::select([ 
                    'transaction.transaction_on',
                    'users.name', 
                    DB::raw('(-1 * servings) AS quantity'),
                   'items.name AS item_name' 
                ])
                ->leftJoin('transaction_menu', 'transaction_menu.id', '=', 'transaction_item.transaction_menu_id')
                ->leftJoin('transaction', 'transaction.id', '=', 'transaction_menu.transaction_id')
                ->leftJoin('users', 'users.id', '=', 'transaction.user_id')
                ->leftJoin('items', 'items.id', '=', 'transaction_item.item_id')
                ->where(function($query) use ($inventory_from, $inventory_to){
                    $query->where(DB::raw('date(transaction_on)') ,'>=', date('Y-m-d', strtotime($inventory_from)))
                        ->where(DB::raw('date(transaction_on)') ,'<=' , date('Y-m-d', strtotime($inventory_to)));
                });

        if($inventory_item != '')
        {
            $query->where('transaction_item.item_id', $inventory_item);
        }

        $inventory = $query->unionAll($purchase)->orderBy('transaction_on', 'ASC')->get();
            
        $data = compact('inventory', 'inventory_from', 'inventory_to');

        return view('reports.inventory', $data);
        

    }
    public function generate_sales(Request $request)
    {
        $sales_from = $request->sales_from;
        $sales_to = $request->sales_to;

        $purchases = Purchases::with('PurchasedItems', 'PurchasedItems.Items')
                ->where(DB::raw('date(purchase_date)') ,'>=' , date('Y-m-d', strtotime($request->sales_from)))
                ->where(DB::raw('date(purchase_date)') ,'<=' , date('Y-m-d', strtotime($request->sales_to)))
                ->get();

        $transactions = Transaction::where('terminal_id', $request->sales_terminal)
                        ->where(function($query) use ($sales_from, $sales_to){
                            $query->where(DB::raw('date(transaction_on)') ,'>=', date('Y-m-d', strtotime($sales_from)))
                                ->where(DB::raw('date(transaction_on)') ,'<=' , date('Y-m-d', strtotime($sales_to)));
                        }) 
                        ->get();

        $data = compact('purchases', 'transactions', 'sales_from', 'sales_to');

        return view('reports.sales', $data);
    }

    public function generate_purchases(Request $request)
    {
        $purchase_from = $request->purchases_from;
        $purchase_to = $request->purchases_to;
        $purchases = Purchases::with('PurchasedItems', 'PurchasedItems.Items')
                ->where(DB::raw('date(purchase_date)') ,'>=' , date('Y-m-d', strtotime($request->purchases_from)))
                ->where(DB::raw('date(purchase_date)') ,'<=' , date('Y-m-d', strtotime($request->purchases_to)))->get();
        #dd($purchases);
        $data = compact('purchases', 'purchase_from', 'purchase_to');
        return view('reports.purchases', $data);
    }
}
