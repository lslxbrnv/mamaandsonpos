<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\MenuCategory;
use Response;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = MenuCategory::paginate(10);
        $data = compact('categories');

        return view('category.index', $data);
    }

    public function add_category_page()
    {
        return view('category.add-category');
    }

    public function edit_category(Request $request)
    {
        $category = MenuCategory::where('id','=', $request->categoryid)->first();
        $data = compact('category');

        return view('category.edit-category', $data);
    }

    public function add_category(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255|unique:menu_category,name,NULL,id,deleted_at,NULL'
        ]);

        $x = MenuCategory::create($request->only('name'));

        $data = [
            'message' => 'Category has been added.',
            'id' => $x->id,
            'name' => $request->name
        ];

        return redirect()->route('add_category_page')->with('message', 'Success');
    }

    public function delete_category(Request $request)
    {
        $category = MenuCategory::find($request->category_id);
        $category->delete();
    }

     public function update_category(Request $request)
    {
        if($request->name_hidden != $request->name)        {
            $name = 'required|max:255|unique:menu_category,name,NULL,id,deleted_at,NULL';
        }else{
            $name = 'required|max:255';
        }

        $this->validate($request, [
            'name' => $name
        ]);

        $x = MenuCategory::whereId($request->id)->update($request->only('name'));
       
        $data = [
            'message' => 'Category has been updated.',
            'id' => $request->id,
            'name' => $request->name
        ];

        return redirect()->route('edit_category', $request->id)->with('message', 'Success');

    }
}
