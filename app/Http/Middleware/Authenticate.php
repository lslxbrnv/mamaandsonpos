<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    public function handle($request, Closure $next, $guard = null)
    {
        /*if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }*/
        if(!Auth::check())
        {
            if($request->ajax() || $request->wantsJson())
            {
                return response('Unauthorized.', 401);
            }

            return redirect()->route('login_page')->with('message', 'Not authenticated!');
        }



        if(Auth::user()->access == 2 && $request->segment(2) != 'sales')
        {
            #return Auth::user();
            if($request->segment(3) != 'data-json')
            {
  
                return redirect()->route('sales');
            }
            
        }

        return $next($request);
    }
}
