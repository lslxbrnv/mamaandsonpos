<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionItem extends Model
{
    public $table = 'transaction_item';
    public $fillable = [ 'transaction_menu_id', 'pricing_id', 'item_id', 'servings', 'deleted_at' ];
    public $timestamps = false;

    public function transaction_menu()
    {
    	return $this->hasMany('App\TransactionMenu', 'transaction_menu_id');
    }

    public function transaction_menu_reverse()
    {
    	return $this->hasMany('App\TransactionMenu', 'id');
    }
}
