<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public $table = 'transaction';
    public $fillable = ['official_receipt', 'customer_name', 'table_id', 'terminal_id', 'user_id', 'transaction_on', 'deleted_at', 'type', 'discount' ];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function tables()
    {
        return $this->belongsTo('App\Tables', 'table_id')->withTrashed();
    }

    public function terminal()
    {
        return $this->belongsTo('App\Terminals');
    }

    public function transaction()
    {
        return $this->belongsTo('App\TransactionMenu');
    }

    public function transaction_reverse()
    {
        return $this->belongsTo('App\TransactionMenu', 'id');
    }

    public function transaction_menu()
    {
        return $this->hasMany('App\TransactionMenu', 'transaction_id', 'id');
    }

    public function transaction_payments()
    {
        return $this->belongsTo('App\TransactionPayments', 'id');
    }

    public function getOfficialReceiptFormatAttribute()
    {
        return str_pad($this->official_receipt, 10, '0', STR_PAD_LEFT);
    }
}
