<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\TransactionItem;
use App\PurchasedItems;
use App\Spoilages;

class Items extends Model
{
    use SoftDeletes;
    
    public $table = 'items';
    public $fillable = ['name', 'unit', 'created_at', 'updated_at'];
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function MenuItem()
    {
    	return $this->hasMany('App\MenuItem');
    }
    
    public function cash_register_items()
    {
    	return $this->hasMany('App\CashRegisterItems', 'item_id');
    }

    public function Recipes()
    {
    	return $this->hasMany('App\Recipes', 'item_id');
    }

    public function PurchasedItems() {
    	return $this->belongsTo('App\PurchasedItems', 'item_id');
    }


    public function getCurrentQuantityAttribute()
    {
        $id = $this->id;
        $balance = PurchasedItems::whereItemId($id)->sum('quantity');
        $deductspoilages = Spoilages::whereItemId($id)->sum('quantity');  
        $deduct = TransactionItem::whereItemId($id)->sum('servings');
        $total = ($balance - $deductspoilages) - $deduct;

        return $total;
    }

    public function getLastPurchasedDateAttribute()
    {
        $id = $this->id;
        $date = PurchasedItems::with('Purchases')->whereItemId($id)->orderBy('id' ,'desc')->first();
        return date_format($date->Purchases->purchase_date,'Y-m-d');
    }

    public function getTotalSpoilagesAttribute()
    {
        $id = $this->id;
        $spoilages = Spoilages::whereItemId($id)->sum('quantity');  
        return $spoilages;
    }

     public function getTotalTransactionItemsAttribute()
    {
        $id = $this->id;
        $transactionitems = TransactionItem::whereItemId($id)->sum('servings');
        return $transactionitems;
    }
}
