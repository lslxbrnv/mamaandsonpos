<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashRegisterMenu extends Model
{
    public $table = 'cash_register_menu';
    public $fillable = ['cash_register_id', 'pricing_id', 'discount'];
    public $timestamps = false;

    public function pricing()
    {
    	return $this->belongsTo('App\Pricing');
    }

    public function CashRegisterItems()
    {
        return $this->belongsTo('App\CashRegisterItems');
    }

    public function cash_register_items()
    {
        return $this->hasMany('App\CashRegisterItems', 'cash_register_id');
    }

    public function User()
    {
    	return $this->hasMany('App\User');
    }

    public function scopeCartGroupBy($query, $user_id)
    {
        return $query->select( 'id', 'menu_id', DB::raw("COUNT(menu_id) AS qty") )
                     ->whereUserId( $user_id )->groupBy('menu_id')->orderBy('id', 'ASC');
    }

    public function scopeCartNotGroupBy($query, $user_id)
    {
        return $query->select( 'id', 'menu_id' )
                     ->whereUserId( $user_id )
                     ->orderBy('menu_id', 'ASC');
    }
}
