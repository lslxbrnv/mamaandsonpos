<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdersMenu extends Model
{
    public $table = 'orders_menu';
    public $fillable = ['transaction_id', 'menu_id', 'quantity', 'price_id'];  
    public $timestamps = false;
    
    public function orders()
    {
        return $this->belongsTo('App\Orders', 'order_id', 'id');
    }

    public function menu()
    {
        return $this->belongsTo('App\Menu', 'menu_id', 'id');
    }

    public function pricing()
    {
        return $this->belongsTo('App\Pricing', 'price_id', 'id');
    }
}
