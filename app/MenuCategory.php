<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MenuCategory extends Model
{
    //
    use SoftDeletes;
    public $table = 'menu_category';
    public $fillable = ['name'];
    public $timestamps = false;
    protected $dates = ['deleted_at'];

    public function Menu()
    {
    	return $this->hasMany('App\Menu')->withTrashed();
    }

}
