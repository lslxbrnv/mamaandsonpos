<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchasedItems extends Model
{
    public $table = 'purchased_items';
    public $fillable = ['purchased_id', 'item_id', 'quantity', 'price'];
    public $timestamps = false;

    public function Purchases()
    {
        return $this->belongsTo('App\Purchases', 'purchase_id');
    }
    public function Items()
    {
    	return $this->belongsTo('App\Items', 'item_id')->withTrashed();
    }
}
