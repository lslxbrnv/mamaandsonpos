<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Tables extends Model
{
    use SoftDeletes;
    
    public $table = 'tables';
    public $fillable = [ 'name'];
    public $timestamps = false;
    protected $dates = ['deleted_at'];

    public function cash_register()
    {
        return $this->belongsTo('App\CashRegister');
    }

    public function cash_register_reverse()
    {
        return $this->hasMany('App\CashRegister', 'table_id');
    }

    public function transaction()
    {
        return $this->belongsTo('App\Transaction', 'table_id');
    }
}
