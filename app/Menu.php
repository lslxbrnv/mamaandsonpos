<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Menu extends Model
{
    use SoftDeletes;
    public $table = 'menu';
    public $fillable = ['category_id', 'menu', 'selling_price', 'user_id', 'created_at', 'image', 'details'];  
    public $timestamps = false;

    protected $dates = ['deleted_at'];

    public function Category()
    {
    	return $this->belongsTo('App\MenuCategory')->withTrashed();
    }

    public function Recipes()
    {
    	return $this->belongsTo('App\Recipes');
    }

    public function User()
    {
        return $this->belongsTo('App\User');
    }

    public function CashRegister()
    {
        return $this->hasMany('App\CashRegister');
    }

    public function TransactionItems()
    {
        return $this->hasMany('App\TransactionItems');
    }

    public function Pricing()
    {
        return $this->hasMany('App\Pricing');
    }

    public function cart()
    {
        return $this->hasMany('App\Cart');
    }

}
