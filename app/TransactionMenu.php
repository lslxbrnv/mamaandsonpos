<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransactionMenu extends Model
{
    public $table = 'transaction_menu';
    public $fillable = [ 'transaction_id', 'menu_id', 'price', 'discount', 'vat', 'deleted_at', 'pricing_id' ];
    public $timestamps = false;

    public function transaction()
    {
        return $this->hasMany('App\Transaction');
    }

    public function transaction_reverse()
    {
        return $this->hasMany('App\Transaction', 'id');
    }

    public function pricing()
    {
        return $this->belongsTo('App\Pricing')->withTrashed();
    }

    public function transaction_item()
    {
        return $this->belongsTo('App\TransactionItem', 'transaction_menu_id');
    }

    public function transaction_item_reverse()
    {
        return $this->belongsTo('App\TransactionItem', 'id');
    }

    public function getTotalPriceAttribute()
    {
        return $this->price * $this->quantity;
    }

    public function getVatAmountAttribute()
    {
    	$vat = 1 + $this->vat;
        $quantity = $this->quantity;
        $price = $this->price;

        return ($price * $quantity) / $vat;
    }
}
