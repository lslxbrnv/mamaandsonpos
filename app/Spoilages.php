<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Items;

class Spoilages extends Model
{
    public $table = 'spoilages';
    public $fillable = ['item_id', 'quantity', 'user_id', 'created_at', 'updated_at', 'spoilage_date'];
    public $timestamps = true;

    public function Items()
    {
    	return $this->belongsTo('App\Items', 'item_id');
    }
}
