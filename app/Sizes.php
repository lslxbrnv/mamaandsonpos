<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sizes extends Model
{
    use SoftDeletes;
    public $table = 'sizes';
    public $fillable = ['size', 'created_at'];  
    public $timestamps = false;

    protected $dates = ['deleted_at'];

  	public function pricing()
    {
    	return $this->hasMany('App\Pricing');
    }

    public function cart_items_size()
    {
        return $this->hasMany('App\Sizes');
    }
}
