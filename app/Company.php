<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    public $table = 'company';
    public $fillable = ['name', 'address', 'telephone_number', 'tin', 'serial_number', 'owner', 'permit_number', 'vat'];
    public $timestamps = false;
}
