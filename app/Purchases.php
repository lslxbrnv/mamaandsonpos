<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchases extends Model
{
    public $table = 'purchases';
    public $fillable = ['receipt', 'purchase_date'];
    public $timestamps = false;

    protected $dates = ['purchase_date'];

    public function PurchasedItems() {
    	return $this->hasMany('App\PurchasedItems', 'purchase_id');
    }

     public function getOfficialReceiptFormatAttribute()
    {
        return str_pad($this->receipt, 10, '0', STR_PAD_LEFT);
    }

}
