<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\TransactionItem;
use App\PurchasedItems;
use App\Purchases;
use App\Spoilages;

class Inventory extends Model
{
    use SoftDeletes;
    
    public $table = 'items';
    public $fillable = ['name', 'unit', 'created_at', 'updated_at'];
    public $timestamps = true;

    protected $dates = ['deleted_at'];

    public function PurchasedItems() {
    	return $this->belongsTo('App\PurchasedItems', 'item_id');
    }

    public function getCurrentQuantityAttribute()
    {
        $id = $this->id;
        $balance = PurchasedItems::whereItemId($id)->sum('quantity');
        $deductspoilages = Spoilages::whereItemId($id)->sum('quantity');  
        $deduct = TransactionItem::whereItemId($id)->sum('servings');
        $total = ($balance - $deductspoilages) - $deduct;

        return $total;
    }

    public function getLastPurchasedDateAttribute()
    {
        return $id = $this->id;
        $date = PurchasedItems::whereItemId($id)->orderBy('id' ,'desc')->get();
        #dd($date);#with('Purchases')->w
       
        #return $date->purchase_date;
        return 1;

    }
}
