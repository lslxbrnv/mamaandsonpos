<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class CashRegister extends Model
{
    public $table = 'cash_register';
    public $fillable = [ 'table_id', 'customer_name', 'user_id', 'added_on', 'type', 'discount'];
    public $timestamps = false;

    public function table()
    {
        return $this->hasMany('App\Tables');
    }

    public function table_reverse()
    {
        return $this->belongsTo('App\Tables', 'table_id');
    }

    public function User()
    {
    	return $this->hasMany('App\User');
    }
}
