<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipes extends Model
{
    public $table = 'recipes';
    public $fillable = ['menu_id', 'item_id', 'serving'];
    public $timestamps = false;

    public function Pricing()
    {
    	return $this->belongsTo('App\Pricing');
    }

    public function Item()
    {
    	return $this->belongsTo('App\Items');
    }
    #->withTrashed()
}
