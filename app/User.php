<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    public $table = 'users';
    public $timestamps = true;

    protected $fillable = [
        'name', 'username', 'password', 'access', 'status', 'remember_token', 'created_at', 'updated_at'
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];


}