<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        Model::unguard();

    	$this->call('CompanySeed');
    	$this->call('TerminalsSeed');
    	$this->call('ItemsSeed');
    	$this->call('MenuCategorySeed');
    	$this->call('MenuSeed');
    	#$this->call(TerminalsTableSeeder::class);
    	#$this->call(TerminalsSeed::class);

        #Model::reguard();
    }
}

class CompanySeed extends Seeder
{
	public function run()
	{
        $faker = Faker\Factory::create();

		App\Company::create([
			'name' => $faker->company, 
			'address' => $faker->address, 
			'telephone_number' => $faker->phoneNumber, 
			'tin' => $faker->creditCardNumber, 
			'serial_number' => $faker->creditCardNumber, 
			'owner' => $faker->name($gender = null|'male'|'female'), 
			'permit_number' => $faker->phoneNumber, 
			'vat' => 32
		]);
	}
}

class TerminalsSeed extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
    	$number = 2;

		App\Terminals::create([
				'terminal_name' => $faker->company, 
				'ip' => '127.0.0.1', 
				'hd_serial' => $faker->isbn13, 
				'machine_id' => $faker->ean13, 
				'server' => $faker->numberBetween(0, 1), 
				'active' => $faker->numberBetween(0,1)
			]);

		for ($i = 0; $i < $number; $i++)
		{
			$terminal_name = $faker->company;
			$ip = $faker->ipv4;
			$hd_serial = $faker->isbn13;
			$machine_id = $faker->ean13;
			$server = $faker->numberBetween(0, 1);
			$active = $faker->numberBetween(0,1);

			$terminal = [
				'terminal_name' => $terminal_name, 
				'ip' => $ip, 
				'hd_serial' => $hd_serial, 
				'machine_id' => $machine_id, 
				'server' => $server, 
				'active' => $active
			];

			App\Terminals::create($terminal);
		}
    }
}

class ItemsSeed extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
    	$number = 20;

		for ($i = 0; $i < $number; $i++)
		{
			$item = [
				'name' => $faker->words($nb = 2, $asText = true)
			];

			App\Items::create($item);
		}
    }
}

class MenuCategorySeed extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
    	$number = 10;

		for ($i = 0; $i < $number; $i++)
		{
			$item = [
				'name' => $faker->words($nb = 4, $asText = true)
			];

			App\MenuCategory::create($item);
		}
    }
}

class MenuSeed extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
    	$number = 30;

    	$category = App\MenuCategory::count();
    	$item_count = App\Items::count();

		for ($i = 0; $i < $number; $i++)
		{
			$data = [
				'category_id' => $faker->numberBetween(1, $category), 
				'menu' => $faker->words($nb = 2, $asText = true), 
				'selling_price' => $faker->randomNumber(4), 
				'user_id' => 1, 
				'created_at' => Carbon::now()
			];

			$menu = App\Menu::create($data);

			$number_of_item = $faker->numberBetween(3, 8);

			for ($x = 0; $x < $number_of_item; $x++)
			{
				$item = [
					'menu_id' => $menu->id, 
					'item_id' => $faker->numberBetween(1, 20), 
					'serving' => $faker->numberBetween(1, 5)
				];

				App\Recipes::create($item);
			}
		}
    }
}
