<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Terminals

class Terminals extends Seeder
{
    public function run()
    {
        $faker = Faker\Factory::create();
    	$number = 2;

		for ($i = 0; $i < $number; $i++)
		{

			$terminal_name = $faker->company;
			$ip = $faker->ipv4;
			$hd_serial = $faker->isbn13;
			$machine_id = $faker->ean13;
			$server = $faker->numberBetween(0, 1);
			$active = $faker->numberBetween(0,1);

			$terminal = [
				'terminal_name' => $terminal_name, 
				'ip' => $ip, 
				'hd_serial' => $hd_serial, 
				'machine_id' => $machine_id, 
				'server' => $server, 
				'active' => $active
			];

			Terminals::create($terminal);
		}
    }
}
