<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteTransactionsTables extends Migration
{
    public function up()
    {
        if(Schema::hasTable('trans_items_item'))
        {
            Schema::drop('trans_items_item');
        }

        if(Schema::hasTable('transaction_items'))
        {
            Schema::drop('transaction_items');
        }
        
        if(Schema::hasTable('transaction_payments'))
        {
            Schema::drop('transaction_payments');
        }
        
        if(Schema::hasTable('transaction'))
        {
            Schema::drop('transaction');
        }
    }

    public function down()
    {
        //
    }
}
