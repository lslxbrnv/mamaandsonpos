<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterNewTableTransactionType extends Migration
{
    public function up()
    {
        if (Schema::hasTable('transaction'))
        {
            Schema::table('transaction', function(Blueprint $table){
                $table->string('type', 25);
            });
        }
    }

    public function down()
    {
        //
    }
}
