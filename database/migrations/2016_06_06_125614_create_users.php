<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsers extends Migration
{
    public function up()
    {
        Schema::create('users', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('username', 60);
            $table->string('password', 60);
            $table->string('name');
            $table->string('email', 255);
            $table->tinyInteger('access');
            $table->tinyInteger('status'); // 0-disable; 1-enable; 2-reset;
            $table->rememberToken(); //

            $table->timestamps();
        });

        App\User::create([
            'username' => 'admin',
            #'password' => '$2y$10$QFWx.m1YUnb1lzW2WJEjduAJjIB9FY8p6EewYw3M/J5Ego5fMAB7i',
            'password' => bcrypt('p@ssw0rd'),
            'name' => 'Administrator',
            'access' => 1,
            'status' => 1
        ]);
    }

    public function down()
    {
        Schema::drop('users');
    }
}
