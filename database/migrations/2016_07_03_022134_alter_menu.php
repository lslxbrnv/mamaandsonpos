<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMenu extends Migration
{
    public function up()
    {
        if (Schema::hasTable('menu'))
        {
            if (!Schema::hasColumn('menu', 'deleted_at'))
            {
                Schema::table('menu', function(Blueprint $table){
                    $table->softDeletes(); //
                });
            }
        }
    }

    public function down()
    {
        Schema::table('menu', function ($table) {
            $table->dropColumn(['deleted_at']);
        });
    }
}
