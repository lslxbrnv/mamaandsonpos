<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCashRegisterTableType extends Migration
{
    public function up()
    {
        if (Schema::hasTable('cash_register'))
        {
            Schema::table('cash_register', function(Blueprint $table){
                $table->string('type', 25);
            });
        }
    }

    public function down()
    {
        //
    }
}
