<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Menu extends Migration
{
    public function up()
    {
        Schema::create('menu', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->string('menu');
            $table->decimal('selling_price', 10, 2);
            $table->integer('user_id')->unsigned();
            $table->text('details');
            $table->dateTime('created_at');

            $table->string('image');
            $table->index(['category_id', 'user_id']);

            $table->foreign('category_id')->references('id')->on('menu_category')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::drop('menu');
    }
}
