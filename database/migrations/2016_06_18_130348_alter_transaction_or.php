<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransactionOr extends Migration
{
    public function up()
    {
        if (Schema::hasTable('transaction'))
        {
            if (!Schema::hasColumn('transaction', 'official_receipt'))
            {
                Schema::table('transaction', function(Blueprint $table){
                    $table->integer('official_receipt');
                });
            }
        }
    }

    public function down()
    {
        Schema::table('transaction', function ($table) {
            $table->dropColumn(['official_receipt']);
        });
    }
}
