<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCashRegisterItems extends Migration
{
    public function up()
    {
        Schema::create('cash_register_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cash_register_menu_id')->unsigned()->index();
            $table->integer('pricing_id')->unsigned()->index();
            $table->integer('item_id')->unsigned()->index();

           # $table->index(['cash_register_menu_id', 'pricing_id', 'item_id']);

            $table->foreign('cash_register_menu_id')->references('id')->on('cash_register_menu');
            $table->foreign('pricing_id')->references('id')->on('pricing');
            $table->foreign('item_id')->references('id')->on('items');
        });
    }

    public function down()
    {
        if (Schema::hasTable('cash_register_items'))
        {
            Schema::drop('cash_register_items');
        }
    }
}
