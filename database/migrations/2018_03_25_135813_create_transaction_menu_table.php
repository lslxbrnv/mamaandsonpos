<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionMenuTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->integer('menu_id')->unsigned();
            $table->integer('quantity');
            $table->integer('price_id')->unsigned();


            $table->foreign('menu_id')->references('id')->on('menu');
            $table->foreign('price_id')->references('id')->on('pricing');
            $table->foreign('order_id')->references('id')->on('orders');


            #$table->index(['menu_id', 'transaction_id', 'size_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_menu');
    }
}
