<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenuItem extends Migration
{
    public function up()
    {
        Schema::create('menu_item', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->integer('menu_id')->unsigned();
            $table->integer('item_id')->unsigned();
            
            $table->index(['menu_id', 'item_id']);

            $table->foreign('menu_id')->references('id')->on('menu');
            $table->foreign('item_id')->references('id')->on('items')->onDelete('cascade');
        });    
    }

    public function down()
    {
        Schema::drop('menu_item');
    }
}
