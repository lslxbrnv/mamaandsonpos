<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterMenuCategory extends Migration
{
    public function up()
    {
        if (Schema::hasTable('menu_category'))
        {
            if (!Schema::hasColumn('menu_category', 'deleted_at'))
            {
                Schema::table('menu_category', function(Blueprint $table){
                    $table->softDeletes(); //
                });
            }
        }
    }

    public function down()
    {
        Schema::table('menu_category', function ($table) {
            $table->dropColumn(['deleted_at']);
        });
    }
}
