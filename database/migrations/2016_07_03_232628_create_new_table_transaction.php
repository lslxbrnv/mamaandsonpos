<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTableTransaction extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('transaction'))
        {
            Schema::create('transaction', function(Blueprint $table) 
            {
                $table->increments('id');
                $table->integer('official_receipt');
                $table->string('customer_name');
                $table->integer('table_id')->unsigned();
                #$table->string('table', 100);
                $table->integer('terminal_id')->unsigned();
                $table->integer('user_id')->unsigned();
                $table->timestamp('transaction_on');
                $table->softDeletes();
                
                $table->index([ 'terminal_id', 'user_id' ]);

                $table->foreign('table_id')->references('id')->on('tables');
                $table->foreign('terminal_id')->references('id')->on('terminals');
                $table->foreign('user_id')->references('id')->on('users');
            });
        }
    }

    public function down()
    {
        if(Schema::hasTable('transaction'))
        {
            Schema::drop('transaction');
        }
    }
}
