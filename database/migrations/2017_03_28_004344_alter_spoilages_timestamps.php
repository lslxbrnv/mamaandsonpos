<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterSpoilagesTimestamps extends Migration
{
   public function up()
    {
        if (Schema::hasTable('spoilages'))
        {
            if (!Schema::hasColumn('spoilages', 'created_at'))
            {
                Schema::table('spoilages', function(Blueprint $table){
                    $table->timestamps();
                });
            }
        }
    }

    public function down()
    {
        Schema::table('spoilages', function ($table) {
            $table->dropColumn(['created_at']);
        });
    }
}



            