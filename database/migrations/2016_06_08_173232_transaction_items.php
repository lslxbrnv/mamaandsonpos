<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TransactionItems extends Migration
{
    public function up()
    {
        Schema::create('transaction_items', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->integer('transaction_id')->unsigned();
            $table->integer('pricing_id')->unsigned();
            $table->integer('quantity');
            
            $table->index(['pricing_id', 'transaction_id']);

            $table->foreign('pricing_id')->references('id')->on('pricing');
            $table->foreign('transaction_id')->references('id')->on('transaction');
        });
    }

    public function down()
    {
        Schema::drop('transaction_items');
    }
}
