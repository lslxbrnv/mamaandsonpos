<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransactionPayment extends Migration
{
    public function up()
    {
        if (Schema::hasTable('transaction'))
        {
            if (Schema::hasColumn('transaction', 'cash_received'))
            {
                Schema::table('transaction', function(Blueprint $table){
                    $table->dropColumn(['cash_received']);
                });
            }
        }
    }

    public function down()
    {
    }
}
