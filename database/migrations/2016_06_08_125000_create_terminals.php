<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTerminals extends Migration
{
    public function up()
    {
        Schema::create('terminals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('terminal_name');
            $table->string('ip');
            $table->string('hd_serial');
            $table->string('machine_id');
            $table->tinyInteger('server'); // 0 = disable, 1 = enable
            $table->tinyInteger('active'); // 0 = disable, 1 = enable
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('terminals');
    }
}
