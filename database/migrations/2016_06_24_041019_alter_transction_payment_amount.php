<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransctionPaymentAmount extends Migration
{
    public function up()
    {
        if (Schema::hasTable('transaction_payments'))
        {
            Schema::table('transaction_payments', function(Blueprint $table){
                $table->dropColumn('cash');
                $table->decimal('amount', 10, 2);
            });
        }
    }

    public function down()
    {

    }
}
