<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCashRegister extends Migration
{
    public function up()
    {
        if (Schema::hasTable('cash_register'))
        {
            Schema::table('cash_register', function(Blueprint $table){
                $table->dropColumn(['id']);
            });
            Schema::table('cash_register', function(Blueprint $table){
                $table->increments('id');
            });
        }
    }

    public function down()
    {
        Schema::table('cash_register', function(Blueprint $table){
            $table->dropColumn(['id']);
        });
    }
}
