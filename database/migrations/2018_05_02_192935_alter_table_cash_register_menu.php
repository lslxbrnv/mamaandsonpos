<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableCashRegisterMenu extends Migration
{
    public function up()
    {
        Schema::create('cash_register_menu', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cash_register_id')->unsigned()->index();
            $table->integer('pricing_id')->unsigned()->index();
            $table->decimal('discount', 5, 2);

           # $table->index(['cash_register_id', 'pricing_id']);

            $table->foreign('cash_register_id')->references('id')->on('cash_register');
            $table->foreign('pricing_id')->references('id')->on('pricing');
        });
    }

    public function down()
    {
        if (Schema::hasTable('cash_register_menu'))
        {
            Schema::drop('cash_register_menu');
        }
    }
}
