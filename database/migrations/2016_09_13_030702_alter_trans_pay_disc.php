<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransPayDisc extends Migration
{
    public function up()
    {
        if (Schema::hasTable('transaction'))
        {
            Schema::table('transaction', function(Blueprint $table){
                $table->decimal('discount', 5, 2);
            });
        }
    }

    public function down()
    {
        if (Schema::hasTable('transaction'))
        {
            Schema::table('transaction', function(Blueprint $table){
                $table->dropColumn('discount');
            });
        }
    }
}
