<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCreateCompanyVat extends Migration
{
    public function up()
    {
        if (Schema::hasTable('company'))
        {
            Schema::table('company', function(Blueprint $table){
                $table->decimal('vat', 3, 2);
            });
        }
    }

    public function down()
    {

    }
}
