<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCashRegisterItemsIpriamryKey extends Migration
{
    public function up()
    {
        if (Schema::hasTable('cash_register_items'))
        {
            Schema::table('cash_register_items', function(Blueprint $table){
                $table->dropColumn(['id']);
            });
            Schema::table('cash_register_items', function(Blueprint $table){
                $table->increments('id');
            });
        }
    }

    public function down()
    {
        Schema::table('cash_register_items', function(Blueprint $table){
            $table->dropColumn(['id']);
        });
    }
}
