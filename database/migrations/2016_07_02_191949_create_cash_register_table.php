<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashRegisterTable extends Migration
{
    public function up()
    {
        Schema::create('cash_register', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('table_number');
            $table->string('customer_name');
            $table->integer('user_id')->unsigned();
            $table->timestamp('added_on');

            $table->index(['added_on', 'user_id']);

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down()
    {
        if (Schema::hasTable('cash_register'))
        {
            Schema::drop('cash_register');
        }
    }
}
