<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransactionItemsTaxStatus extends Migration
{
    public function up()
    {
        if (Schema::hasTable('transaction_items'))
        {
            Schema::table('transaction_items', function(Blueprint $table){
                $table->decimal('tax', 3, 2);
                $table->softDeletes();
            });
        }
    }

    public function down()
    {
        if (Schema::hasTable('transaction_items'))
        {
            Schema::table('transaction_items', function(Blueprint $table){
                $table->dropColumn(['tax', 'deleted_at']);
            });
        }
    }
}
