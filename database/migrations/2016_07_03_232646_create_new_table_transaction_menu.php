<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTableTransactionMenu extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('transaction_menu'))
        {
            Schema::create('transaction_menu', function(Blueprint $table) 
            {
                $table->increments('id');
                $table->integer('transaction_id')->unsigned();
                $table->integer('pricing_id')->unsigned();
                $table->decimal('discount', 5, 2);
                $table->decimal('vat', 5, 2);
                $table->softDeletes();
                
                $table->index([ 'transaction_id', 'pricing_id' ]);

                $table->foreign('transaction_id')->references('id')->on('transaction');
                $table->foreign('pricing_id')->references('id')->on('pricing');
            });
        }
    }

    public function down()
    {
        if(Schema::hasTable('transaction_menu'))
        {
            Schema::drop('transaction_menu');
        }
    }
}
