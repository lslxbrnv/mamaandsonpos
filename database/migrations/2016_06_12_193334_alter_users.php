<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUsers extends Migration
{
    public function up()
    {
        if (Schema::hasTable('users'))
        {
            if (!Schema::hasColumn('users', 'updated_at') && !Schema::hasColumn('users', 'created_at'))
            {
                Schema::table('users', function(Blueprint $table){
                    $table->timestamps(); //
                });
            }
        }
    }

    public function down()
    {   
        Schema::table('users', function ($table) {
            $table->dropColumn([ 'created_at', 'updated_at']);
        });
    }
}
