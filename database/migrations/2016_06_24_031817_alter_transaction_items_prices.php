<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransactionItemsPrices extends Migration
{
    public function up()
    {
        if (Schema::hasTable('transaction_items'))
        {
            Schema::table('transaction_items', function(Blueprint $table){
                $table->decimal('price', 10, 2);
                $table->decimal('discount', 3, 2);
            });
        }
    }

    public function down()
    {
        if (Schema::hasTable('transaction_items'))
        {
            Schema::table('transaction_items', function(Blueprint $table){
                $table->dropColumn(['price', 'discount']);
            });
        }
    }
}
