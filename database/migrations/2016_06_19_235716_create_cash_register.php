<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashRegister extends Migration
{
    public function up()
    {
        Schema::create('cash_register', function (Blueprint $table) {
            $table->string('id')->unique();
            $table->integer('pricing_id')->unsigned();
            $table->integer('user_id')->unsigned();
            
            $table->index(['pricing_id', 'user_id']);

            $table->foreign('pricing_id')->references('id')->on('pricing');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::drop('cash_register');
    }
}
