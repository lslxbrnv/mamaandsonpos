<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTableTransactionItems extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('transaction_item'))
        {
            Schema::create('transaction_item', function(Blueprint $table) 
            {
                $table->increments('id');
                $table->integer('transaction_menu_id')->unsigned();
                $table->integer('pricing_id')->unsigned();
                $table->integer('item_id')->unsigned();
                $table->decimal('servings', 10, 2);
                $table->softDeletes();
                
                $table->index([ 'transaction_menu_id', 'pricing_id', 'item_id' ]);

                $table->foreign('transaction_menu_id')->references('id')->on('transaction_menu');
                $table->foreign('pricing_id')->references('id')->on('pricing');
                $table->foreign('item_id')->references('id')->on('items');
            });
        }
    }

    public function down()
    {
        if(Schema::hasTable('transaction_item'))
        {
            Schema::drop('transaction_item');
        }
    }
}
