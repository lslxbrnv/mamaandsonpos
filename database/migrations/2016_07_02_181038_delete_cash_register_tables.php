<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteCashRegisterTables extends Migration
{
    public function up()
    {
        if(Schema::hasTable('cash_register_items'))
        {
            Schema::drop('cash_register_items');
        }

        if(Schema::hasTable('cash_register_menu'))
        {
            Schema::drop('cash_register_menu');
        }
        
        if(Schema::hasTable('cash_register'))
        {
            Schema::drop('cash_register');
        }
    }

    public function down()
    {
        //
    }
}
