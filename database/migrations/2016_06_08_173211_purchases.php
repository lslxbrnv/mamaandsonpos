<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Purchases extends Migration
{
    public function up()
    {
        Schema::create('purchases', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('receipt');
            $table->timestamp('purchase_date');
            
            $table->index(['purchase_date']);
        });
    }

    public function down()
    {
        Schema::drop('purchases');
    }
}
