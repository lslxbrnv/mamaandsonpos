<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTables extends Migration
{
    public function up()
    {
        Schema::create('tables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        App\Tables::create([
            'id' => 1,
            'name' => 'Take Out'
        ]);

        App\Tables::create([
            'name' => 'Table 1'
        ]);

        App\Tables::create([
            'name' => 'Table 2'
        ]);

        App\Tables::create([
            'name' => 'Table 3'
        ]);

        App\Tables::create([
            'name' => 'Table 4'
        ]);

        App\Tables::create([
            'name' => 'Table 5'
        ]);

        App\Tables::create([
            'name' => 'Table 6'
        ]);

        App\Tables::create([
            'name' => 'Table 7'
        ]);

        App\Tables::create([
            'name' => 'Table 8'
        ]);

        App\Tables::create([
            'name' => 'Table 9'
        ]);

        App\Tables::create([
            'name' => 'Table 10'
        ]);
    }

    public function down()
    {
        if (Schema::hasTable('tables'))
        {
            Schema::drop('tables');
        }
    }
}
