<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCashRegDisc extends Migration
{
    public function up()
    {
        if (Schema::hasTable('cash_register'))
        {
            Schema::table('cash_register', function(Blueprint $table){
                $table->decimal('discount', 5, 2);
            });
        }
    }

    public function down()
    {
        if (Schema::hasTable('cash_register'))
        {
            Schema::table('cash_register', function(Blueprint $table){
                $table->dropColumn('discount');
            });
        }
    }
}
