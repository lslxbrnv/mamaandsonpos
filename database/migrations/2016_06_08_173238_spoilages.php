<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Spoilages extends Migration
{
    public function up() 
    {
        Schema::create('spoilages', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->integer('item_id')->unsigned();
            $table->decimal('quantity', 10,2);
            $table->integer('user_id')->unsigned();
            $table->dateTime('spoilage_date');
        });    
    }

    public function down()
    {
        Schema::drop('spoilages');
    }
}
