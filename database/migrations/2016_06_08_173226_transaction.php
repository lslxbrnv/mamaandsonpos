<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Transaction extends Migration
{
    public function up()
    {
        Schema::create('transaction', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->date('transaction_date');
            $table->time('transaction_time');
            $table->string('customer_name');
            $table->decimal('cash_received',7,2);
            $table->integer('user_id')->unsigned();
            
            $table->index(['user_id']);

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    public function down()
    {
        Schema::drop('transaction');
    }
}
