<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCashRegisterItems extends Migration
{
    public function up()
    {
        Schema::create('cash_register_items', function (Blueprint $table) {
            $table->string('id')->unique();
            $table->integer('cash_register_id')->unsigned();
            $table->integer('pricing_id')->unsigned();
            $table->integer('item_id')->unsigned();
            
            $table->index(['cash_register_id', 'pricing_id', 'item_id']);

            $table->foreign('cash_register_id')->references('id')->on('cash_register');
            $table->foreign('pricing_id')->references('id')->on('pricing');
            $table->foreign('item_id')->references('id')->on('items');
        });
    }

    public function down()
    {
        Schema::drop('cash_register_items');
    }
}
