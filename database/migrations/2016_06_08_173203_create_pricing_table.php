<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePricingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pricing', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('price', 6, 2);
            $table->integer('menu_id')->unsigned();
            $table->integer('size_id')->unsigned();

            $table->index(['menu_id', 'size_id']);

            $table->foreign('menu_id')->references('id')->on('menu');
            $table->foreign('size_id')->references('id')->on('sizes');


            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pricing');
    }
}
