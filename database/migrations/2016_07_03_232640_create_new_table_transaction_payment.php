<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewTableTransactionPayment extends Migration
{
    public function up()
    {
        if(!Schema::hasTable('transaction_payments'))
        {
            Schema::create('transaction_payments', function(Blueprint $table) 
            {
                $table->increments('id');
                $table->integer('transaction_id')->unsigned();
                $table->string('type', 50);
                $table->decimal('amount', 10, 2);
                $table->softDeletes();
                
                $table->index([ 'transaction_id' ]);

                $table->foreign('transaction_id')->references('id')->on('transaction');
            });
        }
    }

    public function down()
    {
        if(Schema::hasTable('transaction_payments'))
        {
            Schema::drop('transaction_payments');
        }
    }
}
