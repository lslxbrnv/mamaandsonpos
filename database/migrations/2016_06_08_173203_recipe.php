<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Recipe extends Migration
{
    public function up()
    {
        Schema::create('recipes', function(Blueprint $table)
        {
            $table->increments('id');
            /*$table->integer('menu_id')->unsigned()->nullable();*/
            $table->integer('item_id')->unsigned();
            $table->double('serving');

         /*   $table->index(['menu_id', 'item_id']);*/
            $table->index(['item_id']);
            
            $table->foreign('item_id')->references('id')->on('items');
        /*    $table->foreign('menu_id')->references('id')->on('menu');*/
        });
    }

    public function down()
    {
        Schema::drop('recipes');

    }
}
