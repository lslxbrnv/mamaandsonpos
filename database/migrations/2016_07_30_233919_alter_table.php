<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTable extends Migration
{
   public function up()
    {
        if (Schema::hasTable('tables'))
        {
            if (!Schema::hasColumn('tables', 'deleted_at'))
            {
                Schema::table('tables', function(Blueprint $table){
                    $table->softDeletes(); //
                });
            }
        }
    }

    public function down()
    {
        Schema::table('tables', function ($table) {
            $table->dropColumn(['deleted_at']);
        });
    }
}