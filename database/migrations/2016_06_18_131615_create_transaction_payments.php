<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionPayments extends Migration
{
    public function up()
    {
        Schema::create('transaction_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('transaction_id')->unsigned();
            $table->decimal('cash', 10, 2);
            $table->string('type', 50);
            $table->softDeletes();
            
            $table->index(['transaction_id', 'deleted_at']);

            $table->foreign('transaction_id')->references('id')->on('transaction');
        });
    }

    public function down()
    {
        Schema::drop('transaction_payments');
    }
}
