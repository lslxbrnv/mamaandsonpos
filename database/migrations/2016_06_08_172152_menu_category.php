<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MenuCategory extends Migration
{
    public function up()
    {
        Schema::create('menu_category', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('name');
        });
    }

    public function down()
    {
        Schema::drop('menu_category');
    }
}
