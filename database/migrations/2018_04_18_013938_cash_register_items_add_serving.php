<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CashRegisterItemsAddServing extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        if (Schema::hasTable('cash_register_items'))
        {
            Schema::table('cash_register_items', function(Blueprint $table){
                 $table->integer('servings');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cash_register_items', function ($table) {
            $table->dropColumn(['servings']);
        });
    }
}
