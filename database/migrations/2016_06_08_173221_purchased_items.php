<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PurchasedItems extends Migration
{
    public function up()
    {
        Schema::create('purchased_items', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->integer('purchase_id')->unsigned();
            $table->integer('item_id')->unsigned();
            $table->decimal('quantity',10,2);
            $table->decimal('price',6,2);
            
            $table->index(['purchase_id', 'item_id']);
            
            $table->foreign('purchase_id')->references('id')->on('purchases');
            $table->foreign('item_id')->references('id')->on('items');
        });
    }

    public function down()
    {
        Schema::drop('purchased_items');
    }
}
