<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItems extends Migration
{
    public function up()
    {
        if (Schema::hasTable('items'))
        {
            if (!Schema::hasColumn('items', 'unit'))
            {
                Schema::table('items', function(Blueprint $table){
                    $table->string('unit');
                });
            }
        }
    }

    public function down()
    {
        Schema::table('items', function ($table) {
            $table->dropColumn(['unit']);
        });
    }
}
