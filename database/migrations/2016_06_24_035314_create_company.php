<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompany extends Migration
{
    public function up()
    {
        Schema::create('company', function(Blueprint $table) 
        {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('address')->nullable();
            $table->string('telephone_number')->nullable();
            $table->string('tin')->nullable();
            $table->string('serial_number')->nullable();
            $table->string('owner')->nullable();
            $table->string('permit_number')->nullable();
        });
    }

    public function down()
    {
        Schema::drop('company');
    }
}
