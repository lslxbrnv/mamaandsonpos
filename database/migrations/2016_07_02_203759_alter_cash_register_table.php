<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCashRegisterTable extends Migration
{
    public function up()
    {
        if (Schema::hasTable('cash_register'))
        {
            Schema::table('cash_register', function(Blueprint $table){
                $table->dropColumn('table_number');
                #$table->string('table', 100);
                $table->integer('table_id')->unsigned();
                $table->index(['table_id']);
                $table->foreign('table_id')->references('id')->on('tables');
            });
        }
    }

    public function down()
    {
        //
    }
}
