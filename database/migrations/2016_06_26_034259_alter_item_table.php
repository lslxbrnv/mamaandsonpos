<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterItemTable extends Migration
{
     public function up()
    {
        if (Schema::hasTable('items'))
        {
            if (!Schema::hasColumn('items', 'deleted_at'))
            {
                Schema::table('items', function(Blueprint $table){
                    $table->softDeletes(); //
                });
            }
        }
    }

    public function down()
    {
        Schema::table('items', function ($table) {
            $table->dropColumn(['deleted_at']);
        });
    }
}
