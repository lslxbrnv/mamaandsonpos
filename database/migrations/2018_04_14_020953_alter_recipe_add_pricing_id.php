<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterRecipeAddPricingId extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('recipes'))
        {
            Schema::table('recipes', function(Blueprint $table){
                $table->integer('pricing_id')->unsigned();
                $table->foreign('pricing_id')->references('id')->on('pricing');

                $table->index(['pricing_id']);

            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recipes', function ($table) {
            $table->dropColumn(['pricing_id']);
        });
    }
}
