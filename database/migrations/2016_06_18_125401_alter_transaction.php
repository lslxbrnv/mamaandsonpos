<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransaction extends Migration
{
    public function up()
    {
        if (Schema::hasTable('transaction'))
        {
            if (!Schema::hasColumn('transaction', 'terminal_id'))
            {
                Schema::table('transaction', function(Blueprint $table){
                    $table->integer('terminal_id')->unsigned();
                    $table->index(['terminal_id']);
                    $table->foreign('terminal_id')->references('id')->on('terminals');
                });
            }

            if (!Schema::hasColumn('transaction', 'transaction_datetime'))
            {
                Schema::table('transaction', function(Blueprint $table){
                    $table->timestamp('transaction_datetime');
                });
            }

            if (!Schema::hasColumn('transaction', 'deleted_at'))
            {
                Schema::table('transaction', function(Blueprint $table){
                    $table->softDeletes(); //
                });
            }

            if (Schema::hasColumn('transaction', 'transaction_date') || Schema::hasColumn('transaction', 'transaction_time'))
            {
                Schema::table('transaction', function(Blueprint $table){
                    $table->dropColumn(['transaction_date', 'transaction_time']);
                });
            }
        }
    }

    public function down()
    {
        if (Schema::hasTable('transaction'))
        {
            if (Schema::hasColumn('transaction', 'terminal_id'))
            {
                Schema::table('transaction', function ($table) {
                    #$table->dropForeign('transaction_terminal_id_index');
                    #$table->dropIndex(['transaction_terminal_id_index', 'transaction_user_id_index']);
                    #$table->dropColumn(['terminal_id', 'transaction_datetime', 'deleted_at']);
                });
            }
        }
    }
}
