<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterCashRegisterItems extends Migration
{
    public function up()
    {
        if (Schema::hasTable('cash_register_items'))
        {
            Schema::table('cash_register_items', function(Blueprint $table){
                $table->decimal('servings', 10, 2);
            });
        }
    }

    public function down()
    {

    }
}
