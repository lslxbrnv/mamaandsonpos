<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTransactionMenuAddPrice extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
        if (Schema::hasTable('transaction_menu'))
        {
            Schema::table('transaction_menu', function(Blueprint $table){
                $table->decimal('price', 10,2);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transaction_menu', function ($table) {
            $table->dropColumn(['price']);
        });
    }
}
